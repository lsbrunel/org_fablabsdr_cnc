package org.fablabsdr.cnc;


import java.awt.Color;
import java.util.Vector;

import org.fablabsdr.cnc3D.CNC3D;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DSet;




public class ToolPath  
{
private Vector<ToolMove> toolPath3D=new Vector<ToolMove>();
//private Vecteur lastPoint; //TODO optimize putting the last point here


public ToolPath()
{
Vecteur p1=new Vecteur(0,0,0);
Vecteur p2=new Vecteur(0,0,0);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,Color.red.getRGB()),0) );
}

public void add(ToolMove tm) {toolPath3D.add(tm);}



/**
 * add a movement incremental dx dy dz in the toolPath
 * @param _dx
 * @param _dy
 * @param _dz
 * @param speed
 * @param toolPath3D
 */
public  void addMovementIncremental(double _dx,double _dy,double _dz,double speed)
{
Vecteur p1=lastPoint();
Vecteur p=p1.copy();
p._add(_dx,_dy,_dz);
Vecteur p2=p;
float hue=-0.09f+(float)(speed-CNCUtil.SPEEDMIN)/(float)(CNCUtil.SPEEDMAX-CNCUtil.SPEEDMIN)*0.78f;
if (hue>1) hue-=1;
Color c=Color.getHSBColor(hue,1.0f,1.0f);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,c.getRGB()),speed));
}

/**
 * add a movement toward absolute coordinates x y z in the toolPath
 * @param x
 * @param y
 * @param z
 * @param speed speed of the movement
 * @param toolPath3D path to add the move
 */
public  void addMovementAbsolute(double x,double y,double z,double speed)
{
Vecteur p1=lastPoint();
Vecteur p2=new Vecteur(x,y,z);
float hue=-0.09f+(float)(speed-CNCUtil.SPEEDMIN)/(float)(CNCUtil.SPEEDMAX-CNCUtil.SPEEDMIN)*0.78f;
if (hue>1) hue-=1;
Color c=Color.getHSBColor(hue,1.0f,1.0f);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,c.getRGB()),speed));
}

/**
 * 
 * @param p2 point where to move
 * @param speed speed of the movement
 */
public  void addMovementAbsolute(Vecteur p2,double speed)
{
Vecteur p1=lastPoint();
float hue=-0.09f+(float)(speed-CNCUtil.SPEEDMIN)/(float)(CNCUtil.SPEEDMAX-CNCUtil.SPEEDMIN)*0.78f;
if (hue>1) hue-=1;
Color c=Color.getHSBColor(hue,1.0f,1.0f);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,c.getRGB()),speed));
}


/**
 * get the last point of the tool path
 * @param toolPath3D
 * @return
 */
public  Vecteur lastPoint()
{
return toolPath3D.elementAt(toolPath3D.size()-1).p2();	
}


/**remove all movements*/
public void clean() 
{
toolPath3D.removeAllElements();
Vecteur p1=new Vecteur(0,0,0);
Vecteur p2=new Vecteur(0,0,0);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,Color.red.getRGB()),0) );
}



public String convertInGCode()
{
//calculate the G code
return CNC3D.calculateGcode(this,false);
}




public Object3DColorSet getObject3DSet() 
{
Segment3DSet set=new Segment3DSet();
//initialize the drawing 3D
//Vector<Object3D> drawing3D=new Vector<Object3D>();
//draw the tool path
//for (int i=0;i<toolPath3D.size();i++) drawing3D.add(toolPath3D.elementAt(i).getSegment3D());
for (int i=0;i<toolPath3D.size();i++) set.add(toolPath3D.elementAt(i).getSegment3D());
return set;
}


public int nbMovements() 
{
return toolPath3D.size();
}



public ToolMove getToolMove(int i) 
{
return toolPath3D.elementAt(i);
}


public double getDuration_s()
{
double time=0;
for (ToolMove m:toolPath3D) time+=m.getDuration_s();
System.out.println(getClass()+" duration="+time+" s");
return time;
}


public String toString()
{
StringBuffer sb=new StringBuffer();
for (ToolMove seg:toolPath3D) sb.append(seg+"\n");
return sb.toString();
}

public int size() {
	
	return toolPath3D.size();
}

public ToolMove elementAt(int i) {
	
	return toolPath3D.elementAt(i);
}


public void removeAllToolMove()
{
	toolPath3D.removeAllElements();
}

}


