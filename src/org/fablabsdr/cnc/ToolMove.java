package org.fablabsdr.cnc;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.threeD.rendering.Sun;



public class ToolMove implements Object3DColor
{
private Segment3DColor seg;//unit mm
private double  speed;//mm/s

public ToolMove(Segment3DColor _seg,double _speed)
{
speed=_speed;
seg=_seg;
}

public Vecteur p1(){return seg.p1();}
public Vecteur p2(){return seg.p2();}

@Override
public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
{
seg.checkOccupyingCube(cornerMin,cornerMax);
	
}

@Override
public void draw(Graphics g, Projector proj) 
{
seg.draw(g, proj);
}
public void draw(Graphics g, Projector proj, Sun sun)
{
	this.draw(g,proj);
}

@Override
public double getDistanceToScreen(Projector proj) {
	// TODO Auto-generated method stub
	return 0;
}

public Segment3DColor getSegment3D() 
{
return seg;
}

public double getSpeed() {
	
	return speed;
}

/**move duraction in seconds*/
public double getDuration_s()
{
//System.out.println(" speed="+speed+" mm/min");
double dt=0;
if (speed!=0) dt=seg.getLength()/speed*60;
return dt;
}


public String toString()
{
return "Move from "+p1()+" to "+p2();
}

@Override
public boolean isOn(Point p, Projector proj)
{
// TODO Auto-generated method stub
return false;
}

@Override
public void setColor(Color c) {
	// TODO Auto-generated method stub
	
}



}


