package org.fablabsdr.cnc.flexible.xyz;

import java.awt.Dimension;
import java.io.File;
import javax.swing.JFileChooser;

import org.fablabsdr.cnc.flexible.turning.PanelTurning;

import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.io.STLFileFilter;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import nanoxml.XMLElement;

public class CNCApp2 extends WindowApp implements XMLstorable
{
	private PanelCNCxyx panelCNC=new PanelCNCxyx();
	//surfaces :
	private Signal2D1D finalSurface,surf1;

	public CNCApp2()
	{
		super("org_fablabsdr_cnc",false,true,true);

		this.putMenuFile(new XMLFileStorage(this), new CFileFilter("xml","cnc turning file") );
		this.getxMLFileStorage().addThingToStore(this);

		this.getCJFrame().setSize(1200, 800);

		//button to add STL
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"openSTL"));
			//tl_button.add(tl_update);
			CButton cButton=new CButton(tl_button);
			cButton.setPreferredSize(new Dimension(150, 20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/STLImport.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Import stl");
			this.getToolBar().add(cButton);
		}

	
		
		//3D view :
		this.add(panelCNC,"4 axis");
		//panelCNC.getMachine().getObject().readSTLinBinaryFile("/home/laurent/fabserver/fablab/proyectos/2022_09_29_cuillere_bois","quille3.stl");
		panelCNC.getMachine().getObject().readSTLinBinaryFile("/home/laurent/fabserver/cloud1/photonlyx/projets/2015_retrophane/papa","test_fraisage.stl");
		panelCNC.getRouter().calcMachineSpaceSurfaces();
		panelCNC.getBoxNbPasses().update();
		panelCNC.update3Dview();
		panelCNC.getRouter().updateSurfaceView();
		
	}



	public void openSTL()
	{
		JFileChooser df=new JFileChooser(path);
		STLFileFilter ff=new STLFileFilter();
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);
		int returnVal = df.showOpenDialog(null);
		String filename="";
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			path=df.getSelectedFile().getParent()+File.separator;
			filename=df.getSelectedFile().getName();

			panelCNC.getMachine().getObject().readSTLinBinaryFile(path,filename);

		}	
	}





	public static void main(String[] args)
	{
		CNCApp2 app =new CNCApp2();
	}



	@Override
	public XMLElement toXML() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void updateFromXML(XMLElement xml) {
		// TODO Auto-generated method stub
		
	}



}








