package org.fablabsdr.cnc.flexible.xyz;

import java.awt.Color;
import java.util.Vector;

import org.fablabsdr.cnc.flexible.Machine;

import com.photonlyx.toolbox.math.geometry.Box;
import com.photonlyx.toolbox.math.geometry.CylinderMesh;
import com.photonlyx.toolbox.math.geometry.Plane;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;

/**
 * a CNC machine with 3 translation axis X Y and Z
 */
public class MachineXYZ implements Machine
{
	//the object to machine:
	private TriangleMeshFrame object=new TriangleMeshFrame();
	private Box box=new Box(new Vecteur(0,-100/2,-50/2),100,100,50);
	//tool:
	private double xTool=0;
	private double yTool=0;
	private double zTool=0;
	private double toolDiameter=5;//tool diameter in mm
	private CylinderMesh tool=new CylinderMesh(toolDiameter,toolDiameter*3,30);

	//max z machined
	private double zmax=50;
	
	
	@Override
	public TriangleMeshFrame getObject() 
	{
		return object;
	}

	

	@Override
	public TriangleMeshFrame getStartingVolume() 
	{
		TriangleMeshFrame startingVolume;
		startingVolume=box;
		return startingVolume;
	}
	
	
	@Override
	public void convert2RealSpace(Vecteur out, Vecteur in) 
	{
		out.copy(in);
	}

	@Override
	public Signal2D1D calcsurfaceInMachineSpace(TriangleMeshFrame object, double dx, double dy) 
	{
		Vecteur cornerMin=new Vecteur();
		Vecteur cornerMax=new Vecteur();
		object.checkOccupyingCube(cornerMin, cornerMax);
		double xmin=cornerMin.x();
		double xmax=cornerMax.x();
		double ymin=cornerMin.y();
		double ymax=cornerMax.y();
		int overSampling=3;
		int dimx=(int)((xmax-xmin)/dx)*overSampling;
		int dimy=(int)((ymax-ymin)/dy)*overSampling;
		Signal2D1D surf=new Signal2D1D(xmin,xmax,ymin,ymax,dimx,dimy);
		double sx=surf.samplingx();
		double sy=surf.samplingy();
		//ChartWindow cw=new ChartWindow();
		Object3DColorSet topProfile=new Object3DColorSet();
		for (int j=0;j<dimy;j++)
		{
			double y=ymin+j*dy;
			Signal1D1DXY p=this.calcProfile(object,y,topProfile);
			//cw.add(p, ""+alpha);
			for (int i=0;i<dimx;i++)
			{
				double x=xmin+sx*i;
				double z=p.valueInterpol(x);
				surf.setVal(i, j, z);
			}
		}
		return surf;
	}


	/**
	 * 
	 * @param object object to machine
	 * @param topProfile for debug, to show the profile in a 3D sketch
	 * @return
	 */
	public  Signal1D1DXY calcProfile(TriangleMeshFrame object,double y,Object3DColorSet topProfile)
	{
		//get the boundary volume:
		Vecteur cornerMin=new Vecteur();
		Vecteur cornerMax=new Vecteur();
		object.checkOccupyingCube(cornerMin, cornerMax);
		double xmin=cornerMin.x();
		double xmax=cornerMax.x();
		double ymin=cornerMin.y();
		double ymax=cornerMax.y();


		topProfile.removeAllElements();
		Plane plane=new Plane(new Vecteur(0,y,0),new Vecteur(0,1,0));
		Vector<Segment3D> segs=object.getIntersectedSegments(plane);
		Vector<Segment3D> segs2=new Vector<Segment3D>();
		for (Segment3D seg:segs)  
		{
			if ((seg.p1().z()>0)&&(seg.p2().z()>0))
			{
				Segment3DColor s1=new Segment3DColor(seg,Color.RED);
				Point3DColor pp1=new Point3DColor(seg.p1(),Color.RED,3);
				Point3DColor pp2=new Point3DColor(seg.p2(),Color.RED,3);
				topProfile.add(s1);
				topProfile.add(pp1);
				topProfile.add(pp2);
				//double slope=(seg.p2().z()-seg.p1().z())/(seg.p2().x()-seg.p1().x());
				//if (Math.abs(slope)<1) 
				{
					//System.out.println(slope);
					segs2.add(seg);
				}
			}
		}
		//transform the segments in a curve:
		Signal1D1DXY sig=new Signal1D1DXY();
		for(Segment3D seg:segs2)
		{
			sig.addPoint(seg.p1().x(),seg.p1().z());
			sig.addPoint(seg.p2().x(),seg.p2().z());
		}
		sig._sort();
		//		System.out.println();
		//		System.out.println();
		//		System.out.println();
		//		System.out.println(sig);
		//clean:
		Signal1D1DXY sig2=new Signal1D1DXY();
		for (int i=0;i<sig.getDim()-1;i++)
		{
			double dx=sig.x(i+1)-sig.x(i);
			double dy=sig.y(i+1)-sig.y(i);
			//double slope=(dy/dx);
			//if (Math.abs(slope)<10) 
			if (dx>0.0001)
				sig2.addPoint(sig.x(i),sig.y(i));
			//sig.addPoint(seg.p2().x(),seg.p2().z());
		}
		//clean:
		if (sig2.x(0)>xmin)
		{
			//complete the profile down to xmin
			sig2.addPointBeforeIndex(xmin,sig2.y(0), 0);
		}
		if (sig2.x(sig2.getDim()-1)<xmax)
		{
			//complete the profile up to xmax
			sig2.addPoint(xmax,sig2.y(sig2.getDim()-1));
		}
		//correct the first value:
//		sig2.setValue(0,rmax);
//		sig2.setValue(sig2.getDim()-1,rmax);

		for (int i=0;i<sig2.getDim()-1;i++)
		{
			Vecteur p1=new Vecteur(sig2.x(i),0,sig2.y(i));
			Vecteur p2=new Vecteur(sig2.x(i+1),0,sig2.y(i+1));
			Point3DColor pp1=new Point3DColor(p1,Color.GREEN,5);
			Point3DColor pp2=new Point3DColor(p2,Color.GREEN,5);
			Segment3DColor ss=new Segment3DColor(p1,p2,Color.GREEN);
			topProfile.add(pp1);
			topProfile.add(pp2);
			topProfile.add(ss);
		}
		return sig2;		
	}

	public TriangleMeshFrame getTool() 
	{
		//  cnc  mill
		Vecteur toolPosReal=new Vecteur(xTool,yTool,zTool);
		tool.getFrame().setCentre(toolPosReal);
		tool.updateGlobal();
		return tool;
	}

	@Override
	public double getZmax() 
	{
		return zmax;
	}



	public double getxTool() {
		return xTool;
	}



	public void setxTool(double xTool) {
		this.xTool = xTool;
	}



	public double getyTool() {
		return yTool;
	}



	public void setyTool(double ytool) {
		this.yTool = ytool;
	}



	public double getzTool() {
		return zTool;
	}



	public void setzTool(double zTool) {
		this.zTool = zTool;
	}

}
