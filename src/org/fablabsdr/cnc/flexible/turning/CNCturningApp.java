package org.fablabsdr.cnc.flexible.turning;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.util.Vector;

import javax.swing.JFileChooser;

import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Plane;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.threeD.io.STLFileFilter;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import nanoxml.XMLElement;

public class CNCturningApp extends WindowApp implements XMLstorable
{
	private PanelTurning panelTurning=new PanelTurning();
	//surfaces :
	private Signal2D1D finalSurface,surf1;

	//public PanelTurning(WindowApp app)
	public CNCturningApp()
	{
		super("org_fablabsdr_turning_cnc",false,true,true);

		this.putMenuFile(new XMLFileStorage(this), new CFileFilter("xml","cnc turning file") );
		this.getxMLFileStorage().addThingToStore(this);

		this.getCJFrame().setSize(1200, 800);

		//button to add STL
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"openSTL"));
			//tl_button.add(tl_update);
			CButton cButton=new CButton(tl_button);
			cButton.setPreferredSize(new Dimension(150, 20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/STLImport.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Import stl");
			this.getToolBar().add(cButton);
		}

	
		
		//3D view :
		this.add(panelTurning,"4 axis");

		//		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		//		graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);

//debug starting surface:
//		Signal2D1D start=panelTurning.getMachine().calcsurfaceInMachineSpace(panelTurning.getMachine().getStartingVolume(), 3, 3);
//		Graph3DWindow gw=new Graph3DWindow();//to see the machine space surfaces
//		gw.getGraph3DPanel().add(start);

		panelTurning.getMachine().getObject().readSTLinBinaryFile("/home/laurent/fabserver/fablab/proyectos/2022_09_29_cuillere_bois","quille3.stl");
		panelTurning.getRouter().calcMachineSpaceSurfaces();
		panelTurning.getBoxNbPasses().update();
		panelTurning.update3Dview();
		panelTurning.getRouter().updateSurfaceView();
		
	}

//
///**
// * calc the surface after 1 pass
// */
//public void surface1()
//{
//	if (finalSurface==null) surface();
//	
//	Signal2D1D start=new Signal2D1D(finalSurface.xmin(),finalSurface.xmax(),finalSurface.ymin(),finalSurface.ymax(),finalSurface.dimx(),finalSurface.dimy());
//	start.setUniformValue(panelTurning.getDiameter()/2);
//	surf1=FourAxisTurningApp.calcsurface1(start,finalSurface,panelTurning.getDz());
//	//ChartWindow cw=new ChartWindow(surf1);
//	//Graph3DWindow w=new Graph3DWindow(surf1);
//	//gw.getGraph3DPanel().add(start);
//	gw.getGraph3DPanel().add(surf1);
//	//gw.getGraph3DPanel().initialiseProjectorAuto();
//	gw.updateView();
//}


	public void openSTL()
	{
		JFileChooser df=new JFileChooser(path);
		STLFileFilter ff=new STLFileFilter();
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);
		int returnVal = df.showOpenDialog(null);
		String filename="";
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			path=df.getSelectedFile().getParent()+File.separator;
			filename=df.getSelectedFile().getName();

			panelTurning.getMachine().getObject().readSTLinBinaryFile(path,filename);

		}	
	}





	public static void main(String[] args)
	{
		CNCturningApp app =new CNCturningApp();


		//		//create frame and add the panel 3D
		//		CJFrame cjf=new CJFrame();
		//		cjf.getContentPane().setLayout(new BorderLayout());
		//		
		//		
		//		PanelTurning pt=new PanelTurning();
		//		cjf.add(pt.getGraph3DPanel().getComponent());
		//		cjf.setSize(1200, 800);
		//		cjf.setVisible(true);
	}



	@Override
	public XMLElement toXML() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void updateFromXML(XMLElement xml) {
		// TODO Auto-generated method stub
		
	}




//
//	/**
//	 * calc the top profile of the object for the angle alpha
//	 * @param object
//	 * @param alpha
//	 * @return
//	 */
//	public static Signal1D1DXY calcProfile(TriangleMeshFrame object,double alpha)
//	{
//		Vecteur angles=new Vecteur(alpha/180*Math.PI,0,0);
//		object.getFrame().init();
//		object.getFrame()._rotate(angles);
//		object.updateGlobal();
//		
//		//Graph3DWindow w=new Graph3DWindow(object.getTriangles());
//		//object.getTriangles().saveToSTL("/home/laurent/temp/test"+"/"+"object"+alpha+".stl");
//
//		
//		Plane plane=new Plane(new Vecteur(0,0,0),new Vecteur(0,1,0));
//		Vector<Segment3D> segs=object.getIntersectedSegments(plane);
//		Vector<Segment3D> segs2=new Vector<Segment3D>();
//		for (Segment3D seg:segs)  
//		{
//			if ((seg.p1().z()>0)&&(seg.p2().z()>0))
//			{
//				//Segment3DColor s1=new Segment3DColor(seg,Color.RED);
//				//topProfile.add(s1);
//				double slope=(seg.p2().z()-seg.p1().z())/(seg.p2().x()-seg.p1().x());
//			if (Math.abs(slope)<1) 
//				{
//				//System.out.println(slope);
//				segs2.add(seg);
//				}
//			}
//		}
//		//transform the segments in a curve:
//		Signal1D1DXY sig=new Signal1D1DXY();
//		for(Segment3D seg:segs2)
//		{
//			sig.addPoint(seg.p1().x(),seg.p1().z());
//		}
//		sig._sort();	
//		return sig;
//	}
	
	
	
	
	
//	
//	/**
//	 * 
//	 * @param object object to machine
//	 * @param rmax radius max of the starting piece
//	 * @param alpha angle of rotation around X axis
//	 * @param topProfile for debug, to show the profile in a 3D sketch
//	 * @return
//	 */
//	public static Signal1D1DXY calcProfile(TriangleMeshFrame object,double rmax,double alpha, Object3DColorSet topProfile)
//	{
//		//get the boundary volume:
//		Vecteur cornerMin=new Vecteur();
//		Vecteur cornerMax=new Vecteur();
//		object.checkOccupyingCube(cornerMin, cornerMax);
//		double xmin=cornerMin.x();
//		double xmax=cornerMax.x();
//		double ymin=cornerMin.y();
//		double ymax=cornerMax.y();
//
//		Vecteur angles=new Vecteur(alpha/180*Math.PI,0,0);
//		object.getFrame().init();
//		object.getFrame()._rotate(angles);
//		object.updateGlobal();
//		
//		topProfile.removeAllElements();
//		Plane plane=new Plane(new Vecteur(0,0,0),new Vecteur(0,1,0));
//		Vector<Segment3D> segs=object.getIntersectedSegments(plane);
//		Vector<Segment3D> segs2=new Vector<Segment3D>();
//		for (Segment3D seg:segs)  
//		{
//			if ((seg.p1().z()>0)&&(seg.p2().z()>0))
//			{
//				Segment3DColor s1=new Segment3DColor(seg,Color.RED);
//				Point3DColor pp1=new Point3DColor(seg.p1(),Color.RED,3);
//				Point3DColor pp2=new Point3DColor(seg.p2(),Color.RED,3);
//				topProfile.add(s1);
//				topProfile.add(pp1);
//				topProfile.add(pp2);
//				//double slope=(seg.p2().z()-seg.p1().z())/(seg.p2().x()-seg.p1().x());
//			//if (Math.abs(slope)<1) 
//				{
//				//System.out.println(slope);
//				segs2.add(seg);
//				}
//			}
//		}
//		//transform the segments in a curve:
//		Signal1D1DXY sig=new Signal1D1DXY();
//		for(Segment3D seg:segs2)
//		{
//			sig.addPoint(seg.p1().x(),seg.p1().z());
//			sig.addPoint(seg.p2().x(),seg.p2().z());
//		}
//		sig._sort();
////		System.out.println();
////		System.out.println();
////		System.out.println();
////		System.out.println(sig);
//		//clean:
//		Signal1D1DXY sig2=new Signal1D1DXY();
//		for (int i=0;i<sig.getDim()-1;i++)
//		{
//			double dx=sig.x(i+1)-sig.x(i);
//			double dy=sig.y(i+1)-sig.y(i);
//			//double slope=(dy/dx);
//			//if (Math.abs(slope)<10) 
//			if (dx>0.0001)
//				sig2.addPoint(sig.x(i),sig.y(i));
//			//sig.addPoint(seg.p2().x(),seg.p2().z());
//		}
//		//clean:
//		if (sig2.x(0)>xmin)
//		{
//			//complete the profile down to xmin
//			sig2.addPointBeforeIndex(xmin,sig2.y(0), 0);
//		}
//		if (sig2.x(sig2.getDim()-1)<xmax)
//		{
//			//complete the profile up to xmax
//			sig2.addPoint(xmax,sig2.y(sig2.getDim()-1));
//		}
//		//correct the first value:
//		sig2.setValue(0,rmax);
//		sig2.setValue(sig2.getDim()-1,rmax);
//		
//		for (int i=0;i<sig2.getDim()-1;i++)
//		{
//			Vecteur p1=new Vecteur(sig2.x(i),0,sig2.y(i));
//			Vecteur p2=new Vecteur(sig2.x(i+1),0,sig2.y(i+1));
//			Point3DColor pp1=new Point3DColor(p1,Color.GREEN,5);
//			Point3DColor pp2=new Point3DColor(p2,Color.GREEN,5);
//			Segment3DColor ss=new Segment3DColor(p1,p2,Color.GREEN);
//			topProfile.add(pp1);
//			topProfile.add(pp2);
//			topProfile.add(ss);
//		}
//		return sig2;		
//	}
//	
//	/**
//	 * calc the uncurved surface
//	 * @return
//	 */
//	public static Signal2D1D calcsurface(TriangleMeshFrame object,double rmax)
//	{
//		Vecteur cornerMin=new Vecteur();
//		Vecteur cornerMax=new Vecteur();
//		object.checkOccupyingCube(cornerMin, cornerMax);
//		double xmin=cornerMin.x();
//		double xmax=cornerMax.x();
//		double alphaMin=0;
//		double alphaMax=360;
//		int dimx=50;
//		int dimy=50;
//		Signal2D1D surf=new Signal2D1D(xmin,xmax,alphaMin,alphaMax,dimx,dimy);
//		double sx=surf.samplingx();
//		double sy=surf.samplingy();
//		//ChartWindow cw=new ChartWindow();
//		Object3DColorSet topProfile=new Object3DColorSet();
//		for (int j=0;j<dimy;j++)
//		{
//			double alpha=alphaMin+j*sy;
//			Signal1D1DXY p=calcProfile(object,rmax,alpha,topProfile);
//			//cw.add(p, ""+alpha);
//			for (int i=0;i<dimx;i++)
//			{
//				double x=xmin+sx*i;
//				double z=p.valueInterpol(x);
//				surf.setVal(i, j, z);
//			}
//		}
//		return surf;
//	}
//	
//	/**
//	 * get the surface after first pass
//	 * @param current surface
//	 * @param dz z pass 
//	 * @return
//	 */
//	public static Signal2D1D calcsurface1(Signal2D1D currentSurface,Signal2D1D finalSurface,double dz)
//	{
//		Signal2D1D sig=new Signal2D1D(currentSurface.xmin(),currentSurface.xmax(),currentSurface.ymin(),currentSurface.ymax(),currentSurface.dimx(),currentSurface.dimy());
//		for (int i=0;i<currentSurface.dimx();i++)
//			for (int j=0;j<currentSurface.dimy();j++)
//			{
//				double z_current=currentSurface.getValue(i,j);
//				double z_to_cut=z_current-dz;
//				double zfinal=finalSurface.getValue(i,j);
//				sig.setValue(i,j,Math.max(z_to_cut, zfinal));
//			}
//		return sig;
//	}

}








