package org.fablabsdr.cnc.flexible.turning;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JPanel;

import org.fablabsdr.cnc.flexible.Machine;
import org.fablabsdr.cnc.flexible.Router;

import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ColorList;
import com.photonlyx.toolbox.gui.ParDoubleBox;
import com.photonlyx.toolbox.gui.ParIntBox;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.math.geometry.PolyLine3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.PolyLine3DColor;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class PanelTurning extends JPanel// implements XMLstorable
{
	private MachineXalphaR machine=new MachineXalphaR();
	//private MachineXYZ machine=new MachineXYZ();
	private Router router=new Router(machine);
	private Graph3DPanel graph3DPanel=new Graph3DPanel();
	private Object3DColorSet topProfile=new Object3DColorSet();
	private Object3DColorSet topProfileStart=new Object3DColorSet();


	private boolean seeStartingVolume=true;
	private boolean seeObject=true;
	private boolean seeProfile=false;
	private boolean seePathsZones=false;
	private boolean seeTool=true;
	private boolean seeFullPath=true;

	private int iPath=0;
	private ParamsBox boxPosTool;

	//paths on the real view:

	//
	//	//debug:
	//	private static Graph3DWindow gw=new Graph3DWindow();

	//gui:
	private ParamsBox boxNbPasses;


	public PanelTurning()
	{
		//graph3DPanel.getPanelSide().setPreferredSize(new Dimension(200,0));
		graph3DPanel.setAxesLength(300);

		this.setLayout(new BorderLayout());
		this.add(graph3DPanel.getComponent(),BorderLayout.CENTER);
		graph3DPanel.getPanelSide().setPreferredSize(new Dimension(200,0));



		//params
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"updateTopProfile"));
			tl.add(new Trigger(this,"update3Dview"));
			//			String[] names={"diameter","length","xTool","angle","rTool"};
			//			String[] units={"mm","mm","mm","deg","mm"};
			String[] names={"xTool","angle","rTool"};
			String[] units={"mm","deg","mm"};
			boxPosTool=new ParamsBox(machine,tl,null,names,units);
			((ParDoubleBox)boxPosTool.getParamBoxOfParam("angle")).setWheelAdd(true);
			((ParDoubleBox)boxPosTool.getParamBoxOfParam("angle")).setWheelCoef(15);
			((ParDoubleBox)boxPosTool.getParamBoxOfParam("xTool")).setWheelAdd(true);
			((ParDoubleBox)boxPosTool.getParamBoxOfParam("xTool")).setWheelCoef(1);
			((ParDoubleBox)boxPosTool.getParamBoxOfParam("rTool")).setWheelAdd(true);
			((ParDoubleBox)boxPosTool.getParamBoxOfParam("rTool")).setWheelCoef(1);
			//boxTimeMachining.setEditable(false);
			graph3DPanel.getPanelSide().add(boxPosTool.getComponent());
		}


		//params
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"updateObject"));
			tl.add(new Trigger(this,"updateTopProfile"));
			tl.add(new Trigger(this,"update3Dview"));
			String[] names={"seeStartVol","seeTool","seeObject"};
			String[] units={"","",""};
			//ParamsBox box=new ParamsBox(this,tl,null,names,units);
			ParamsBox box=new ParamsBox(this,tl,null,names,null,units,ParamsBox.VERTICAL,120,30);
			//boxTimeMachining.setEditable(false);
			graph3DPanel.getPanelSide().add(box.getComponent());
		}

		//params
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"updateObject"));
			tl.add(new Trigger(this,"updateTopProfile"));
			tl.add(new Trigger(this,"update3Dview"));
			String[] names={"seePathsZones","seeProfile","seeFullPath"};
			String[] units={"","",""};
			//ParamsBox box=new ParamsBox(this,tl,null,names,units);
			ParamsBox box=new ParamsBox(this,tl,null,names,null,units,ParamsBox.VERTICAL,120,30);
			//boxTimeMachining.setEditable(false);
			graph3DPanel.getPanelSide().add(box.getComponent());
		}





		//params
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(router,"reset"));
			String[] names={"dx","dy","dz"};
			String[] units={"mm","mm","mm"};
			ParamsBox box=new ParamsBox(router,tl,null,names,units);
			graph3DPanel.getPanelSide().add(box.getComponent());
		}


		//params
		{
			TriggerList tl=new TriggerList();
			String[] names={"nbPass","nbZones"};
			String[] units={"",""};
			boxNbPasses=new ParamsBox(router,tl,null,names,units);
			boxNbPasses.setEditable(false);
			graph3DPanel.getPanelSide().add(boxNbPasses.getComponent());
		}

		//params
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"update3Dview"));
			tl.add(new Trigger(router,"updateSurfaceView"));
			String[] names={"ipass"};
			String[] units={""};
			ParamsBox box=new ParamsBox(router,tl,null,names,units);
			graph3DPanel.getPanelSide().add(box.getComponent());
		}


		//		//button to see surface 0
		//		{
		//			TriggerList tl_button=new TriggerList();
		//			tl_button.add(new Trigger(this,"surface"));
		//			//tl_button.add(tl_update);
		//			CButton cButton=new CButton(tl_button);
		//			cButton.setPreferredSize(new Dimension(150, 20));
		//			cButton.setText("surf");
		//			graph3DPanel.getPanelSide().add(cButton);
		//		}
		//button to calc machine space surfaces
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(router,"calcMachineSpaceSurfaces"));
			tl_button.add(new Trigger(router,"updateSurfaceView"));
			tl_button.add(new Trigger(boxNbPasses,"update"));
			//tl_button.add(tl_update);
			CButton cButton=new CButton(tl_button);
			cButton.setPreferredSize(new Dimension(150, 20));
			cButton.setText("calc surfs");
			graph3DPanel.getPanelSide().add(cButton);
		}

		//button to start machining simulation:
		{
			TriggerList tl_button=new TriggerList();
			//tl_button.add(new Trigger(boxNbPasses,"update"));
			//tl_button.add(tl_update);
			CButton cButton=new CButton(tl_button);
			cButton.setPreferredSize(new Dimension(150, 20));
			cButton.setText("Simulate");
			graph3DPanel.getPanelSide().add(cButton);
		}

		//params
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"updateToolPos"));
			tl.add(new Trigger(this,"update3Dview"));
			tl.add(new Trigger(boxPosTool,"update"));
			String[] names={"iPath"};
			String[] units={""};
			ParamsBox box=new ParamsBox(this,tl,null,names,units);
			box.setEditable(true);
			((ParIntBox)box.getParamBoxOfParam("iPath")).setWheelIncrement(10);
			graph3DPanel.getPanelSide().add(box.getComponent());
		}
		

		//		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		//		graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);

		//add starting cylinder:
		update3Dview();


		graph3DPanel.initialiseProjectorAuto();

		//graph3DPanel.getPanelSide().validate();
		//graph3DPanel.getPanelSide().repaint();
	}




	public void updateObject()
	{
		Vecteur angles=new Vecteur(machine.getAngle()/180*Math.PI,0,0);
		machine.getObject().getFrame().init();
		machine.getObject().getFrame()._rotate(angles);
		machine.getObject().updateGlobal();

	}


	public void updateTopProfile()
	{
		machine.calcProfile(machine.getObject(),topProfile);
		machine.calcProfile(machine.getStartingVolume(),topProfileStart);
		for (Object3DColor o:topProfileStart) o.setColor(Color.BLUE);
	}

	public void updateToolPos()
	{
		if ((iPath<0)||(iPath>=router.getFullPath().getPolyLine3D().nbPoints())) return;
		Vecteur v=router.getFullPath().getPolyLine3D().getPoint(iPath);
		machine.setxTool(v.x());
		machine.setAngle(v.y());
		machine.setrTool(v.z());
	}

	public void update3Dview()
	{
		//		if (ipass>=paths.size()) ipass=router.getPaths().size()-1;
		//		if (ipass<0) ipass=0;
		graph3DPanel.removeAll3DObjects();
		if (seeStartingVolume) graph3DPanel.add(machine.getStartingVolume());
		if (seeTool) graph3DPanel.add(machine.getTool());
		if (seeObject) graph3DPanel.add(machine.getObjectRotated());
		if (seeProfile) 
		{
			graph3DPanel.addColorSet(topProfile);
			graph3DPanel.addColorSet(topProfileStart);
		}
		//add the path in the real 3D view
		ColorList cl=new ColorList("red green blue orange cyan");
		if (seePathsZones)
			if ((router.getIpass()>=0)&&(router.getIpass()<router.getPaths().size()))
				for (PolyLine3DColor pathsOfZone:router.getPaths().get(router.getIpass()))
				{
					PolyLine3DColor pol=new PolyLine3DColor();
					pol.setColor(cl.getNewJavaColor());
					for (Vecteur p:pathsOfZone.getPolyLine3D())
					{
						Vecteur p1=new Vecteur();
						machine.convert2RealSpace(p1, p);
						pol.getPolyLine3D().addPoint(p1);
					}
					graph3DPanel.add(pol);
				}
		if (seeFullPath)
		{
			PolyLine3DColor pol=new PolyLine3DColor();
			int c=0;
			for (Vecteur p:router.getFullPath().getPolyLine3D())
			{
				//if (c>(iPath-100))
				{
					Vecteur p1=new Vecteur();
					//p.setY(p1.y()+2*machine.getAngle());
					machine.convert2RealSpace(p1, p);
					pol.getPolyLine3D().addPoint(p1);
				}
				c++;
				if (c>iPath) break;
			}
			graph3DPanel.add(pol);
		}
		graph3DPanel.update();
	}



	public Graph3DPanel getGraph3DPanel() 
	{
		return graph3DPanel;
	}








	public Router getRouter() {
		return router;
	}









	public boolean isSeeStartVol() {
		return seeStartingVolume;
	}

	public void setSeeStartVol(boolean b) {
		this.seeStartingVolume = b;
	}

	public boolean isSeeObject() {
		return seeObject;
	}

	public void setSeeObject(boolean seeObject) {
		this.seeObject = seeObject;
	}

	public boolean isSeeProfile() {
		return seeProfile;
	}

	public void setSeeProfile(boolean seePath) {
		this.seeProfile = seePath;
	}

	public boolean isSeePathsZones() {
		return seePathsZones;
	}

	public void setSeePathsZones(boolean seePathsZones) {
		this.seePathsZones = seePathsZones;
	}

	public boolean isSeeTool() {
		return seeTool;
	}

	public void setSeeTool(boolean seeTool) {
		this.seeTool = seeTool;
	}

	public boolean isSeeFullPath() {
		return seeFullPath;
	}

	public void setSeeFullPath(boolean seeFullPath) {
		this.seeFullPath = seeFullPath;
	}




	public ParamsBox getBoxNbPasses() {
		return boxNbPasses;
	}




	public Machine getMachine() {
		// TODO Auto-generated method stub
		return machine;
	}




	public int getiPath() {
		return iPath;
	}




	public void setiPath(int iPath) {
		this.iPath = iPath;
	}


	//
	//
	//	public XMLElement toXML() 
	//	{
	//		XMLElement el = new XMLElement();
	//		el.setName(getClass().toString().replace("class ", ""));
	//		el.setAttribute("diameter",diameter);
	//		el.setAttribute("length",length);
	//		return el;
	//	}
	//
	//
	//	public void updateFromXML(XMLElement xml)
	//	{
	//		if (xml==null) 
	//		{
	//			return;
	//		}
	//		diameter=xml.getDoubleAttribute("diameter",20);
	//		length=xml.getDoubleAttribute("length",100);
	//	}

}








