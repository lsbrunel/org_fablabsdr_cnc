package org.fablabsdr.cnc.flexible.turning;

import java.awt.Color;
import java.util.Vector;

import org.fablabsdr.cnc.flexible.Machine;

import com.photonlyx.toolbox.chart.ChartWindow;
import com.photonlyx.toolbox.math.geometry.Box;
import com.photonlyx.toolbox.math.geometry.CylinderMesh;
import com.photonlyx.toolbox.math.geometry.Plane;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;

public class MachineXalphaR implements Machine
{
	//max radius machined
	private double rmax=50;

	//the object to machine:
	private TriangleMeshFrame object=new TriangleMeshFrame();

	//starting volume cylinder:
	private double diameter=70;//beginning cylinder diameter
	private double length=220;//beginning cylinder length
	private TriangleMeshFrame cyl=new CylinderMesh(diameter,length,30);

	//starting volume box:
	private double bx=200,by=80,bz=80;
	private Box box=new Box(new Vecteur(0,-by/2,-bz/2),bx,by,bz);
	//enum Start {START_CYLINDER,START_BOX}; //choice of starting volumes
	int START_CYLINDER=0;
	int START_BOX=1;
	private int start=1;
	
	private Signal2D1D startingSurface;

	//tool:
	private double xTool=0;
	private double angle=0;//angle of rotation of the lathe
	private double rTool=40;
	private double toolDiameter=5;//tool diameter in mm
	private CylinderMesh tool=new CylinderMesh(toolDiameter,toolDiameter*3,30);


	public MachineXalphaR()
	{
		//set the starting volume cylinder:
		Vecteur angles=new Vecteur(0,Math.PI/2,0);
		cyl.getFrame().init();
		cyl.getFrame()._rotate(angles);
		cyl.getFrame()._translate(new Vecteur(length,0,0));
		cyl.updateGlobal();
		
		//set the origin of the starting box to rotate correctly:
		box.setOrigin(new Vecteur(0,by/2,bz/2));
	}
	


	public TriangleMeshFrame getStartingVolume() 
	{
		TriangleMeshFrame startingVolume;
		if (start==START_CYLINDER) startingVolume=cyl;else startingVolume=box;
		// starting volume:
		Vecteur angles=new Vecteur(angle/180*Math.PI,0,0);
		startingVolume.getFrame().init();
		startingVolume.getFrame()._rotate(angles);
		startingVolume.updateGlobal();
		return startingVolume;
	}
	
	
	public TriangleMeshFrame getTool() 
	{
		//  cnc  mill
		Vecteur toolPosReal=new Vecteur(xTool,0,rTool);
		tool.getFrame().setCentre(toolPosReal);
		tool.updateGlobal();
		return tool;
	}
	
	
	/**
	 * calc the starting surface
	 */
	public Signal2D1D calcStartingSurface(double dx,double dy)
	{
		if (startingSurface==null) startingSurface=calcsurfaceInMachineSpace(this.getStartingVolume(),dx,dy);
		return startingSurface;
	}


	/**
	 * convert (x,alpha(deg),r) to (x,y,z)
	 * alpha is the angle in deg from z axis towards -y axis
	 * @param out
	 * @param in
	 */
	public void convert2RealSpace(Vecteur out,Vecteur in)
	{
		double x=in.x();
		double alpha=in.y();
		double r=in.z();
		double x1,y1,z1;//coordinates in real space
		x1=x;
		y1=-r*Math.sin(alpha*Math.PI/180.0);
		z1=r*Math.cos(alpha*Math.PI/180.0);
		out.copy(new Vecteur(x1,y1,z1));
	}




	/**
	 * 
	 * @param object object to machine
	 * @param topProfile for debug, to show the profile in a 3D sketch
	 * @return
	 */
	public  Signal1D1DXY calcProfile(TriangleMeshFrame object,Object3DColorSet topProfile)
	{
		double alpha=angle;
		//get the boundary volume:
		Vecteur cornerMin=new Vecteur();
		Vecteur cornerMax=new Vecteur();
		object.checkOccupyingCube(cornerMin, cornerMax);
		double xmin=cornerMin.x();
		double xmax=cornerMax.x();
		double ymin=cornerMin.y();
		double ymax=cornerMax.y();

		Vecteur angles=new Vecteur(alpha/180*Math.PI,0,0);
		object.getFrame().init();
		object.getFrame()._rotate(angles);
		object.updateGlobal();

		topProfile.removeAllElements();
		Plane plane=new Plane(new Vecteur(0,0,0),new Vecteur(0,1,0));
		Vector<Segment3D> segs=object.getIntersectedSegments(plane);
		Vector<Segment3D> segs2=new Vector<Segment3D>();
		for (Segment3D seg:segs)  
		{
			if ((seg.p1().z()>0)&&(seg.p2().z()>0))
			{
				Segment3DColor s1=new Segment3DColor(seg,Color.RED);
				Point3DColor pp1=new Point3DColor(seg.p1(),Color.RED,3);
				Point3DColor pp2=new Point3DColor(seg.p2(),Color.RED,3);
				topProfile.add(s1);
				topProfile.add(pp1);
				topProfile.add(pp2);
				//double slope=(seg.p2().z()-seg.p1().z())/(seg.p2().x()-seg.p1().x());
				//if (Math.abs(slope)<1) 
				{
					//System.out.println(slope);
					segs2.add(seg);
				}
			}
		}
		//transform the segments in a curve:
		Signal1D1DXY sig=new Signal1D1DXY();
		for(Segment3D seg:segs2)
		{
			sig.addPoint(seg.p1().x(),seg.p1().z());
			sig.addPoint(seg.p2().x(),seg.p2().z());
		}
		sig._sort();
		//		System.out.println();
		//		System.out.println();
		//		System.out.println();
		//		System.out.println(sig);
		//clean:
		Signal1D1DXY sig2=new Signal1D1DXY();
		for (int i=0;i<sig.getDim()-1;i++)
		{
			double dx=sig.x(i+1)-sig.x(i);
			double dy=sig.y(i+1)-sig.y(i);
			//double slope=(dy/dx);
			//if (Math.abs(slope)<10) 
			if (dx>0.0001)
				sig2.addPoint(sig.x(i),sig.y(i));
			//sig.addPoint(seg.p2().x(),seg.p2().z());
		}
		//clean:
		if (sig2.x(0)>xmin)
		{
			//complete the profile down to xmin
			sig2.addPointBeforeIndex(xmin,sig2.y(0), 0);
		}
		if (sig2.x(sig2.getDim()-1)<xmax)
		{
			//complete the profile up to xmax
			sig2.addPoint(xmax,sig2.y(sig2.getDim()-1));
		}
		//correct the first value:
//		sig2.setValue(0,rmax);
//		sig2.setValue(sig2.getDim()-1,rmax);

		for (int i=0;i<sig2.getDim()-1;i++)
		{
			Vecteur p1=new Vecteur(sig2.x(i),0,sig2.y(i));
			Vecteur p2=new Vecteur(sig2.x(i+1),0,sig2.y(i+1));
			Point3DColor pp1=new Point3DColor(p1,Color.GREEN,5);
			Point3DColor pp2=new Point3DColor(p2,Color.GREEN,5);
			Segment3DColor ss=new Segment3DColor(p1,p2,Color.GREEN);
			topProfile.add(pp1);
			topProfile.add(pp2);
			topProfile.add(ss);
		}
		return sig2;		
	}



	/**
	 * calc the uncurved surface
	 * @return
	 */
	public Signal2D1D calcsurfaceInMachineSpace(TriangleMeshFrame object,double dx,double dy)
	{
		Vecteur cornerMin=new Vecteur();
		Vecteur cornerMax=new Vecteur();
		object.checkOccupyingCube(cornerMin, cornerMax);
		double xmin=cornerMin.x();
		double xmax=cornerMax.x();
		double alphaMin=0;
		double alphaMax=360;
		int overSampling=3;
		int dimx=(int)((xmax-xmin)/dx)*overSampling;
		double dalpha=dy/(Math.PI*2*rmax)*360;
		int dimy=(int)((alphaMax-alphaMin)/dalpha)*overSampling;
		Signal2D1D surf=new Signal2D1D(xmin,xmax,alphaMin,alphaMax,dimx,dimy);
		double sx=surf.samplingx();
		double sy=surf.samplingy();
		//ChartWindow cw=new ChartWindow();
		Object3DColorSet topProfile=new Object3DColorSet();
		for (int j=0;j<dimy;j++)
		{
			double alpha=alphaMin+j*sy;
			this.setAngle(alpha);
			//System.out.println("alpha="+alpha+" calc profile");
			Signal1D1DXY p=this.calcProfile(object,topProfile);
			//cw.add(p, ""+alpha);
			for (int i=0;i<dimx;i++)
			{
				double x=xmin+sx*i;
				double z=p.valueInterpol(x);
				surf.setVal(i, j, z);
			}
		}
		this.setAngle(0);
		return surf;
	}

	

	
	public TriangleMeshFrame getObject() {
		return object;
	}

	
	public TriangleMeshFrame getObjectRotated() 
	{
		Vecteur angles=new Vecteur(angle/180*Math.PI,0,0);
		object.getFrame().init();
		object.getFrame()._rotate(angles);
		object.updateGlobal();
		return object;
	}


	
	public double getDiameter() 
	{
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}


	
	
	
	public double getLength() {
		return length;
	}


	public void setLength(double length) {
		this.length = length;
	}

	public double getxTool() {
		return xTool;
	}

	public void setxTool(double xTool) {
		this.xTool = xTool;
	}

	public double getrTool() {
		return rTool;
	}

	public void setrTool(double rTool) {
		this.rTool = rTool;
	}

	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) 
	{
		this.angle = angle;
	}

	@Override
	public double getZmax() 
	{
		return rmax;
	}
	
}
