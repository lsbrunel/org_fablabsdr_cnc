package org.fablabsdr.cnc.flexible.gui;

import java.awt.Color;
import java.util.Vector;

import com.photonlyx.toolbox.chart.ChartWindow;
import com.photonlyx.toolbox.gui.ColorList;
import com.photonlyx.toolbox.imageGui.ImageWindow;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.math.image.Zone;
import com.photonlyx.toolbox.math.image.Zones;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;
import com.photonlyx.toolbox.threeD.gui.PolyLine3DColor;
import com.photonlyx.toolbox.util.Messager;

public class Router 
{
	private Machine machine;
	//surfaces :
	private Signal2D1D finalSurface,currentSurface;
	private Vector<Signal2D1D> surfaces=new Vector<Signal2D1D>();
	//zones:
	private Vector<CImageProcessing> zones=new Vector<CImageProcessing>();
	//paths of each pass and each zone:
	private Vector<Vector<PolyLine3DColor>> paths=new Vector<Vector<PolyLine3DColor>>();
	//full path
	private PolyLine3DColor fullPath=new PolyLine3DColor();

	private int ipass=0;
	private int nbPassesMax=100;
	private double dx=10,dy=10,dz=10;//passes
	private double zstart=80;

	private static Graph3DWindow gw=new Graph3DWindow();//to see the machine space surfaces
	private static ChartWindow cw=new ChartWindow();//to see the surfaces
	private static ImageWindow iw=new ImageWindow();//to see the images with the zones



	public Router(Machine machine)
	{
		this.machine=machine;
		cw.getChartPanel().setPaletteVisible(true);
	}

	/**
	 * calc the final surface
	 */
	public void calcFinalSurface()
	{
		finalSurface=machine.calcsurfaceInMachineSpace(machine.getObject(),dx,dy);
		//ChartWindow cw=new ChartWindow(surf0);
		gw.getGraph3DPanel().add(finalSurface);
		gw.getGraph3DPanel().initialiseProjectorAuto();
		gw.updateView();
	}



	private CImageProcessing calcImageWithZones(Signal2D1D diff)
	{
		int w=diff.dimx();
		int h=diff.dimy();
		CImageProcessing cimp=new CImageProcessing(w,h);
		int[] pix=new int[w*h];
		for (int i=0;i<w;i++) 
			for (int j=0;j<h;j++) 
			{
				//if (diff.getValue(i, j)!=0) System.out.println(diff.getValue(i, j));
				if (diff.getValue(i, j)!=0) pix[i+j*w]=new Color(255,255,255).getRGB() ;
			}
		cimp.setPixelsDirect(pix);
		return cimp;
	}


	/**
	 * calc all the surfaces to machine at each pass:
	 */
	public void calcMachineSpaceSurfaces()
	{
		if (finalSurface==null) calcFinalSurface();

		//calc the starting surface:
		//		Signal2D1D start=new Signal2D1D(finalSurface.xmin(),finalSurface.xmax(),finalSurface.ymin(),finalSurface.ymax(),finalSurface.dimx(),finalSurface.dimy());
		//		start.setUniformValue(zstart);
		Signal2D1D start=machine.calcsurfaceInMachineSpace(machine.getStartingVolume(), dx, dy);

		surfaces.removeAllElements();
		paths.removeAllElements();
		surfaces.add(start);
		zones.removeAllElements();
		zones.add(new CImageProcessing());
		iw.removeAllPanels();
		Signal2D1D current=start;
		double absdiff=0;
		for(int i=0;i<nbPassesMax;i++)
		{
			//calc the next surface to machine:
			Signal2D1D nextSurf=Router.calcNextsurface(current,finalSurface,this.getDz());
			surfaces.add(nextSurf);

			//calc the image for segmentation:
			Signal2D1D diff=nextSurf.sub(current);
			absdiff=Math.abs(diff.integral());
			CImageProcessing cimp=calcImageWithZones(diff);
			zones.add(cimp);
			Zones zones=cimp.segmentation2(true);
			Vector<PolyLine3DColor> pathOfZone=new Vector<PolyLine3DColor>();
			paths.add(pathOfZone);

			//filter the zone size:
			Zones zones2=new Zones();
			for (Zone zone:zones)
			{
				if (zone.size()>20) zones2.add(zone);
			}
			System.out.println("\nPass "+i+"   "+zones2.size()+" zone(s)");

			ColorList cl=new ColorList("red green blue orange cyan");
			for (Zone zone:zones2)
			{
				//paint the zones:
				//Color c= cl.getNewJavaColor();
				//if (c==Color.BLACK) c=cl.getNewJavaColor();
				//cimp.paintZone(zone, c);
				//calc the paths in the zones:
				//Zone path=Router.calcPathOfZoneVertical(cimp,zone,3,3);
				int stepx=(int)(dx/diff.samplingx());
				int stepy=(int)(dy/diff.samplingy());
				Zones paths=new Zones();
				Router.calcPathsOfZoneHorizontal(paths,cimp,zone,stepx,stepy);
				//debug:
				//if (i==1)
				{
					for (Zone path:paths) System.out.println(paths.size()+" paths");
					//break;
				}
				for (Zone path:paths)
				{
					cimp.paintZone(path,cl.getNewJavaColor());
					//add the path of this zone:
					PolyLine3DColor pl=new PolyLine3DColor();
					pathOfZone.add(pl);
					double sx=(nextSurf.xmax()-nextSurf.xmin())/nextSurf.dimx();
					double sy=(nextSurf.ymax()-nextSurf.ymin())/nextSurf.dimy();
					for (int index:path)
					{
						int xim=path.x(index);
						int yim=path.y(index);
						double x=nextSurf.xmin()+sx*xim;
						double y=nextSurf.ymin()+sy*yim;
						double z=nextSurf.getValueInterpol(x, y);
						pl.getPolyLine3D().addPoint(new Vecteur(x,y,z));
					}
				}
			}
			//ImageWindow iw2=new ImageWindow(cimp);
			iw.add(cimp);

			//stop the loop when there is no more to machine:	
			//System.out.println(i+" diff="+diff);
			if (absdiff<0.001) break;
			current=nextSurf;
		}

		//build the full path:
		this.updateFullPath();

		if (absdiff>0.001)
		{
			Messager.messErr("More than "+nbPassesMax+" passes !!");
		}
	}


	/**
	 * get the surface after first pass
	 * @param current surface
	 * @param dz z pass 
	 * @return
	 */
	public static Signal2D1D calcNextsurface(Signal2D1D currentSurface,Signal2D1D finalSurface,double dz)
	{
		Signal2D1D sig=new Signal2D1D(currentSurface.xmin(),currentSurface.xmax(),currentSurface.ymin(),currentSurface.ymax(),currentSurface.dimx(),currentSurface.dimy());
		for (int i=0;i<currentSurface.dimx();i++)
			for (int j=0;j<currentSurface.dimy();j++)
			{
				double z_current=currentSurface.getValue(i,j);
				double z_to_cut=z_current-dz;
				double zfinal=finalSurface.getValue(i,j);
				sig.setValue(i,j,Math.max(z_to_cut, zfinal));
			}
		return sig;
	}

	/**
	 * gather the paths of the zones in the single path that will be sent to the machine
	 * @param fullPath
	 * @param paths
	 * @param zTravel
	 */
	public  void updateFullPath()
	{
		fullPath.getPolyLine3D().removeAllPoints();
		//build the full path
		double zTravel=machine.getZmax();
		Vecteur start=new Vecteur(0,0,machine.getZmax());
		Vecteur current=start;
		fullPath.getPolyLine3D().addPoint(start.copy());
		for (Vector<PolyLine3DColor> pathsOfPass:paths) 
			for (PolyLine3DColor pathOfZone:pathsOfPass) 
			{
				if (pathOfZone.getPolyLine3D().nbPoints()>0)
				{
					Vecteur firstPointOfZone=pathOfZone.getPolyLine3D().getPoint(0);
					//move along x:
					Vecteur v1=new Vecteur(firstPointOfZone.x(),current.y(),zTravel);
					fullPath.getPolyLine3D().addPoint(v1);
					//move along y:
					Vecteur v2=new Vecteur(firstPointOfZone.x(),firstPointOfZone.y(),zTravel);
					fullPath.getPolyLine3D().addPoint(v1);
					//machine the zone:
					fullPath.getPolyLine3D().add(pathOfZone.getPolyLine3D());
					//get the last point of the zone
					Vecteur last=pathOfZone.getPolyLine3D().getPoint(pathOfZone.getPolyLine3D().nbPoints()-1);
					//goes up :
					Vecteur nxt=new Vecteur(last.x(),last.y(),zTravel);
					fullPath.getPolyLine3D().addPoint(nxt);
					current=nxt;
				}
			}
	}







	/**
	 * calc the path for a zone on a pixel image with vertical rasper
	 * @param cimp
	 * @param zone
	 */
	public static Zone calcPathOfZoneVertical(CImageProcessing cimp,Zone zone,int dx,int dy)
	{
		int indexLeft=zone.getIndexOfMinX();
		int xmin=zone.x(indexLeft);
		int indexRight=zone.getIndexOfMaxX();
		int xmax=zone.x(indexRight);
		int nx=(xmax-xmin)/dx;
		//System.out.println("xmin ="+xmin+"  xmax="+xmax);
		Zone path=new Zone(cimp.w(),cimp.h());
		boolean down=true;//direction of path
		for (int i=0;i<nx;i++)
		{
			int x=xmin+i*dx;
			Zone vertical=zone.getPixelsOfX(x);
			int imin=vertical.getIndexOfMinY();
			int ymin=vertical.y(imin);
			int imax=vertical.getIndexOfMaxY(); 
			int ymax=vertical.y(imax);
			//			if (vertical.size()>0)
			//			{
			//				System.out.println("vertical of x="+x);
			//				System.out.println(vertical);
			//			}
			int ny=(ymax-ymin)/dy;
			int y;
			for (int j=0;j<ny;j++)
			{
				if (down) y=ymin+j*dy; else y=ymax-j*dy;
				Zone horizontal=vertical.getPixelsOfY(y);//horizontal should contain only one pixel
				//if (horizontal.size()!=0) System.out.println(horizontal);
				path.add(horizontal);
			}
			//end the path:
			{
				if (down) y=ymax; else y=ymin;
				Zone horizontal=vertical.getPixelsOfY(y);//horizontal should contain only one pixel
				//if (horizontal.size()!=0) System.out.println(horizontal);
				path.add(horizontal);
			}

			//invert the direction of the path
			down=!down;

			//			path.add(vertical);
		}
		return path;
	}



	public static void calcPathsOfZoneHorizontal(Zones paths,CImageProcessing cimp,Zone zone,int dx,int dy)
	{
		Zone path=calcPathOfZoneHorizontal(cimp,zone,dx,dy,true);
		if (path.size()==1) //the zone must be split in 2 zones
		{
			int x=path.x(path.elementAt(0));
			System.out.println("split vertical");
			Zones zs=zone.splitVertical(x);
			for (Zone z:zs)
			{
				//calcPathsOfZoneHorizontal(paths,cimp,z,dx,dy);
				paths.add(calcPathOfZoneHorizontal(cimp,z,dx,dy,false));
			}
		}
		else
		{
			paths.add(path);
		}
	}




	/**
	 * calc the path for a zone on a pixel image with horizontal rasper
	 * @param cimp
	 * @param zone
	 */
	public static Zone calcPathOfZoneHorizontal(CImageProcessing cimp,Zone zone,int dx,int dy,boolean checkBreaks)
	{
		int indexTop=zone.getIndexOfMinY();
		int yminZone=zone.y(indexTop);
		int indexBottom=zone.getIndexOfMaxY();
		int ymaxZone=zone.y(indexBottom);
		int ny=(ymaxZone-yminZone)/dx;
		//System.out.println("xmin ="+xmin+"  xmax="+xmax);
		Zone path=new Zone(cimp.w(),cimp.h());
		boolean right=true;//direction of path
		for (int i=0;i<ny;i++)
		{
			int y=yminZone+i*dy;
			Zone horizontal=zone.getPixelsOfY(y);
			int imin=horizontal.getIndexOfMinX();
			int xmin=horizontal.x(imin);
			int imax=horizontal.getIndexOfMaxX(); 
			int xmax=horizontal.x(imax);
			//			if (horizontal.size()>0)
			//			{
			//				System.out.println("horizontal of x="+x);
			//				System.out.println(horizontal);
			//			}
			int nx=(xmax-xmin)/dx;
			int x;
			for (int j=0;j<nx;j++)
			{
				if (right) x=xmin+j*dx; else x=xmax-j*dx;
				Zone vertical=horizontal.getPixelsOfX(x);
				//if (horizontal.size()!=0) System.out.println(horizontal);
				path.add(vertical);
				//check if path is broken:
				if (path.size()>2)
				{
					int i2=path.elementAt(path.size()-1);
					int i1=path.elementAt(path.size()-2);
					int x1=path.x(i1);
					int x2=path.x(i2);
					//System.out.println(x+" "+(x2-x1));
					if (checkBreaks) if (Math.abs(x2-x1)>dy) 
					{
						System.out.println("Zone broken");
						Zone z1=new Zone(cimp.getWidth(),cimp.getHeight());
						z1.add(i1);
						return z1;
					}
				}
			}
			//end the path:
			{
				if (right) x=xmax; else x=xmin;
				Zone vertical=horizontal.getPixelsOfX(x);
				//if (horizontal.size()!=0) System.out.println(horizontal);
				path.add(vertical);
			}

			//invert the direction of the path
			right=!right;

			//			path.add(vertical);
		}
		return path;
	}


	public Signal2D1D getFinalSurface() {
		return finalSurface;
	}

	public Vector<Signal2D1D> getSurfaces() {
		return surfaces;
	}

	public Vector<Vector<PolyLine3DColor>> getPaths() {
		return paths;
	}

	public PolyLine3DColor getFullPath() {
		return fullPath;
	}


	public int getIpass() {
		return ipass;
	}

	public void setIpass(int ipass) {
		this.ipass = ipass;
	}


	public int getNbPass() {
		return surfaces.size()-1;
	}

	public void setNbPass(int i) {

	}



	public double getZstart() {
		return zstart;
	}

	public void setZstart(double zstart) {
		this.zstart = zstart;
	}

	public double getDx() {
		return dx;
	}
	public void setDx(double dx) {
		this.dx = dx;
	}
	public double getDy() {
		return dy;
	}
	public void setDy(double dy) {
		this.dy = dy;
	}
	public double getDz() {
		return dz;
	}
	public void setDz(double dz) {
		this.dz = dz;
	}



	public int getNbZones() 
	{
		if ((ipass>=0)&&(ipass<paths.size()))
			return paths.get(ipass).size();
		else return 0;
	}

	public void setNbZones(int i) {

	}


	public void updateSurfaceView()
	{
		//		if (ipass>=router.getPaths().size()) ipass=router.getPaths().size()-1;
		//		if (ipass<0) ipass=0;
		gw.getGraph3DPanel().removeAll3DObjects();
		Router router=this;
		if ((router.getIpass()>=0)&&(router.getIpass()<router.getSurfaces().size()))
		{
			gw.getGraph3DPanel().add(router.getSurfaces().get(router.getIpass()));
		}
		ColorList cl=new ColorList("red green blue orange cyan");
		if ((router.getIpass()>=0)&&(router.getIpass()<router.getPaths().size()))
		{
			//add the paths
			for (PolyLine3DColor pathsOfZone:router.getPaths().get(router.getIpass())) 
			{
				pathsOfZone.setColor(cl.getNewJavaColor());
				gw.getGraph3DPanel().add(pathsOfZone);
			}
		}
		if ((router.getIpass()>=1)&&(router.getIpass()<router.getSurfaces().size()))
		{
			Signal2D1D surfBefore=router.getSurfaces().get(router.getIpass()-1);
			Signal2D1D surf=router.getSurfaces().get(router.getIpass());
			Signal2D1D diff=surf.sub(surfBefore);
			cw.getChart().setSignal2D1D(diff);
			cw.update();
		}
		else
		{
			cw.getChart().setSignal2D1D(new Signal2D1D());
			cw.update();
		}

		//add the full path:
		//gw.getGraph3DPanel().add(new PolyLine3DColor(fullPath,Color.RED));

		gw.updateView();

	}
	
	public void reset()
	{
	finalSurface=null;
	}
	
	

}
