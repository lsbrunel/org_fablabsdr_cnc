package org.fablabsdr.cnc.flexible.gui;

import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal2D1D;

public interface Machine 
{
	public TriangleMeshFrame getObject();
	public TriangleMeshFrame getStartingVolume();		
	public void convert2RealSpace(Vecteur out,Vecteur in);
	public Signal2D1D calcsurfaceInMachineSpace(TriangleMeshFrame object,double dx,double dy);
	public double getZmax();

}
