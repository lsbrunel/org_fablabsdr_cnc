package org.fablabsdr.cnc;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.math.function.Function1D2D;
import com.photonlyx.toolbox.math.function.Function1D2DTangent;
import com.photonlyx.toolbox.math.function.usual1D2D.ArcCircleCurve;
import com.photonlyx.toolbox.math.function.usual1D2D.CircleCurve;
import com.photonlyx.toolbox.math.geometry.Line2D;
import com.photonlyx.toolbox.math.geometry.Segment2D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.spline.Bspline;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.util.Messager;



public class CNCUtil 
{
	
	
//tables for displacement combinations
private static int max=50;
private static int[]  dxs=new int[max*max/2];
private static int[]  dys=new int[max*max/2];
private static int[]  pgcds=new int[max*max/2];
private static double[]  tans=new double[max*max/2];
private static int nbAngles;

//max length of message due to serial buffer
private static int maxArduinoMessageLength=60;

public static double SPEEDMIN=11;
public static double SPEEDMAX=280;
private static DecimalFormatSymbols dfs=new DecimalFormatSymbols(Locale.ENGLISH);
private static DecimalFormat df0=new DecimalFormat("0",dfs);
private static DecimalFormat df3=new DecimalFormat("0.000",dfs);

static Vector<Integer> primeNumbers(int max)
{
Vector<Integer> v=new Vector<Integer>();
for (int i=3;i<max;i++)
{
boolean flag=true;
for (int j=2;j<i;j++) if ((i/j)-((double)i/(double)j)==0) 
	{
	flag=false;
	break;
	}
if (flag) 
		{
		v.add(new Integer(i));
		//System.out.println(i);
		}
}
return v;
}



static int pgcd2(int a, int b) 
{
 if (b == 0) {
 return a;
 } else if (a >= b) {
 return pgcd2(a - b, b);
 } else {
	 return pgcd2(b, a);
 }
} 
 
 
/**
 * find a combination to move by dx and dy
 * @param stepX
 * @param stepY
 * @param _dx
 * @param _dy
 * @param comb
 */
public  static void findDxDy(double stepX,double stepY,double _dx,double _dy,double[] comb)
{
double[] combReduit=new double[3];

double abs_dx=Math.abs(_dx);
double abs_dy=Math.abs(_dy);
int sign_dx=(int)Math.signum(_dx);
int sign_dy=(int)Math.signum(_dy);

double dx,dy,mult;
//System.out.println("_dx="+_dx);
//System.out.println("_dy="+_dy);

if (abs_dx>=abs_dy) 
	{
	findcombination(stepX,stepY,abs_dx,abs_dy,combReduit);
	dx=combReduit[0]*sign_dx;
	dy=combReduit[1]*sign_dy;
	mult=combReduit[2];
	}
else 
	{
	findcombination(stepX,stepY,abs_dy,abs_dx,combReduit);
	dx=combReduit[1]*sign_dx;
	dy=combReduit[0]*sign_dy;
	mult=combReduit[2];
	}

//System.out.println("dx="+dx);
//System.out.println("dy="+dy);
//System.out.println("mult="+mult);
//System.out.println("dxfinal="+dx*mult*step);
//System.out.println("dyfinal="+dy*mult*step);

comb[0]=dx;
comb[1]=dy;
comb[2]=mult;
}
	
	

/**
 * find a combination to move by dx and dy, dx and dy must >0 and dx>dy
 * @param dx
 * @param dy
 * @param combReduit the combination calculated:array of 3 numbers: dx dy and pgcd
 * @param squareSize
 */
private static void findcombination(double stepX,double stepY,double _dx,double _dy,double[] combReduit)
{
//look for the closest combination  //TODO can be optimized by sorting
double tan=_dy/_dx;
double bestDiff=1e30;
int indexBestAngle=0;
for (int i=0;i<nbAngles;i++)
	{
	double  diff=Math.abs(tan-tans[i]);
	if (diff<bestDiff)
		{
		indexBestAngle=i;
		bestDiff=diff;
		}
	}
//System.out.println("dx="+_dx+" _dy="+_dy+" tan="+tan);
//System.out.println("best"+" dx="+dxs[indexBestAngle]+"dy="+dys[indexBestAngle]+" tan="+tans[indexBestAngle]);
		
combReduit[0]=dxs[indexBestAngle];
combReduit[1]=dys[indexBestAngle];
combReduit[2]=(_dx/stepX/dxs[indexBestAngle]);
}

/**
 * fill the 
 */
public  static void fillTables()
{
 //check all the possibles angles (tan)

int index=0;

		dxs[index]=1;
		dys[index]=0;
		pgcds[index]=1;		
		tans[index]=0;
		//System.out.println(dxs[index]+" "+dys[index]+" "+df3.format(tans[index]));
		index++;

for (int dx=1;dx<=max;dx++)
	for (int dy=1;dy<dx;dy++)
		{
		int pgcd=pgcd2(dx,dy);
		if ((pgcd>1)&(dx!=1)) continue;
		dxs[index]=dx/pgcd;
		dys[index]=dy/pgcd;
		pgcds[index]=pgcd;		
		tans[index]=(double)dy/(double)dx;
		//System.out.println(dxs[index]+" "+dys[index]+" "+df3.format(tans[index]));
		index++;
		}
 nbAngles=index-1;
}

/**
 * add a movement in (X,Y) plane of (_dx,_dy)
 * @param arduinoMessages
 * @param _dx
 * @param _dy
 * @param _stepX movement done by one step of the stepping motor in X
 * @param _stepY movement done by one step of the stepping motor in Y
 * @param trajectoireCNC
 */
public  static void addMovementXY(Vector<StringBuffer> arduinoMessages, double _dx,double _dy,double _stepX,double _stepY,Signal1D1DXY trajectoireCNC)
{
StringBuffer sb=arduinoMessages.lastElement();

if (sb.length()>maxArduinoMessageLength) 
	{
	sb=new StringBuffer();
	arduinoMessages.add(sb);
	}

double[] comb=new double[3];
CNCUtil.findDxDy(_stepX,_stepY,_dx, _dy,comb);
int dx=(int)comb[0];
int dy=(int)comb[1];
int mult=(int)comb[2];		
if (!((dx==0)&(dy==0))&&(mult!=0) )
	{
	sb.append("n"+mult);
	sb.append("x"+dx);
	sb.append("y"+dy);
	sb.append(";");

	//update the simulated trajectory:
	//get the last point
	double nx=trajectoireCNC.x(trajectoireCNC.getDim()-1);
	double ny=trajectoireCNC.y(trajectoireCNC.getDim()-1);
	//add the new points start with x move and ends with y movement:
	for (int j=0;j<mult;j++)
		{
		nx+=dx*_stepX;
		trajectoireCNC.addPoint(nx, ny);
		ny+=dy*_stepY;
		trajectoireCNC.addPoint(nx, ny);
		}
	 }

//go to the final point (to avoid accumulating errors with multi segments
//af("reste:");
double resteX=_dx-dx*mult*_stepX;
double resteY=_dy-dy*mult*_stepY;
CNCUtil.findDxDy(_stepX,_stepY,resteX, resteY,comb);
 dx=(int)(comb[0]*comb[2]);
 dy=(int)(comb[1]*comb[2]);
 mult=1;
 if (!((dx==0)&(dy==0))&&(mult!=0) )
	 {
	sb.append("n"+mult);
	sb.append("x"+dx);
	sb.append("y"+dy);
	sb.append(";");
	//update the simulated trajectory: 
	//get the last point
	double nx=trajectoireCNC.x(trajectoireCNC.getDim()-1);
	double ny=trajectoireCNC.y(trajectoireCNC.getDim()-1);
	nx+=dx*_stepX;
	trajectoireCNC.addPoint(nx, ny);
	// nx=trajectoireCNC.x(i)+mult*dx*step.val;
	ny+=dy*_stepY;
	trajectoireCNC.addPoint(nx, ny);
	}
// resteX=_dx-dx*mult*_step;
// resteY=_dy-dy*mult*_step;
		
}


public  static  void addMovementZ(Vector<StringBuffer> arduinoMessages, double _dz,double _stepZ)
{
int dz=(int)(_dz/_stepZ);
StringBuffer sb=arduinoMessages.lastElement();
if (sb.length()>maxArduinoMessageLength) 
	{
	sb=new StringBuffer();
	arduinoMessages.add(sb);
	}
if (dz!=0 ) sb.append("z"+dz+";");
}

public  static void addSpeed(Vector<StringBuffer> arduinoMessages, int speed)
{
StringBuffer sb=arduinoMessages.lastElement();
if (sb.length()>maxArduinoMessageLength) 
	{
	sb=new StringBuffer();
	arduinoMessages.add(sb);
	}
sb.append("s"+speed+";");	
}

/**
 * transform a curve (1D -> 2D) curve abscisse t from 0 to 1
 * @param curve
 * @param segmentLength the length of the elementary segments
 * @return
 */
public static Signal1D1D curve2Segments(Function1D2D curve,double segmentLength) 
{
Signal1D1DXY segments=new Signal1D1DXY();
Function1D2D tangent=new Function1D2DTangent(curve,false,0.000001);
double t=0.00000;
double[] P=curve.f(t);
segments.addPoint(P[0], P[1]);

for(;;)
	{
	double[] tan=tangent.f(t);
	System.out.println("tang="+tan[0]+" "+tan[1]);
	double normeTan=Math.sqrt(Math.pow(tan[0],2)+Math.pow(tan[1],2));
	double dt= segmentLength/normeTan;
	System.out.println("dt="+dt);
	t+=dt;
	if (t>1) t=1;
	P=curve.f(t);
	segments.addPoint(P[0], P[1]);
	if (t==1) break;
	}

return segments;
}



/**
 * convert speed in mm/min into time between 2 steps in microseconds
 * @param stepX
 * @param speedMmPerMin
 * @return
 */
public static int timeBetweenSteps_us(double stepX,double speedMmPerMin)
{
return (int)(stepX/2/speedMmPerMin*60*1000000);//TODO take all steps into account
}




/**
 * add a movement incremental dx dy dz in the toolPath
 * @param _dx
 * @param _dy
 * @param _dz
 * @param speed
 * @param toolPath3D
 */
public static void addMovementIncremental(double _dx,double _dy,double _dz,double speed,ToolPath toolPath3D)
{
Vecteur p1=toolPath3D.lastPoint();
Vecteur p=p1.copy();
p._add(_dx,_dy,_dz);
Vecteur p2=p;
float hue=-0.09f+(float)(speed-SPEEDMIN)/(float)(SPEEDMAX-SPEEDMIN)*0.78f;
if (hue>1) hue-=1;
Color c=Color.getHSBColor(hue,1.0f,1.0f);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,c.getRGB()),speed));
}

/**
 * add a movement toward absolute coordinates x y z in the toolPath
 * @param x
 * @param y
 * @param z
 * @param speed speed of the movement
 * @param toolPath3D path to add the move
 */
public static void addMovementAbsolute(double x,double y,double z,double speed,ToolPath toolPath3D)
{
Vecteur p1=toolPath3D.lastPoint();
Vecteur p2=new Vecteur(x,y,z);
float hue=-0.09f+(float)(speed-SPEEDMIN)/(float)(SPEEDMAX-SPEEDMIN)*0.78f;
if (hue>1) hue-=1;
Color c=Color.getHSBColor(hue,1.0f,1.0f);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,c.getRGB()),speed));
}




/**
 * transform a curve (t->(x,y)) into a polyLine
 * @param curve
 * @param segmentLength
 * @return
 */
public static Signal1D1DXY getSegments(Function1D2D curve,double segmentLength) 
{
Signal1D1DXY segments=new Signal1D1DXY();
Function1D2D tangent=new Function1D2DTangent(curve,false,0.000001);
double t=0.00000;
double[] P=curve.f(t);
segments.addPoint(P[0], P[1]);

for(;;)
	{
	double[] tan=tangent.f(t);
	double normeTan=Math.sqrt(Math.pow(tan[0],2)+Math.pow(tan[1],2));
	double dt= segmentLength/normeTan;
	//af("dt="+dt);
	t+=dt;
	if (t>1) t=1;
	P=curve.f(t);
	segments.addPoint(P[0], P[1]);
	if (t==1) break;
	}

return segments;
}



}
