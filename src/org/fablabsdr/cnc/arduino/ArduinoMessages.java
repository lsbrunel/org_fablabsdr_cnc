package org.fablabsdr.cnc.arduino;

public class ArduinoMessages 
{
 static  int maxLenght=30; //max nb of char in message before CR and LF
static int  endChar=59;  //ascii code end char for command , ascii code of ; is 59
static int   maxNbVals=10; //max 10 long values in a message !!!

static char[] message;//=new char[maxLenght];
static int messageLenght;
static int messageIndex;
static int motorSpeed;
static long[] vals=new long[maxNbVals];
static int nbVals=0;
static char[] tags=new char[maxNbVals];

public static int  decodeMessage(String _message,char[] _tags,long[] _vals)
{
message=(_message+" ").toCharArray();
messageLenght=message.length-1;
readNumValsInMessage();
for (int i=0;i<nbVals;i++) _tags[i]=tags[i];
for (int i=0;i<nbVals;i++) _vals[i]=vals[i];
 return nbVals;
 }

static void readNumValsInMessage()
    {
    messageIndex=0;
    int i=0;
    for (;;)
      {
      tags[i]=message[messageIndex];
      vals[i]=readNumValInMessage();
      if (messageIndex>=messageLenght) break;
      i++;
      }
    nbVals=i+1;
//    for (i=0;i<nbVals;i++) 
//      {
//		      System.out.print("val");
//		      System.out.print(i);
//		      System.out.print(" ");
//		      System.out.print(tags[i]);
//		      System.out.print("=");
//		      System.out.println(vals[i]);
//      }
    }
/**
read a number after a letter like "n12"
*/
static long readNumValInMessage()
    {
    long val=0;
    
    //explore the number length
    int i=messageIndex;
    i++; //jump the letter
//    Serial.print("numberStart ");
//    Serial.println(i, DEC);
    for (;;) 
      {
      char c=message[i++];
      if (! (isAFigure(c)||isASign(c))  ) break;
      if (i>=messageLenght+1) break;
      }
    int numberEnd=i-2;
//    Serial.print("numberEnd ");
//    Serial.println(numberEnd, DEC);
    
    //read the number
    int sign=+1;
    for (i=messageIndex+1;i<=numberEnd;i++)
      {
      char c=message[i];  
      if (isASign(c))
        {
        sign=signum(c);
//      Serial.print("  sign ");
//      Serial.println(sign, DEC);
        continue;
        }
      int exponent=numberEnd-i;
      long part=(c-48)*p10(exponent);
      val+=part;
//      Serial.print("  exponent ");
//      Serial.print(exponent, DEC);
//      Serial.print("  part ");
//      Serial.println(part, DEC);
      }
      val*=sign;
     messageIndex=numberEnd+1;//set the cursor 
//    Serial.print("Val ");
//    Serial.println(val, DEC);
    return val;
    }

//max 10^8
static long p10(int p)
{
long r=0;
switch (p){
case 0:
r=1;
break;
case 1:
r=10;
break;
case 2:
r=100;
break;
case 3:
r=1000;
break;
case 4:
r=10000;
break;
case 5:
r=100000;
break;
case 6:
r=1000000;
break;
case 7:
r=10000000;
break;
case 8:
r=100000000;
break;}
return r;
}

static boolean  isAFigure(char c)
{
return ((c>=48)&&(c<=57));
}

static boolean isASign(char c)
{
return ((c==43)||(c==45));
}

static int signum(char c)
{
if (c==43) return 1;
else if (c==45) return -1;
else return 0;
}



}
