package org.fablabsdr.cnc.gui;

import java.awt.Color;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;



/**
 * drawing of the cutter
 * @author laurent
 *
 */
public class CutterDrawing3D  extends Object3DColorSet
{
private Vector<Object3DColor> drawing3D=new Vector<Object3DColor>();
private Vecteur p=new Vecteur();//tool position
private double toolDiameter=4;

public CutterDrawing3D(double toolDiameter)
{
	this.toolDiameter=toolDiameter;
}

public void setPosition(Vecteur v)
{
p.affect(v);
}


	@Override
public Vector<Object3DColor> getObject3DList() 
{
Vecteur p1,p2;
//initialize the drawing 3D
drawing3D=new Vector<Object3DColor>();

Color color=Color.BLACK;
double h=toolDiameter*3;
int n=20;
//draw vertical lines:
for (int i=0;i<n;i++)
{
	double a=Math.PI*2*((double)i)/n;
	p1=p.addn(new Vecteur(toolDiameter/2*Math.cos(a),toolDiameter/2*Math.sin(a),0));
	p2=p1.addn(new Vecteur(0,0,h));
	drawing3D.add(new Segment3DColor(p1,p2,color));
}
//draw circles:
for (int i=0;i<n-1;i++)
{
	double a1=Math.PI*2*((double)i)/n;
	double a2=Math.PI*2*((double)(i+1))/n;
	p1=p.addn(new Vecteur(toolDiameter/2*Math.cos(a1),toolDiameter/2*Math.sin(a1),0));
	p2=p.addn(new Vecteur(toolDiameter/2*Math.cos(a2),toolDiameter/2*Math.sin(a2),0));
	drawing3D.add(new Segment3DColor(p1,p2,color));
	p1=p.addn(new Vecteur(toolDiameter/2*Math.cos(a1),toolDiameter/2*Math.sin(a1),h));
	p2=p.addn(new Vecteur(toolDiameter/2*Math.cos(a2),toolDiameter/2*Math.sin(a2),h));
	drawing3D.add(new Segment3DColor(p1,p2,color));
}
return drawing3D;
}



}
