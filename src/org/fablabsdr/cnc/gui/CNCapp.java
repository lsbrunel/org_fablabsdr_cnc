package org.fablabsdr.cnc.gui;


import java.awt.Dimension;
import java.io.File;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.fablabsdr.cnc.ToolPath;
import org.fablabsdr.cnc3D.CNC3D;

import com.photonlyx.toolbox.cad.DXFImport;
import com.photonlyx.toolbox.cad.SVGImport;
import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.InputDialog;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMesh;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.PNG_JPG_FileFilter;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.serial.gui.ManualControlPanel;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.threeD.io.STLFileFilter;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;

public class CNCapp extends WindowApp
{
	private CNC3D cnc3D=new CNC3D();
	private ChartPanel cp=new ChartPanel();//the chart panel for top view and editor
	//private Graph3DPanel graph3DPanelShape =new Graph3DPanel();//3D view of the surface
	private ToolPathPanel toolPathPanel=new ToolPathPanel(this,cnc3D);//3D view of the tool path
	private PlotSet entry=new PlotSet();//user defined polylines	
	private PlotSet curves=new PlotSet();//to show the curves like splines and circles	
	private Signal1D1DXY  trajectoireCNC=new Signal1D1DXY();	
	private ImageRaw_to_machine imageRaw_to_machine=new ImageRaw_to_machine(this,cp,cnc3D);
	private ParamsBox pbRawImage;
	private TriggerList tl_update=new TriggerList();
	private JPanel changingGui=new JPanel();
	//private Graph3DPanel cp_turning=new Graph3DPanel();//the chart panel for top view and editor


	public CNCapp()
	{
		super("org_fablabsdr_seeNsee",false,true,true);
		cp.setApp(this);

		this.getCJFrame().setSize(1200, 800);
		this.getToolBar().setPreferredSize(new Dimension(1,40));


		//tl_update.add(new Trigger(cnc3D,"add3Dshape2toolPath"));//calc the tool path
		//tl_update.add(new Trigger(this,"updateImage"));//update view of the shape as a float image
		tl_update.add(new Trigger(cp,"update"));//update editor view
		//tl_update.add(new Trigger(this,"updateGraph3DPanelShape"));
		tl_update.add(new Trigger(this,"calcCurves"));
		tl_update.add(new Trigger(toolPathPanel,"setnbMovesShownToMax"));
		tl_update.add(new Trigger(toolPathPanel,"updateView"));
		tl_update.add(new Trigger(toolPathPanel,"updateGcodeStreamerFile"));
		tl_update.add(new Trigger(toolPathPanel.getBoxTimeMachining(),"update"));//time of machining

		this.putMenuFile(new XMLFileStorage(this), new CFileFilter("xml","cnc file") );
		this.getxMLFileStorage().addThingToStore(entry);
		this.getxMLFileStorage().addThingToStore(cnc3D);
		this.getxMLFileStorage().addThingToStore(imageRaw_to_machine);
		this.getFileMenuTriggerList().add(tl_update);

		cp.getPanelSide().setPreferredSize(new Dimension(150,200));
		//cp_float_image.hidePanelSide();
		cp.getSplitPane().setDividerLocation(0.9);
		cp.getChart().setGridLinex(true);
		cp.getChart().setGridLiney(true);
		cp.getChart().setFormatTickx("0.000");
		cp.getChart().setFormatTicky("0.000");
		cp.getChart().setXlabel("");
		cp.getChart().setYlabel("");

		cp.getChart().add(entry);	
		cp.getChart().add(curves);	
		cp.getChart().add(new Plot(trajectoireCNC,"tool trajectory","green"));	
		cp.getChart().setKeepXYratio(true);
		cp.getChart().setSignal2D1D(cnc3D.getShape());

		cp.getChart().setMouseGripNearX(false);

		//button to recalc all
		{
			CButton cButton=new CButton(tl_update);
			cButton.setPreferredSize(new Dimension(150,20));
			//cButton.setIconFileName("org/fablabsdr/gui/cnc2D/AddPolyLine.png");
			//cButton.setContentAreaFilled(false);
			//cButton.setBorderPainted(false);
			cButton.setText("Update");
			this.getToolBar().add(cButton);
		}

		//button to add STL
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"openSTL"));
			tl_button.add(tl_update);
			CButton cButton=new CButton(tl_button);
			cButton.setPreferredSize(new Dimension(150, 20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/STLImport.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Import stl");
			cp.getPanelSide().add(cButton);
		}

		//button
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(imageRaw_to_machine,"importRawImageDialog"));
			tl_button.add(tl_update);
			CButton cButton=new CButton(tl_button);
			cButton.setPreferredSize(new Dimension(150, 20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/RawImport.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Import raw image");
			cp.getPanelSide().add(cButton);
		}

		//button
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"openJpgImage"));
			tl_button.add(tl_update);
			CButton cButton=new CButton(tl_button);
			cButton.setPreferredSize(new Dimension(150, 20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/PNGJPGImport.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Import jpg/png");
			cp.getPanelSide().add(cButton);
		}

		////button
		//{
		//TriggerList tl_button5=new TriggerList();
		//tl_button5.add(new Trigger(this,"saveImageAsSTL"));
		//CButton cButton5=new CButton(tl_button5);
		//cButton5.setText("Save image as STL");
		//cButton5.setPreferredSize(new Dimension(150, 20));
		//cp.getPanelSide().add(cButton5);
		//}

		//button
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"eraseImage"));
			tl_button.add(tl_update);
			CButton cButton=new CButton(tl_button);
			cButton.setText("Erase image");
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/Erase.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setPreferredSize(new Dimension(150, 20));
			cp.getPanelSide().add(cButton);
		}

		//button to add polyline
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"addPolyLine"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/AddPolyLine.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("add polyline");
			cp.getPanelSide().add(cButton);
		}


		//button to add circle
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"addCircle"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/AddCircle.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("add circle");
			cp.getPanelSide().add(cButton);
		}

		//button to add circle using 3 pts
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"addCircle3Pts"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/AddCircle3Pts.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("add circle 3 pts");
			cp.getPanelSide().add(cButton);
		}

		//button to add arc circle using 3 pts
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"addArcCircle"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/AddArcCircle.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("add arc");
			cp.getPanelSide().add(cButton);
		}


		//button to add spline
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"addSpline"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/AddSpline.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("add spline");
			cp.getPanelSide().add(cButton);
		}

		//button to add dxf
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"addDXF"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/dxf/DXFImport.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("add dxf");
			cp.getPanelSide().add(cButton);
		}


		//button to add svg
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"addSVG"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/svg/SVGImport.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("add svg");
			cp.getPanelSide().add(cButton);
		}


		//button to erase selected curve
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"eraseCurve"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/Erase.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Erase curve");
			cp.getPanelSide().add(cButton);
		}


		//button to set selected curve as engraving
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"setAsEngraving"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/SetAsEngraving.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Set as engraving");
			cp.getPanelSide().add(cButton);
		}

		//button to set selected curve as drilling
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"setAsDrilling"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/SetAsDrilling.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Set as Drilling");
			cp.getPanelSide().add(cButton);
		}


		//button to set selected curve as left
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"setContourAsLeft"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/SetAsContourLeft.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Set contour left");
			cp.getPanelSide().add(cButton);
		}



		//button to set selected curve as right
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"setContourAsRight"));
			tl_button3.add(tl_update);
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setIconFileName("org/fablabsdr/cnc/gui/cnc2D/SetAsContourRight.png");
			cButton.setContentAreaFilled(false);
			cButton.setBorderPainted(false);
			cButton.setText("Set contour right");
			cp.getPanelSide().add(cButton);
		}

		changingGui.setBackground(Global.background);
		cp.getPanelSide().add(changingGui);
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"updateGui"));
			CButton cButton=new CButton(tl_button3);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setText("updateGui");
			this.getToolBar().add(cButton);
		}

		updateGui();

		tl_update.add(new Trigger(pbRawImage,"update"));
		tl_update.add(new Trigger(this,"updateGui"));



		this.add(cp,"Editor");

		//3D view of the shape:
		//this.add(graph3DPanelShape.getJPanel(),"Surface 3D");

		//3D view of the tool path
		this.add(toolPathPanel,"Tool path");

		//machine manual control:
		this.add(new ManualControlPanel(),"Manual control");
		//this.add(cp_turning.getMainPanel(),"Turning");


		//this.add(new CNC2Dpanel(),"CNC 2D");

		//jframe.setVisible(true);


		//debug
		//String s=TextFiles.readFile("/home/laurent/temp/untitled.stl").toString();
		//Triangle3DSet set=readSTL(s);
		//graph3DPanelShape.removeAll3DObjects();
		//graph3DPanelShape.add(set);
		//graph3DPanelShape.initialiseProjector();
		//graph3DPanelShape.repaint();
		//Signal2D1D im=convertTriangles2Image(set,100,100);
		//cp.getChart().setSignal2D1D(im);

	}


	//
	//public void updateGraph3DPanelShape()
	//{
	//System.out.print("update 3D view ...");
	//graph3DPanelShape.removeAll3DObjects();
	//graph3DPanelShape.add(cp.getChart().getSignal2D1D());
	//graph3DPanelShape.initialiseProjectorAuto();
	//graph3DPanelShape.repaint();
	//System.out.println("... done");
	//}

	//public void updateImage()
	//{
	//cp.getChart().setSignal2D1D(cnc3D.getShape());
	//}

	public void openSTL()
	{
		JFileChooser df=new JFileChooser(this.path);
		STLFileFilter ff=new STLFileFilter();
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);
		int returnVal = df.showOpenDialog(null);
		String filename="";
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			this.path=df.getSelectedFile().getParent()+File.separator;
			filename=df.getSelectedFile().getName();
			String s=TextFiles.readFile(this.path+filename).toString();
			TriangleMesh set=readSTL(s);
			//	graph3DPanelShape.removeAll3DObjects();
			//	graph3DPanelShape.add(set);
			//	graph3DPanelShape.initialiseProjectorAuto();
			//	graph3DPanelShape.repaint();


			InputDialog inputDialog=new InputDialog();
			inputDialog.setFieldLabel("Resolution (pixels)");
			inputDialog.setInitialFieldValue("200");
			inputDialog.show();
			int dim= Integer.parseInt(inputDialog.getStringAnswer());	

			Signal2D1D im=convertTriangles2Image(set,dim,dim);
			cnc3D.getShape().copy(im);
			//	cnc3D.setxSize(im.xmax()-im.xmin());
			//	cnc3D.setySize(im.ymax()-im.ymin());
			//	cnc3D.setzSize(im.zmax()-im.zmin());
		}	
	}

	public void eraseImage()
	{
		imageRaw_to_machine.setFilename("");
		cnc3D.resetShape();
	}




	public void openJpgImage()
	{
		JFileChooser df=new JFileChooser(this.path);
		PNG_JPG_FileFilter ff=new PNG_JPG_FileFilter();
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);
		int returnVal = df.showOpenDialog(null);
		String filename="";
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			this.path=df.getSelectedFile().getParent()+File.separator;
			filename=df.getSelectedFile().getName();
			Signal2D1D shape=new Signal2D1D();

			//ask for the scale for XY 
			InputDialog inputDialog=new InputDialog();
			inputDialog.setFieldLabel("Image width (mm) ? ");
			inputDialog.setInitialFieldValue("50");
			inputDialog.show();
			//double scaleXY=new Double(inputDialog.getStringAnswer()); //mm/pix
			double w=Double.parseDouble(inputDialog.getStringAnswer());


			//ask for the scale for  Z
			InputDialog inputDialog2=new InputDialog();
			inputDialog2.setFieldLabel("Depth in z (mm)? ");
			inputDialog2.setInitialFieldValue("3");
			inputDialog2.show();
			//double scaleZ=new Double(inputDialog2.getStringAnswer());
			double dz=Double.parseDouble(inputDialog2.getStringAnswer());

			shape.loadFromImage(new CImage(this.path,filename,false));
			double scaleXY=w/shape.dimx();
			shape.setxmin(0);
			shape.setxmax(shape.dimx()*scaleXY);
			shape.setymin(0);
			shape.setymax(shape.dimy()*scaleXY);
			shape._multiply(dz/(shape.zmax()-shape.zmin()));
			shape._flipAroundX();

			//shape._resample(sampx);
			cp.getChart().setSignal2D1D(shape);
			cnc3D.getShape().copy(shape);
		}	


	}



	private TriangleMesh readSTL(String stl)
	{
		TriangleMesh set=new TriangleMesh();
		String[] lines=StringSource.readLines(stl);
		int lineNb=0;
		Definition def;
		if (new Definition(lines[lineNb]).word(0).compareTo("solid")!=0)
		{
			Messager.messErr("Can't find solid in first line");
			return null;
		}
		lineNb++;//go to 1rst facet
		while (new Definition(lines[lineNb]).word(0).compareTo("endsolid")!=0)
		{
			if (new Definition(lines[lineNb]).word(0).compareTo("facet")!=0)
			{
				Messager.messErr("Line "+lineNb+" Can't find facet in:"+lines[lineNb]);
				return null;
			}
			lineNb++;//go to outer loop
			lineNb++;//go to 1rst vertex
			Vecteur[] p=new Vecteur[3];
			int index=0;
			while ((def=new Definition(lines[lineNb])).word(0).compareTo("endloop")!=0)
			{
				double x=new Double(def.word(1)).doubleValue();
				double y=new Double(def.word(2)).doubleValue();
				double z=new Double(def.word(3)).doubleValue();
				p[index]=new Vecteur(x,y,z);
				index++;
				lineNb++;//go to next vertex or to end loop
			}
			lineNb++;//go to endfacet
			Triangle3DColor t=new Triangle3DColor(p[0],p[1],p[2]);
			set.add(t);
			lineNb++;//go to facet or endsolid
			//System.out.println(lineNb+" "+lines[lineNb]);
		}
		return set;
	}



	private Signal2D1D convertTriangles2Image(TriangleMesh set,int dimx, int dimy)
	{
		Vecteur[] cube=set.getOccupyingCube();
		double xmin=cube[0].x();
		double xmax=cube[1].x();
		double ymin=cube[0].y();
		double ymax=cube[1].y();
		Signal2D1D 	im=new Signal2D1D(xmin,xmax,ymin,ymax,dimx,dimy);
		double sx=(im.xmax()-im.xmin())/(im.dimx()-1);
		double sy=(im.ymax()-im.ymin())/(im.dimy()-1);
		for (int i=0;i<im.dimx();i++)
			for (int j=0;j<im.dimy();j++)
			{
				double x=xmin+i*sx;
				double y=ymin+j*sy;
				TriangleMesh ts=set.getTrianglesSuchProjectionIcontainsPoint(x,y);
				if (ts!=null)
				{
					Triangle3D t=ts.getTriangle(0);
					Vecteur Pplane=t.p1();
					Vecteur nplane=t.getNormal();
					Vecteur Pline=new Vecteur(x,y,0);
					Vecteur nline=new Vecteur(0,0,1);
					Vecteur intersect=Vecteur.intersectionPlaneLine(Pplane,nplane, Pline,nline);
					im.setValue(i, j, intersect.z());
					//im.setValue(i, j, 1);
				}
			}
		return im;
	}


	public void  saveImageAsSTL()
	{
		Signal2D1D im=cp.getChart().getSignal2D1D();
		//Graph3DPanel graph3DPanel =new Graph3DPanel();
		//graph3DPanel.add(im);
		//graph3DPanel.saveTrianglesAndQuadrilatersAsSTL();


		TriangleMesh set=new TriangleMesh();
		TriangleMesh.convert(im,set,0,true);
		set.saveToSTLdialog();

	}




	public void calcCurves()
	{	
		cnc3D.getToolPath().clean();	

		//set the shape in cnc3D:
		imageRaw_to_machine.readShapeFile();

		//add image sculpting:	
		cnc3D.add3Dshape2toolPath();

		trajectoireCNC.init(0);
		curves.removeAllPlots();

		if (entry.nbPlots()!=0) 
		{	
			//eventually process the curves to polylines:
			PlotSet segments=cnc3D.curves2polyLines(entry,curves);

			if (segments.nbPlots()==0) return;

			//transform into a contour cut
			PlotSet contours=cnc3D.buildContourTrajectories(segments);

			//this.getChart().add(contours);

			//transform the polylines into a unique cutter trajectory, making the passes and adding transfers
			cnc3D.mergeTrajectories(contours);

		}

		//Object3DSet drawing3D=cnc3D.calcObject3DList() ;
		//Graph3DWindow g3dw=new Graph3DWindow(drawing3D);

		//calculate the G code
		//String gcode=CNC3D.calculateGcode(toolPath3D,true);
		//System.out.println(gcode);
		updateToolTrajectory(cnc3D.getToolPath(),trajectoireCNC);

		//calculate the W code
		//wcode.val=CNCUtil.gcode2wcode(trajectoireCNC, gcode.val, stepX.val,stepY.val,stepZ.val);

		//String filename=Global.path+"seeNsee2D.gcode";
		//TextFiles.saveString(filename, gcode);
		//System.out.println(filename+" saved");

		cnc3D.calcMachiningTime();

		//calculate the G code
		cnc3D.calculateGcode();

		//System.out.println(toolPath);//debug

		//System.out.println("... done");

		String filename=this.path+CNC3D.getGcodeFileName();
		TextFiles.saveString(filename, cnc3D.getGcode());
		System.out.println(filename+" saved");

	}





	public void updateToolTrajectory(ToolPath toolPath3D,Signal1D1DXY  trajectoireCNC)
	{
		trajectoireCNC.init(0);
		for (int i=0;i<toolPath3D.size();i++)
		{
			Segment3D seg=toolPath3D.elementAt(i).getSegment3D();
			trajectoireCNC.addPoint(seg.p1().x(),seg.p1().y());
		}
		Segment3D seg=toolPath3D.elementAt(toolPath3D.size()-1).getSegment3D();
		trajectoireCNC.addPoint(seg.p2().x(),seg.p2().y());

	}




	public void addPolyLine()
	{
		Signal1D1DXY signal=new Signal1D1DXY();
		signal.addPoint(0, 0);
		signal.addPoint(10, 10);
		signal.addPoint(0, 20);
		Plot plot=entry.add(signal);
		plot.setLabel("z -1 contour right");
		plot.setDotSize(5);
		plot.setLineThickness(2);
	}



	public void addArcCircle()
	{
		Signal1D1DXY signal=new Signal1D1DXY();
		signal.addPoint(0, 0);
		signal.addPoint(10, 10);
		signal.addPoint(0, 20);
		Plot plot=entry.add(signal);
		plot.setLabel("z -1 3pointsArcCircle contour right");
		plot.setDotSize(5);
		plot.setLineThickness(2);
	}



	public void addCircle()
	{
		Signal1D1DXY signal=new Signal1D1DXY();
		signal.addPoint(0, 0);
		signal.addPoint(10, 0);
		Plot plot=entry.add(signal);
		plot.setLabel("z -1 fullCircle contour right");
		plot.setDotSize(5);
		plot.setLineThickness(2);
	}



	public void addCircle3Pts()
	{
		Signal1D1DXY signal=new Signal1D1DXY();
		signal.addPoint(0, 0);
		signal.addPoint(10, 10);
		signal.addPoint(0, 20);
		Plot plot=entry.add(signal);
		plot.setLabel("z -1 3pointsCircle contour right");
		plot.setDotSize(5);
		plot.setLineThickness(2);
	}



	public void addSpline()
	{
		Signal1D1DXY signal=new Signal1D1DXY();
		signal.addPoint(0, 0);
		signal.addPoint(0, 10);
		signal.addPoint(20, 10);
		signal.addPoint(20, 20);
		signal.addPoint(10, 20);
		Plot plot=entry.add(signal);
		plot.setLabel("z -1 spline contour right");
		plot.setDotSize(5);
		plot.setLineThickness(2);
	}

	public void addDXF()
	{
		PlotSet plots = DXFImport.importdxf(this.path);
		entry.add(plots);
	}


	public void addSVG()
	{
		PlotSet plots = SVGImport.importsvg(this.path);
		entry.add(plots);
	}


	public void eraseCurve()
	{
		if (entry.nbPlots()==1)
		{
			entry.removePlot(entry.getPlot(0));
		}
		else
		{
			Plot[] ps=entry.getSelectedPlots();
			if (ps!=null) 
			{
				for (Plot p:ps) entry.removePlot(p);
			}
		}
	}



	public void setContourAsLeft()
	{
		Plot[] plots=entry.getSelectedPlots();
		if (plots!=null) for (int i=0;i<plots.length;i++) 
		{
			String l=plots[i].getLabel();
			l=l.replace("drilling", "contour left");
			l=l.replace("engraving", "contour left");
			if (l.contains("contour")) l=l.replace("right", "left");
			if (!l.contains("contour")) l+=" contour left";
			//System.out.println(l);
			plots[i].setLabel(l);
			plots[i].setLineThickness(2);
			plots[i].setDotSize(5);
		}

	}



	public void setContourAsRight()
	{
		Plot[] plots=entry.getSelectedPlots();
		if (plots!=null) for (int i=0;i<plots.length;i++) 
		{
			String l=plots[i].getLabel();
			l=l.replace("drilling", "contour right");
			l=l.replace("engraving", "contour right");
			if (l.contains("contour")) l=l.replace("left", "right");
			if (!l.contains("contour")) l+=" contour right";
			//System.out.println(l);
			plots[i].setLabel(l);
			plots[i].setLineThickness(2);
			plots[i].setDotSize(5);
		}

	}



	public void setAsEngraving()
	{
		Plot[] plots=entry.getSelectedPlots();
		if (plots!=null) for (int i=0;i<plots.length;i++) 
		{
			String l=plots[i].getLabel();
			l=l.replace("drilling", "engraving");
			l=l.replace("contour", "engraving");
			l=l.replace("right", "");
			l=l.replace("left", "");
			if (!l.contains("engraving")) l+=" engraving";
			plots[i].setLabel(l);
			plots[i].setLineThickness(2);
			plots[i].setDotSize(5);
		}

	}


	public void setAsDrilling()
	{
		Plot[] plots=entry.getSelectedPlots();
		if (plots!=null) for (int i=0;i<plots.length;i++) 
		{
			String l=plots[i].getLabel();
			l=l.replace("engraving", "drilling");
			l=l.replace("right", "");
			l=l.replace("left", "");
			l=l.replace("contour", "drilling");
			if (!l.contains("drilling")) l+=" drilling";
			plots[i].setLabel(l);
			plots[i].setLineThickness(1);
			plots[i].setDotSize(10);
		}

	}
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		//System.setProperty("java.library.path", "/usr/lib/jni");
		CNCapp cNC3D_app =new CNCapp();

	}



	public void updateGui()
	{
		changingGui.removeAll();
		//
		//
		////button to update the gcode calculation
		//{
		//TriggerList tl=new TriggerList();
		//tl.add(new Trigger(cnc3D,"update"));//calc the tool path
		//tl.add(new Trigger(this,"updateImage"));//update view of the shape as a float image
		//tl.add(new Trigger(cp,"update"));//update view of the shape as a float image
		//tl.add(new Trigger(toolPathPanel,"updateView"));
		//tl.add(new Trigger(toolPathPanel,"updateGcodeStreamerFile"));
		//tl.add(new Trigger(toolPathPanel.getBoxTimeMachining(),"update"));//time of machining
		//CButton cButton=new CButton(tl);
		//cButton.setPreferredSize(new Dimension(120,20));
		//cButton.setText("Update");
		//this.getToolBar().add(cButton);
		//}


		if (imageRaw_to_machine.getFilename().length()!=0)
		{
			String[] names1={"width","depth"};
			String[] units1={"mm","mm"};
			pbRawImage=new ParamsBox(imageRaw_to_machine,null,"Raw image:",names1,null,units1,ParamsBox.VERTICAL,120,30);
			changingGui.add(pbRawImage);
			changingGui.setPreferredSize(new Dimension(120,30*3));
			changingGui.validate();
			changingGui.repaint();
			cp.getPanelSide().validate();
			cp.getPanelSide().repaint();			
		}
	}

}
