package org.fablabsdr.cnc.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.fablabsdr.cnc3D.CNC3D;

import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.TaskButton2;
import com.photonlyx.toolbox.gui.TextEditor;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.serial.grbl.GRBLstreamer;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.io.GCODEFileFilter;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;

public class ToolPathPanel extends JPanel
{
	//private ToolPath toolPath;
	private Graph3DPanel graph3DPanelToolPath =new Graph3DPanel();//3D view of the surface
	private JPanel panelSide;
	private int nbMovesShown=10000;
	private JPanel buttonsPanel=new JPanel();
	private ParamsBox boxTimeMachining;
	//private SimpleJavaGrblStreamer grblStreamer;
	private GRBLstreamer grblStreamer2;
	private CNC3D cnc3D;
	private WindowApp app;
	private ParamsBox boxSavedParams;
	private ParamsBox boxNbMovesShown;
	
	
	public ToolPathPanel(WindowApp app,CNC3D cnc3D)
	{
		this.app=app;
		this.cnc3D=cnc3D;
		//this.toolPath=toolPath;

		this.setLayout(new BorderLayout());

		//panel for button and legend
		panelSide=new JPanel();
		panelSide.setPreferredSize(new Dimension(200, 200));
		panelSide.setLayout(new FlowLayout());
		panelSide.setBackground(Global.background);
		//add the side panel at right
		this.add(panelSide,BorderLayout.WEST);

		this.add(graph3DPanelToolPath.getComponent(),BorderLayout.CENTER);

		buttonsPanel.setPreferredSize(new Dimension(150, 50));
		buttonsPanel.setLayout(new FlowLayout());
		buttonsPanel.setBackground(Global.background);
		this.add(buttonsPanel,BorderLayout.NORTH);

		TriggerList tl_update=new TriggerList();
		tl_update.add(new Trigger(this,"updateView"));//update view of the shape as a float image


		//grblStreamer=new SimpleJavaGrblStreamer(Global.path,CNC3D.getGcodeFileName());
		grblStreamer2=new GRBLstreamer(new File(app.path+CNC3D.getGcodeFileName()));

		//params
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"updateView"));//update view of the shape as a float image
			String[] names={"NbMovesShown"};
			boxNbMovesShown=new ParamsBox(this,tl,names);
			panelSide.add(boxNbMovesShown.getComponent());
		}

		//params
		{
			String[] names1={"SpeedMachining","SpeedMachiningZ","SpeedTransfer","dx","dy","dzPass","toolDiameter","zTransfert"};
			String[] units1={"mm/min","mm/min","mm/min","mm","mm","mm","mm","mm"};
			boxSavedParams=new ParamsBox(cnc3D,null,"Machining",names1,null,units1,ParamsBox.VERTICAL,200,30);
			getPanelSide().add(boxSavedParams.getComponent());
		}


		//params
		{
			String[] names3={"MachiningTime","NbPassesX","NbPassesY","NbPassesZ","Depth"};
			String[] units3={"min","","","","mm"};
			boxTimeMachining=new ParamsBox(cnc3D,null,"",names3,units3);
			boxTimeMachining.setEditable(false);
			getPanelSide().add(boxTimeMachining.getComponent());

		}

		{
			String[] names2={"bauds"};
			ParamsBox box2=new ParamsBox(grblStreamer2,null,names2);
			this.getPanelSide().add(box2.getComponent());
		}

		///button to start cutting with CNC machine
		{
			TaskButton2 button=new TaskButton2(grblStreamer2, null);
			button.setText("Start cutting");
			button.getJButton().setPreferredSize(new Dimension(120,30));
			this.getButtonsPanel().add(button.getJButton());
		}

		//button to see Gcode
		{
			TriggerList tl_butto=new TriggerList();
			tl_butto.add(new Trigger(this,"seeGcode"));
			CButton cButton=new CButton(tl_butto);
			cButton.setPreferredSize(new Dimension(120,20));
			cButton.setText("See gcode...");
			this.getButtonsPanel().add(cButton);
		}

		//button to save Gcode
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"saveGcode"));
			CButton cButton3=new CButton(tl_button3);
			cButton3.setPreferredSize(new Dimension(120,20));
			cButton3.setText("Save gcode...");
			this.getButtonsPanel().add(cButton3);
		}
		//button to save NGC (for Alain Brunel)
		{
			TriggerList tl_button3=new TriggerList();
			tl_button3.add(new Trigger(this,"saveNGC"));
			CButton cButton3=new CButton(tl_button3);
			cButton3.setPreferredSize(new Dimension(100,20));
			cButton3.setText("Save ngc...");
			this.getButtonsPanel().add(cButton3);
		}



	}


	public void updateGcodeStreamerFile()
	{
		grblStreamer2.setFile(new File(app.path+CNC3D.getGcodeFileName()));
	}

	//
	//public void update()
	//{
	//graph3DPanelToolPath.removeAll3DObjects();
	//int last=Math.min(nbMovesShown,cnc3D.getToolPath().nbMovements())-1;
	//for (int i=0;i<last;i++) graph3DPanelToolPath.add(cnc3D.getToolPath().getToolMove(i));
	////graph3DPanelToolPath.add(toolPath.getObject3DSet());
	//CutterDrawing3D cutter=new CutterDrawing3D();
	//cutter.setPosition(cnc3D.getToolPath().getToolMove(last).p2());
	//graph3DPanelToolPath.add(cutter);
	//graph3DPanelToolPath.repaint();
	//}

	
	public void setnbMovesShownToMax()
	{
		nbMovesShown=cnc3D.getToolPath().nbMovements();	
		boxNbMovesShown.update();
	}
	
	public void updateView()
	{
		boxSavedParams.update();
		graph3DPanelToolPath.removeAll3DObjects();
		int last=Math.min(nbMovesShown,cnc3D.getToolPath().nbMovements())-1;
		last=Math.max(0,last);
		for (int i=0;i<last;i++) graph3DPanelToolPath.add(cnc3D.getToolPath().getToolMove(i));
		//graph3DPanelToolPath.add(toolPath.getObject3DSet());
		CutterDrawing3D cutter=new CutterDrawing3D(cnc3D.getToolDiameter());
		cutter.setPosition(cnc3D.getToolPath().getToolMove(last).p2());
		graph3DPanelToolPath.addColorSet(cutter);
		graph3DPanelToolPath.repaint();

		graph3DPanelToolPath.initialiseProjectorAuto();
	}


	public int getNbMovesShown()
	{
		return nbMovesShown;
	}
	public void setNbMovesShown(int nbMovesShown)
	{
		this.nbMovesShown = nbMovesShown;
	}


	public JPanel getPanelSide(){return panelSide;}




	public JPanel getButtonsPanel()
	{
		return buttonsPanel;
	}




	public ParamsBox getBoxTimeMachining() 
	{
		return boxTimeMachining;
	}






	public void seeGcode()
	{
		TextEditor te=new TextEditor(cnc3D.getGcode());
	}




	public void saveGcode()
	{
		JFileChooser df=new JFileChooser(app.path);
		GCODEFileFilter ff=new GCODEFileFilter();
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);
		int returnVal = df.showSaveDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			app.path=df.getSelectedFile().getParent()+File.separator;
			String fileName=df.getSelectedFile().getName();
			if (!fileName.endsWith(ff.getExtension())) fileName+="."+ff.getExtension();
			TextFiles.saveString(app.path+fileName, cnc3D.getGcode());
		}	
		//System.out.println(set.saveToSTL());
		//String stlFileName="/home/laurent/temp/shadower"+".stl";
		//TextFiles.saveString(stlFileName, set.saveToSTL());
		//System.out.println(stlFileName);
	}



	/**
	 * for Alain Brunel
	 */
	public void saveNGC()
	{
		JFileChooser df=new JFileChooser(app.path);
		CFileFilter ff=new CFileFilter("ngc","gcode file (*.ngc)");
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);
		int returnVal = df.showSaveDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			app.path=df.getSelectedFile().getParent()+File.separator;
			String imageFileName=df.getSelectedFile().getName();
			if (!imageFileName.endsWith(ff.getExtension())) imageFileName+="."+ff.getExtension();
			TextFiles.saveString(app.path+imageFileName, cnc3D.getNGC());
		}	
	}




}
