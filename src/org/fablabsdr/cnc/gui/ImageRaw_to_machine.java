package org.fablabsdr.cnc.gui;

import java.io.File;

import javax.swing.JFileChooser;

import org.fablabsdr.cnc3D.CNC3D;

import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.gui.InputDialog;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.image.RawFloatImageFileFilter;
import com.photonlyx.toolbox.math.signal.Signal2D1D;

import nanoxml.XMLElement;

public class ImageRaw_to_machine  implements XMLstorable
{
	private ChartPanel cp;
	private CNC3D cnc3d;
	private CNCapp app;
	
	private String path="";
	private String filename="";
	private double width=100;//mm
	private double depth=3;//mm
	
	//private Signal2D1D shape=new Signal2D1D();

	public ImageRaw_to_machine(CNCapp app,ChartPanel cp,CNC3D cnc3d)
	{
		this.app=app;
		this.cp=cp;
		this.cnc3d=cnc3d;	
	}

	public void importRawImageDialog()
	{
		JFileChooser df=new JFileChooser(app.getPath());
		RawFloatImageFileFilter ff=new RawFloatImageFileFilter();
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);
		int returnVal = df.showOpenDialog(null);
		String filename="";
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			this.path=df.getSelectedFile().getParent()+File.separator;
			app.path=this.path;
			filename=df.getSelectedFile().getName();
			this.setPath(this.path);
			this.setFilename(filename);

			//ask for the scale for XY 
			InputDialog inputDialog=new InputDialog();
			inputDialog.setFieldLabel("Image width (mm) ? ");
			inputDialog.setInitialFieldValue("50");
			inputDialog.show();
			//double scaleXY=new Double(inputDialog.getStringAnswer()); //mm/pix
			double w=Double.parseDouble(inputDialog.getStringAnswer());
			this.setWidth(w);

			//ask for the scale for  Z
			InputDialog inputDialog2=new InputDialog();
			inputDialog2.setFieldLabel("Depth in z (mm)? ");
			inputDialog2.setInitialFieldValue("3");
			inputDialog2.show();
			//double scaleZ=new Double(inputDialog2.getStringAnswer());
			double dz=Double.parseDouble(inputDialog2.getStringAnswer());
			this.setDepth(dz);
			readShapeFile();
		}	
	}

/**
 * set the 3D shap of cnc3D:
 */
	public void readShapeFile()
	{
		if (getFilename().length()==0) return;
		Signal2D1D shape=new Signal2D1D();
		shape.readRawFloatImage(this.path+filename);
		double scaleXY=width/shape.dimx();
		shape.setxmin(0);
		shape.setxmax(shape.dimx()*scaleXY);
		shape.setymin(0);
		shape.setymax(shape.dimy()*scaleXY);
		shape._multiply(depth/(shape.zmax()-shape.zmin()));

		//shape._resample(sampx);
		cp.getChart().setSignal2D1D(shape);
		cnc3d.getShape().copy(shape);

	}
	
	

	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	public String getFilename() {
		return filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}


	public double getWidth() {
		return width;
	}


	public void setWidth(double width) {
		this.width = width;
	}


	public double getDepth() {
		return depth;
	}


	public void setDepth(double depth) {
		this.depth = depth;
	}


	public XMLElement toXML() 
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		el.setAttribute("path",path);
		el.setAttribute("filename",filename);
		el.setAttribute("width",width);
		el.setAttribute("depth",depth);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		path=xml.getStringAttribute("path","");
		filename=xml.getStringAttribute("filename","");
		width=xml.getDoubleAttribute("width",100);
		depth=xml.getDoubleAttribute("depth",3);
	}




}
