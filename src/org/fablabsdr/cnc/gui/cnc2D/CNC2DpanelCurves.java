package org.fablabsdr.cnc.gui.cnc2D;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Vector;

import org.fablabsdr.cnc.CNCUtil;
import org.fablabsdr.cnc.ToolMove;
import org.fablabsdr.cnc.ToolPath;
import org.fablabsdr.cnc3D.CNC3D;

import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;



public class CNC2DpanelCurves extends ChartPanel
{
	
private PlotSet  entry=new PlotSet();//user defined polylines	
private PlotSet  curves=new PlotSet();//to show the curves like splines and circles	
private Signal1D1DXY  trajectoireCNC=new Signal1D1DXY();	
private String gcode;
public double  speedMachining=300;//speed of the tool when machining
public double  speedTransfer=500;//speed of the tool when transfering
public double  zTransfert=500;//z pos of the transfert from one machining to another
public double  dz=1;//z increment at each pass for machining
public boolean  absolute=false;//if true put absolute coordinates in the Gcode
public double  plateThickness=10;//thickness of the plate to machine
public double  plateSize=300;//size of the (square) plate to machine
public double  toolDiameter=5;//diameter of the cutting tool (milling cutter)
public double  speedMin=10;//min speed of the machine mm/min
public double  speedMax=500;//max speed mm/min
public double  minSegmentLength=0.5;//min segment length use to transform curves to polylines
public int maxIndexDrawing3D=100000;

//internal
private ToolPath toolPath3D;
private Object3DColorSet drawing3D=new Object3DColorSet();
private static double lastSpeed;
private CNC3D cnc3D;	
private String path;
	
	
public CNC2DpanelCurves(CNC3D cnc3D,String path)
{
this.cnc3D=cnc3D;
this.path=path;
	
this.getChart().add(entry);	
this.getChart().add(curves);	
this.getChart().add(new Plot(trajectoireCNC,"tool trajectory","green"));	
this.getChart().setKeepXYratio(true);

//button to add polyline
{
TriggerList tl_button3=new TriggerList();
tl_button3.add(new Trigger(this,"addPolyLine"));
tl_button3.add(new Trigger(this,"calcCurves"));
tl_button3.add(new Trigger(this,"update"));
CButton cButton=new CButton(tl_button3);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setIconFileName("org/fablabsdr/gui/cnc2D/AddPolyLine.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("add polyline");
this.getPanelSide().add(cButton);
}


//button to add circle
{
TriggerList tl_button3=new TriggerList();
tl_button3.add(new Trigger(this,"addCircle"));
tl_button3.add(new Trigger(this,"calcCurves"));
tl_button3.add(new Trigger(this,"update"));
CButton cButton=new CButton(tl_button3);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setIconFileName("org/fablabsdr/gui/cnc2D/AddCircle.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("add circle");
this.getPanelSide().add(cButton);
}

//button to add circle using 3 pts
{
TriggerList tl_button3=new TriggerList();
tl_button3.add(new Trigger(this,"addCircle3Pts"));
tl_button3.add(new Trigger(this,"calcCurves"));
tl_button3.add(new Trigger(this,"update"));
CButton cButton=new CButton(tl_button3);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setIconFileName("org/fablabsdr/gui/cnc2D/AddCircle3Pts.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("add circle 3 pts");
this.getPanelSide().add(cButton);
}

//button to add arc circle using 3 pts
{
TriggerList tl_button3=new TriggerList();
tl_button3.add(new Trigger(this,"addArcCircle"));
tl_button3.add(new Trigger(this,"calcCurves"));
tl_button3.add(new Trigger(this,"update"));
CButton cButton=new CButton(tl_button3);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setIconFileName("org/fablabsdr/gui/cnc2D/AddArcCircle.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("add arc");
this.getPanelSide().add(cButton);
}


//button to add spline
{
TriggerList tl_button3=new TriggerList();
tl_button3.add(new Trigger(this,"addSpline"));
tl_button3.add(new Trigger(this,"calcCurves"));
tl_button3.add(new Trigger(this,"update"));
CButton cButton=new CButton(tl_button3);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setIconFileName("org/fablabsdr/gui/cnc2D/AddSpline.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("add spline");
this.getPanelSide().add(cButton);
}


//button to erase selected curve
{
TriggerList tl_button3=new TriggerList();
tl_button3.add(new Trigger(this,"eraseCurve"));
tl_button3.add(new Trigger(this,"calcCurves"));
tl_button3.add(new Trigger(this,"update"));
CButton cButton=new CButton(tl_button3);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setIconFileName("org/fablabsdr/gui/cnc2D/EraseCurve.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("Erase");
this.getPanelSide().add(cButton);
}



//button to set selected curve as left
{
TriggerList tl_button3=new TriggerList();
tl_button3.add(new Trigger(this,"setContourAsLeft"));
tl_button3.add(new Trigger(this,"calcCurves"));
tl_button3.add(new Trigger(this,"update"));
CButton cButton=new CButton(tl_button3);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setIconFileName("org/fablabsdr/gui/cnc2D/SetAsContourLeft.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("left");
this.getPanelSide().add(cButton);
}



//button to set selected curve as right
{
TriggerList tl_button3=new TriggerList();
tl_button3.add(new Trigger(this,"setContourAsRight"));
tl_button3.add(new Trigger(this,"calcCurves"));
tl_button3.add(new Trigger(this,"update"));
CButton cButton=new CButton(tl_button3);
cButton.setPreferredSize(new Dimension(120,20));
cButton.setIconFileName("org/fablabsdr/gui/cnc2D/SetAsContourRight.png");
cButton.setContentAreaFilled(false);
cButton.setBorderPainted(false);
cButton.setText("right");
this.getPanelSide().add(cButton);
}




//button to recalc all
{
TriggerList tl_button3=new TriggerList();
tl_button3.add(new Trigger(this,"calcCurves"));
tl_button3.add(new Trigger(this,"update"));
CButton cButton=new CButton(tl_button3);
cButton.setPreferredSize(new Dimension(120,20));
//cButton.setIconFileName("org/fablabsdr/gui/cnc2D/AddPolyLine.png");
//cButton.setContentAreaFilled(false);
//cButton.setBorderPainted(false);
cButton.setText("Update");
this.getPanelSide().add(cButton);
}


}




public void calcCurves()
{
CNCUtil.SPEEDMIN=speedMin;
CNCUtil.SPEEDMAX=speedMax;
if (entry.nbPlots()==0) 
		{
		trajectoireCNC.init(0);
		curves.removeAllPlots();
		drawing3D.removeAll();
		return;
		}

//eventually process the curves to polylines:
PlotSet segments=cnc3D.curves2polyLines(entry,curves);

if (segments.nbPlots()==0) return;

//transform into a contour cut
PlotSet contours=cnc3D.buildContourTrajectories(segments);

//this.getChart().add(contours);

//transform the polylines into a unique cutter trajectory, making the passes and adding transfers
//toolPath3D=cnc3D.mergeTrajectories(contours);

//drawing3D=cnc3D.calcObject3DList(toolPath3D) ;
//Graph3DWindow g3dw=new Graph3DWindow(drawing3D);


//calculate the G code
//gcode=CNCUtil.calculateGcode(toolPath3D,absolute);
//System.out.println(gcode);
updateToolTrajectory(toolPath3D,trajectoireCNC);

//calculate the W code
//wcode.val=CNCUtil.gcode2wcode(trajectoireCNC, gcode.val, stepX.val,stepY.val,stepZ.val);

String filename=path+"seeNsee2D.gcode";
TextFiles.saveString(filename, gcode);
System.out.println(filename+" saved");
}

public void updateToolTrajectory(ToolPath toolPath3D,Signal1D1DXY  trajectoireCNC)
{
trajectoireCNC.init(0);
for (int i=0;i<toolPath3D.size();i++)
	{
	Segment3D seg=toolPath3D.elementAt(i).getSegment3D();
	trajectoireCNC.addPoint(seg.p1().x(),seg.p1().y());
	}
Segment3D seg=toolPath3D.elementAt(toolPath3D.size()-1).getSegment3D();
trajectoireCNC.addPoint(seg.p2().x(),seg.p2().y());

}




public void addPolyLine()
{
Signal1D1DXY signal=new Signal1D1DXY();
signal.addPoint(0, 0);
signal.addPoint(10, 10);
signal.addPoint(0, 20);
Plot plot=entry.add(signal);
plot.setLabel("z -1 contour right");
plot.setDotSize(5);
plot.setLineThickness(2);
}
	


public void addArcCircle()
{
Signal1D1DXY signal=new Signal1D1DXY();
signal.addPoint(0, 0);
signal.addPoint(10, 10);
signal.addPoint(0, 20);
Plot plot=entry.add(signal);
plot.setLabel("z -1 3pointsArcCircle contour right");
plot.setDotSize(5);
plot.setLineThickness(2);
}



public void addCircle()
{
Signal1D1DXY signal=new Signal1D1DXY();
signal.addPoint(0, 0);
signal.addPoint(10, 0);
Plot plot=entry.add(signal);
plot.setLabel("z -1 fullCircle contour right");
plot.setDotSize(5);
plot.setLineThickness(2);
}
	


public void addCircle3Pts()
{
Signal1D1DXY signal=new Signal1D1DXY();
signal.addPoint(0, 0);
signal.addPoint(10, 10);
signal.addPoint(0, 20);
Plot plot=entry.add(signal);
plot.setLabel("z -1 3pointsCircle contour right");
plot.setDotSize(5);
plot.setLineThickness(2);
}



public void addSpline()
{
Signal1D1DXY signal=new Signal1D1DXY();
signal.addPoint(0, 0);
signal.addPoint(0, 10);
signal.addPoint(20, 10);
signal.addPoint(20, 20);
signal.addPoint(10, 20);
Plot plot=entry.add(signal);
plot.setLabel("z -1 spline contour right");
plot.setDotSize(5);
plot.setLineThickness(2);
}


public void eraseCurve()
{
Plot[] ps=entry.getSelectedPlots();
for (Plot p:ps) entry.removePlot(p);
}



public void setContourAsLeft()
{
Plot[] plots=entry.getSelectedPlots();
if (plots!=null) for (int i=0;i<plots.length;i++) 
	{
	String l=plots[i].getLabel();
	l=l.replace("drilling", "contour left");
	l=l.replace("engraving", "contour left");
	if (l.contains("contour")) l=l.replace("right", "left");
	if (!l.contains("contour")) l+=" contour left";
	//System.out.println(l);
	plots[i].setLabel(l);
	plots[i].setLineThickness(2);
	plots[i].setDotSize(5);
	}

}



public void setContourAsRight()
{
Plot[] plots=entry.getSelectedPlots();
if (plots!=null) for (int i=0;i<plots.length;i++) 
	{
	String l=plots[i].getLabel();
	l=l.replace("drilling", "contour right");
	l=l.replace("engraving", "contour right");
			if (l.contains("contour")) l=l.replace("left", "right");
	if (!l.contains("contour")) l+=" contour right";
	//System.out.println(l);
	plots[i].setLabel(l);
	plots[i].setLineThickness(2);
	plots[i].setDotSize(5);
	}

}



}