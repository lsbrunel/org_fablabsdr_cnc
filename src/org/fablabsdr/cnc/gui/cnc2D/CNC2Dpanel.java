package org.fablabsdr.cnc.gui.cnc2D;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.fablabsdr.cnc3D.CNC3D;

public class CNC2Dpanel extends JPanel
{
	private String path;


	public	CNC2Dpanel(String path)
	{
		this.path=path;


		this.setLayout(new BorderLayout());
		JTabbedPane jtp=new JTabbedPane();
		this.add(jtp);

		CNC3D cnc3D=new CNC3D();
		jtp.add(new CNC2DpanelCurves(cnc3D,path),"Curves");
		jtp.add(new CNC2DpanelGcode(),"Gcode");
		jtp.add(new CNC2DpanelToolPath3D(),"Tool path");



	}


}
