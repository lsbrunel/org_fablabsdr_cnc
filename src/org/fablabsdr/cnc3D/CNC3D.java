package org.fablabsdr.cnc3D;


import java.awt.Color;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import org.fablabsdr.cnc.CNCUtil;
import org.fablabsdr.cnc.ToolMove;
import org.fablabsdr.cnc.ToolPath;

import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.function.usual1D2D.ArcCircleCurve;
import com.photonlyx.toolbox.math.function.usual1D2D.CircleCurve;
import com.photonlyx.toolbox.math.geometry.Line2D;
import com.photonlyx.toolbox.math.geometry.Segment2D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.spline.Bspline;
import com.photonlyx.toolbox.math.spline.BsplineCurve;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.util.Messager;

import nanoxml.XMLElement;

/**
 *  calc the gcode to machine a 3D shape
 * */
public class CNC3D  implements XMLstorable
{
	//private Signal2D1D shape0=new Signal2D1D();//shape from the file
	private Signal2D1D shape=new Signal2D1D();//shape rescaled
	private ToolPath toolPath=new ToolPath();
	//private double xSize=100,ySize=100,zSize=10;
	//private double pass=10;
	private double machiningTime;//minutes
	private String gcode;
	private int nbPassesX,nbPassesY,nbPassesZ;
	//private double scaleXY=1;//rescale of the original shape
	//private double scaleZ=1;//rescale of the original shape in Z
	//public boolean invert=false;
	private double depth;
	private static String gcodeFileName="seeNsee3D.gcode";

	private double  minSegmentLength=0.5;//min segment length use to transform curves to polylines
	public double  plateThickness=10;//thickness of the plate to machine
	public double  plateSize=300;//size of the (square) plate to machine
	public int maxIndexDrawing3D=100000;

	//saved parameters:
	private double speedMachining=800,speedMachiningZ=300,speedTransfer=300;//mm/s
	private double  toolDiameter=5;//diameter of the cutting tool (milling cutter)
	public double dzPass=3;//pass
	private double  zTransfert=5;//z pos of the transfert from one machining to another
	private double dx=0.2;//step in x for image cutting
	private double dy=2;//step in y for image cutting


	private static DecimalFormatSymbols dfs=new DecimalFormatSymbols(Locale.ENGLISH);
	private static DecimalFormat df0=new DecimalFormat("0",dfs);
	private static DecimalFormat df3=new DecimalFormat("0.000",dfs);



	public CNC3D()
	{
		super();
	}


	public void resetShape()
	{
		shape.copy(new Signal2D1D());
	}

	public void add3Dshape2toolPath()
	{
		//shape.copy(shape0);
		//System.out.println("ScaleXY="+scaleXY);
		//System.out.println("ScaleZ="+scaleZ);
		System.out.println("Calculate toolpath...");
		//if (scaleZ!=1) shape._multiply(scaleZ);
		//if (invert) shape._multiply(-1);
		//translate to get the top left bottom corner at (0,0,0)
		shape._translate(-shape.xmin(),-shape.ymin(),-shape.zmax());
		//rescale
		//if (scaleXY!=1) shape._setXYBoudaries(0.,shape.xmax()*scaleXY,0.,shape.ymax()*scaleXY);

		//ChartWindow cw=new ChartWindow(shape);

		depth=shape.zmax()-shape.zmin();

		//interpolate
		//Interpolation2D interpol=new Interpolation2D(shape);

		//double dzPass=pass;

		double zmin=shape.minValue();
		double ymax=shape.ymax();
		double xmax=shape.xmax();
		nbPassesZ=(int) Math.ceil((0-zmin)/dzPass);
		double dyPass=dy;
		double dxPass=dx;
		//System.out.println(" dxPass="+dxPass+" "+"dyPass="+dyPass);
		nbPassesY=(int) Math.ceil((ymax-0)/dyPass);
		nbPassesX=(int) Math.ceil((xmax-0)/dxPass);

		toolPath.clean();
		//double[] point=new double[2];

		Vecteur point=new Vecteur();
		Vecteur previousPoint=new Vecteur();
		point.setX(200);
		point.setY(0);
		//System.out.println("x="+point.x()+" y="+point.y()+" "+shape.getValue(point.x(),point.y())+"  "+shape.getValueInterpol(point.x(),point.y()));

		for (int k=0;k<nbPassesZ;k++)
		{
			double zPass=0-(k+1)*dzPass;//minimum z of the pass
			for (int jj=0;jj<nbPassesY/2;jj++)	
			{
				//aller:
				int j=2*jj;
				//point[1]=0+dyPass*j;
				point.setY(dyPass*j);
				//go fast at 0,yy,0
				toolPath.addMovementAbsolute(0,point.y(),0,speedTransfer);
				//System.out.println(toolPath.lastPoint());//debug

				//go cutting at z=zPass or at the shape surface
				double z0=Math.max(shape.getValueInterpol(point.x(),point.y()),zPass);
				//double z0=Math.max(interpol.f(point.x(),point.y()),zPass);
				toolPath.addMovementIncremental(0,0,z0-0.0,speedMachiningZ);
				double previousZ=toolPath.lastPoint().z();

				//go along X varying Z:
				for (int i=1;i<nbPassesX;i++)
				{
					point.setX(0+dxPass*i);
					//double zz=Math.max(shape.getValue(xx, yy),zPass);
					double zz=Math.max(shape.getValueInterpol(point.x(),point.y()),zPass);
					//System.out.println("x="+point.x()+" y="+point.y()+" "+zz+"  "+shape.getValueInterpol(point.x(),point.y()));
					//double zz=Math.max(interpol.f(point.x(),point.y()),zPass);
					point.setZ(zz);
					if (zz!=previousZ) 
					{
						//end last long run:
						toolPath.addMovementAbsolute(point.x()-dxPass,point.y(),previousZ,speedMachining);
						//add this last step
						toolPath.addMovementAbsolute(point.x(),point.y(),point.z(),speedMachining);
						previousZ=zz;
					}
					else 
						if (i==(nbPassesX-1))
						{
							//add this last step
							toolPath.addMovementAbsolute(point.x(),point.y(),point.z(),speedMachining);
							previousZ=zz;
						}
					previousPoint.affect(point);
				}
				//go up at z=0
				double lastZ=toolPath.lastPoint().z();
				toolPath.addMovementIncremental(0,0,-lastZ,speedTransfer);

				//retour:
				j=2*jj+1;
				point.setY(0+dyPass*j);
				//go fast at xmax,yy,0
				//toolPath.addMovementAbsolute(xmax,point.y(),0,speedTransfer);
				toolPath.addMovementAbsolute(point.x(),point.y(),0,speedTransfer);

				//go cutting at z=zPass or at the shape surface
				z0=Math.max(shape.getValueInterpol	(point.x(),point.y()),zPass);
				//z0=Math.max(interpol.f(point.x(),point.y()),zPass);
				toolPath.addMovementIncremental(0,0,z0-0.0,speedMachiningZ);
				previousZ=toolPath.lastPoint().z();
				for (int i=nbPassesX-1;i>=0;i--)
				{
					point.setX(0+dxPass*i);
					//double zz=Math.max(shape.getValue(xx, yy),zPass);
					double zz=Math.max(shape.getValueInterpol(point.x(),point.y()),zPass);
					//double zz=Math.max(interpol.f(point.x(),point.y()),zPass);
					if (zz!=previousZ) 
					{
						//end last long run:
						toolPath.addMovementAbsolute(point.x()+dxPass,point.y(),previousZ,speedMachining);
						//add this last step
						toolPath.addMovementAbsolute(point.x(),point.y(),zz,speedMachining);
						previousZ=zz;
					}
					else
						if (i==0)
						{
							//add this last step
							toolPath.addMovementAbsolute(point.x(),point.y(),zz,speedMachining);
							previousZ=zz;
						}
					previousPoint.affect(point);
				}
				//go up at z=0
				lastZ=toolPath.lastPoint().z();
				toolPath.addMovementIncremental(0,0,-lastZ,speedTransfer);


			}
		}
		//go fast at 0,0,0
		toolPath.addMovementAbsolute(0,0,0,speedTransfer);


		//calculate the G code
		//gcode=toolPath.convertInGCode();

		//System.out.println(toolPath);//debug

		//System.out.println("... done");

		//String filename=Global.path+gcodeFileName;
		//TextFiles.saveString(filename, gcode);
		//System.out.println(filename+" saved");

		//machiningTime=toolPath.getDuration_s()/60;
	}


	public  ToolPath getToolPath() 
	{
		return toolPath;
	}



	public void calcMachiningTime()
	{
		machiningTime=toolPath.getDuration_s()/60;
		System.out.println("Machining time: "+machiningTime);
	}




	/**
	 * transform the curves (polyLines, circles, splines) into polyLines only
	 * @param entry
	 * @return
	 */
	public  PlotSet curves2polyLines(PlotSet entry,PlotSet curves)
	{
		PlotSet f=new PlotSet();
		curves.removeAllPlots();
		for (int i=0;i<entry.nbPlots();i++)
		{
			Plot plot=entry.getPlot(i);
			if (plot.getLabel().contains("off")) continue;
			if (plot.getLabel().contains("spline"))//spline curve defined by control points
			{
				Signal1D1D ctrlPts=plot.getSignal();
				if (ctrlPts.getDim()<=(3+1)) 
				{
					Messager.messErr("Not enough points for the spline:"+plot.getLabel());
					continue;
				}
				BsplineCurve spline=new BsplineCurve(ctrlPts,3);
				spline.update();
				Signal1D1D polyLine=CNCUtil.getSegments(spline,minSegmentLength);
				curves.add(polyLine);
				curves.getPlot(curves.nbPlots()-1).setColor("lightGray");
				curves.getPlot(curves.nbPlots()-1).setLabel(plot.getLabel()+" (curve)");
				f.add(polyLine);
				f.getPlot(f.nbPlots()-1).setLabel(plot.getLabel());
			}
			else if (plot.getLabel().contains("fullCircle"))//circle arc defined by 3 points
			{
				//	double x=new Definition(plot.label()).searchValue("x", 0, false);
				//	double y=new Definition(plot.label()).searchValue("y", 0, false);
				//	double r=new Definition(plot.label()).searchValue("r", 0, false);
				double xc=plot.getSignal().x(0);
				double yc=plot.getSignal().y(0);
				double x=plot.getSignal().x(1);
				double y=plot.getSignal().y(1);
				double r=Math.sqrt(Math.pow(x-xc,2)+Math.pow(y-yc,2));
				CircleCurve circle=new CircleCurve(xc,yc,r);
				Signal1D1D polyLine=CNCUtil.getSegments(circle,minSegmentLength);
				curves.add(polyLine);
				curves.getPlot(curves.nbPlots()-1).setColor("lightGray");
				curves.getPlot(curves.nbPlots()-1).setLabel(plot.getLabel()+" (curve)");
				f.add(polyLine);
				f.getPlot(f.nbPlots()-1).setLabel(plot.getLabel());
			}
			else if (plot.getLabel().contains("3pointsCircle"))//circle arc defined by 3 points
			{
				Vecteur2D p1=new Vecteur2D(plot.getSignal().x(0),plot.getSignal().y(0));
				Vecteur2D p2=new Vecteur2D(plot.getSignal().x(1),plot.getSignal().y(1));
				Vecteur2D p3=new Vecteur2D(plot.getSignal().x(2),plot.getSignal().y(2));
				CircleCurve circle=new CircleCurve(p1,p2,p3);
				Signal1D1D polyLine=CNCUtil.getSegments(circle,minSegmentLength);
				curves.add(polyLine);
				curves.getPlot(curves.nbPlots()-1).setLabel(plot.getLabel()+" (curve)");
				curves.getPlot(curves.nbPlots()-1).setColor("lightGray");
				f.add(polyLine);
				f.getPlot(f.nbPlots()-1).setLabel(plot.getLabel());
			}
			else if (plot.getLabel().contains("3pointsArcCircle"))//circle arc defined by 3 points
			{
				Vecteur2D p1=new Vecteur2D(plot.getSignal().x(0),plot.getSignal().y(0));
				Vecteur2D p2=new Vecteur2D(plot.getSignal().x(1),plot.getSignal().y(1));
				Vecteur2D p3=new Vecteur2D(plot.getSignal().x(2),plot.getSignal().y(2));
				ArcCircleCurve circle=new ArcCircleCurve(p1,p2,p3);
				Signal1D1D polyLine=CNCUtil.getSegments(circle,minSegmentLength);
				curves.add(polyLine);
				curves.getPlot(curves.nbPlots()-1).setLabel(plot.getLabel()+" (curve)");
				curves.getPlot(curves.nbPlots()-1).setColor("lightGray");
				f.add(polyLine);
				f.getPlot(f.nbPlots()-1).setLabel(plot.getLabel());
			}
			else f.add(entry.getPlot(i));//take the same polyline as is
		}
		return f;
	}







	/**
	 * //transform polyLines into a contour polyLine
	 * @param curves
	 * @param toolRadius
	 * @return
	 */
	public PlotSet buildContourTrajectories(PlotSet curves)
	{
		boolean contourAtRight;//if true the contour is done at the right of the segments
		PlotSet f=new PlotSet();
		for (int k=0;k<curves.nbPlots();k++)
		{
			Plot plot=curves.getPlot(k);
			if (plot.getLabel().contains("drilling")) 
			{
				f.add(plot);
				continue;
			}
			if (plot.getLabel().contains("engraving")) 
			{
				f.add(plot);
				continue;
			}
			if (plot.getLabel().contains("right")) contourAtRight=true;else contourAtRight=false;
			Signal1D1D signal=plot.getSignal();
			Signal1D1DXY parallel=new Signal1D1DXY();
			//construct the segments parallel
			for (int i=0;i<signal.getDim()-1;i++)
			{
				//take a segment
				//first point 
				Vecteur2D p1=new Vecteur2D(signal.x(i),signal.y(i));
				//second point
				Vecteur2D p2=new Vecteur2D(signal.x(i+1),signal.y(i+1));
				//the segment
				Vecteur2D segment=new Vecteur2D(signal.x(i+1)-signal.x(i),signal.y(i+1)-signal.y(i));
				//calc the perpendicular vector to the segment 
				Vecteur2D perp=segment.getPerpVector(contourAtRight).normalisen().scmul(toolDiameter/2);
				//first point translated perpendicularly with tool radius
				Vecteur2D p1bis=p1.addn(perp);
				Vecteur2D p2bis=p2.addn(perp);
				parallel.addPoint(p1bis.x(),p1bis.y());
				parallel.addPoint(p2bis.x(),p2bis.y());
				//		if (i==(signal.getDim()-2) )
				//			{
				//			p1=new Vecteur2D(signal.x(i+1),signal.y(i+1));
				//			p2=p1.addn(perp);
				//			parallel.addPoint(p2.x(),p2.y());
				//			}
			}

			//check each node: interior or exterior ?
			//if the consecutive segments intersect,it is interior, cut them , if not complete with circle arc
			boolean[] interior=new boolean[signal.getDim()];
			Vecteur2D[] intersectOrCenter=new Vecteur2D[signal.getDim()];
			for (int i=0;i<parallel.getDim()-3;i+=2)
			{
				int indexOfNodeOnSignal=i/2+1;
				//a segment
				Vecteur2D p1=new Vecteur2D(parallel.x(i),parallel.y(i));
				Vecteur2D p2=new Vecteur2D(parallel.x(i+1),parallel.y(i+1));
				Vecteur2D p1bis=new Vecteur2D(parallel.x(i+2),parallel.y(i+2));
				Vecteur2D p2bis=new Vecteur2D(parallel.x(i+3),parallel.y(i+3));
				Segment2D seg1=new Segment2D(p1,p2);
				Segment2D seg2=new Segment2D(p1bis,p2bis);
				//check segment intersection
				Line2D l1=new Line2D(seg1);
				Line2D l2=new Line2D(seg2);
				Vecteur2D inter=l1.getIntersectionPointWith(l2);
				if (seg1.isOnSegment(inter))//segments intersect
				{
					interior[indexOfNodeOnSignal]=true;
					intersectOrCenter[indexOfNodeOnSignal]=inter;
				}
				else
				{
					interior[indexOfNodeOnSignal]=false;
					//the centre is the point of the original polyline:
					Vecteur2D node=new Vecteur2D(signal.x(indexOfNodeOnSignal),signal.y(indexOfNodeOnSignal));
					intersectOrCenter[indexOfNodeOnSignal]=node;
				}
			}
			//check if the first and last segments intersects
			if (signal.getDim()>=3)
			{
				int i1=0;
				int i2=parallel.getDim()-3;
				int indexOfNodeOnSignal1=0;
				//a segment
				Vecteur2D p1=new Vecteur2D(parallel.x(i1),parallel.y(i1));
				Vecteur2D p2=new Vecteur2D(parallel.x(i1+1),parallel.y(i1+1));
				Vecteur2D p1bis=new Vecteur2D(parallel.x(i2+1),parallel.y(i2+1));
				Vecteur2D p2bis=new Vecteur2D(parallel.x(i2+2),parallel.y(i2+2));
				Segment2D seg1=new Segment2D(p1,p2);
				Segment2D seg2=new Segment2D(p1bis,p2bis);
				//check segment intersection
				Line2D l1=new Line2D(seg1);
				Line2D l2=new Line2D(seg2);
				System.out.println("l1= "+l1);
				System.out.println("l2= "+l2);
				Vecteur2D inter=l1.getIntersectionPointWith(l2);
				System.out.println("inter1= "+inter);
				if (seg1.isOnSegment(inter))//segments intersect
				{
					interior[indexOfNodeOnSignal1]=true;
					intersectOrCenter[indexOfNodeOnSignal1]=inter;
				}
				else
				{
					interior[indexOfNodeOnSignal1]=false;
					//the centre is the point of the original polyline:
					Vecteur2D node=new Vecteur2D(signal.x(indexOfNodeOnSignal1),signal.y(indexOfNodeOnSignal1));
					intersectOrCenter[indexOfNodeOnSignal1]=node;
				}
			}	

			//build another polyline with corrections of trajectory
			Signal1D1DXY parallelCorrected=new Signal1D1DXY();

			//put the first point 
			if (interior[0]) parallelCorrected.addPoint(intersectOrCenter[0]);
			else parallelCorrected.addPoint(parallel.x(0),parallel.y(0));

			for (int i=0;i<parallel.getDim()-3;i+=2)
			{
				int indexOfNodeOnSignal=i/2+1;
				//a segment
				Vecteur2D p2=new Vecteur2D(parallel.x(i+1),parallel.y(i+1));
				Vecteur2D p1bis=new Vecteur2D(parallel.x(i+2),parallel.y(i+2));
				//parallelCuttedInterior.addPoint(p1);
				if (interior[indexOfNodeOnSignal])//interior
				{
					Vecteur2D inter=intersectOrCenter[indexOfNodeOnSignal];
					System.out.println("inter= "+inter);
					parallelCorrected.addPoint(inter);
				}
				else //join with a circle arc
				{
					Vecteur2D centre=intersectOrCenter[indexOfNodeOnSignal];
					//the radius is the same as the tool radius:
					double radius=toolDiameter/2.0;
					System.out.println("centre="+centre);
					//angles of start and end of the arc circle
					double alpha1=p2.subn(centre).polarAngle();
					double alpha2=p1bis.subn(centre).polarAngle();
					//System.out.println("alpha1="+alpha1+" alpha2="+alpha2);
					if (Math.abs(alpha2-alpha1)>Math.PI)//circle would go on the wrong side
					{
						if (alpha1<alpha2) alpha1+=2*Math.PI;
						else alpha2+=2*Math.PI;
					}
					ArcCircleCurve circle=new ArcCircleCurve(centre,radius,alpha1,alpha2);
					Signal1D1DXY arcCircle=CNCUtil.getSegments(circle,minSegmentLength);
					//parallelAdapted.addPoint(p2);
					//parallelAdapted.addPoint(p1bis);
					for (int j=0;j<arcCircle.getDim();j++)
					{
						//System.out.println(getClass()+" arc: "+arcCircle.x(j)+" "+arcCircle.y(j));
						parallelCorrected.addPoint(arcCircle.x(j),arcCircle.y(j));

					}
				}
				//parallelCuttedInterior.addPoint(p2bis);
			}
			//put the last point unmodified
			if (interior[0]) parallelCorrected.addPoint(intersectOrCenter[0]);//same place as first point
			else parallelCorrected.addPoint(parallel.x(parallel.getDim()-1),parallel.y(parallel.getDim()-1));//unmodified

			Plot plotAdapted=f.add(parallelCorrected);
			System.out.println(getClass()+" \n\n"+parallelCorrected);
			plotAdapted.setLabel(plot.getLabel());
		}
		return f;
	}










	/**
	 * merge the polyLines into a unique tool trajectory, making the passes and adding the transfers. 
	 * Result in toolPath3D.
	 * 
	 * @param segments
	 * @param toolPath3D
	 * @param speedTransfer
	 * @param speedMachining
	 * @param zTransfert
	 * @param dzPass
	 */
	public  void mergeTrajectories(PlotSet  segments)
	{
		//if (segments.nbPlots()==0) return null;
		//ToolPath toolPath3D=new ToolPath();

		toolPath.removeAllToolMove();

		Vecteur p1=new Vecteur(0,0,0);
		Vecteur p2=new Vecteur(0,0,0);
		toolPath.add(new ToolMove(new Segment3DColor(p1,p2,Color.red.getRGB()),0) );

		//first transfer from 0,0 to the the first point, the tool is placed manually to 0,0 before starting
		{
			//put the  z for transfer:
			double _dz= zTransfert-toolPath.lastPoint().z();
			CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);

			//make the transfer
			double _dx=segments.getPlot(0).getSignal().x(0)-0;
			double _dy=segments.getPlot(0).getSignal().y(0)-0;
			CNCUtil.addMovementIncremental(_dx,0,0,speedTransfer,toolPath);
			CNCUtil.addMovementIncremental(0,_dy,0,speedTransfer,toolPath);

			//back the z
			_dz=-_dz;
			CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);
		}

		for (int curve=0;curve<segments.nbPlots();curve++)
		{
			Plot plot=segments.getPlot(curve);
			if (curve>=1)  //make the transfer from one machining to another
			{
				//put the  z for transfer:
				double _dz= zTransfert-toolPath.lastPoint().z();
				CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);

				//make the transfer
				Signal1D1D signalPrec=segments.getPlot(curve-1).getSignal();
				Signal1D1D signal=segments.getPlot(curve).getSignal();
				double _dx=signal.x(0)-signalPrec.x(signalPrec.getDim()-1);
				double _dy=signal.y(0)-signalPrec.y(signalPrec.getDim()-1);
				CNCUtil.addMovementIncremental(_dx,0,0,speedTransfer,toolPath);
				CNCUtil.addMovementIncremental(0,_dy,0,speedTransfer,toolPath);
				//back the z
				_dz=-_dz;
				CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);
			}

			Signal1D1D signal=plot.getSignal();

			//make the machining

			//the max  z for machining  is in the curve label:
			//if no depth is annoted in the label, put on pass:
			if (!new Definition(plot.getLabel()).hasWord("z")) plot.setLabel("z -"+dzPass+" "+plot.getLabel());

			if (plot.getLabel().contains("drilling"))
			{//drilling
				double zBottom=new Definition(plot.getLabel()).searchValue("z", 0, false);//depth of drilling

				//drill 1st pointzTransfert
				CNCUtil.addMovementIncremental(0,0,zBottom,speedMachining,toolPath);
				CNCUtil.addMovementIncremental(0,0,zTransfert-zBottom,speedTransfer,toolPath);
				//drill the other points
				for(int i=0;i<signal.getDim()-1;i++)//segments loop
				{
					//drill:
					double _dx=signal.x(i+1)-signal.x(i);
					double _dy=signal.y(i+1)-signal.y(i);		
					CNCUtil.addMovementIncremental(_dx,_dy,0,speedTransfer,toolPath);
					CNCUtil.addMovementIncremental(0,0,0-zTransfert,speedTransfer,toolPath);
					CNCUtil.addMovementIncremental(0,0,zBottom-0,speedMachining,toolPath);
					CNCUtil.addMovementIncremental(0,0,zTransfert-zBottom,speedTransfer,toolPath);
				}//end of segments loop		
				//go back to z=0
				double _dz=0-toolPath.lastPoint().z();
				CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);
			}//drilling end
			//	else
			//		if (plot.label().contains("engraving"))
			//		{//engraving
			//		double zBottom=new Definition(plot.label()).searchValue("z", 0, false);//depth of drilling
			//			
			//		//drill 1st pointzTransfert
			//		CNCUtil.addMovementIncremental(0,0,zBottom,speedMachining,toolPath3D);
			//		CNCUtil.addMovementIncremental(0,0,zTransfert-zBottom,speedTransfer,toolPath3D);
			//		//drill the other points
			//		for(int i=0;i<signal.getDim()-1;i++)//segments loop
			//			{
			//			//drill:
			//			double _dx=signal.x(i+1)-signal.x(i);
			//			double _dy=signal.y(i+1)-signal.y(i);		
			//			CNCUtil.addMovementIncremental(_dx,_dy,0,speedTransfer,toolPath3D);
			//			CNCUtil.addMovementIncremental(0,0,0-zTransfert,speedTransfer,toolPath3D);
			//			CNCUtil.addMovementIncremental(0,0,zBottom-0,speedMachining,toolPath3D);
			//			CNCUtil.addMovementIncremental(0,0,zTransfert-zBottom,speedTransfer,toolPath3D);
			//			}//end of segments loop		
			//		//go back to z=0
			//		double _dz=0-CNCUtil.lastPoint(toolPath3D).z();
			//		CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
			//		}//engraving end
			else
			{//contour machining
				double toCut=0-new Definition(plot.getLabel()).searchValue("z", 0, false);
				int nbPasses=(int) Math.floor(toCut/dzPass)+1;
				//the first passes are complete, the last is smaller to reach the good z
				double lastPass=toCut-(nbPasses-1)*dzPass;
				//		System.out.println("toCut="+toCut);
				//		System.out.println("nbPasses="+nbPasses);
				//		System.out.println("lastPass="+lastPass);
				for (int k=0;k<nbPasses;k++)	
				{
					double _dz=-dzPass;
					if (k==(nbPasses-1)) _dz=-lastPass;
					CNCUtil.addMovementIncremental(0,0,_dz,speedMachining,toolPath);

					//make the machining in XY plane:
					Vecteur P1=toolPath.lastPoint();
					for(int i=0;i<signal.getDim()-1;i++)//segments loop
					{
						double _dx=signal.x(i+1)-signal.x(i);
						double _dy=signal.y(i+1)-signal.y(i);		
						CNCUtil.addMovementIncremental(_dx,_dy,0,speedMachining,toolPath);
					}//end of segments loop

					Vecteur P2=toolPath.lastPoint();
					//go back to the begging of the curve:
					if (k<nbPasses-1)
					{
						_dz= zTransfert-toolPath.lastPoint().z();
						CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);
						//make the transfer
						double _dx=-(P2.x()-P1.x());
						double _dy=-(P2.y()-P1.y());
						CNCUtil.addMovementIncremental(_dx,0,0,speedTransfer,toolPath);
						CNCUtil.addMovementIncremental(0,_dy,0,speedTransfer,toolPath);
						//back the z to last one 
						_dz= -_dz;
						CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);
					}
				}
				//go back to z=0
				double _dz=0-toolPath.lastPoint().z();
				CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);
			}//end contour machining				


		}//end of curves loop


		//back to  0,0,0
		{
			//put the  z for transfer:
			double _dz= zTransfert-toolPath.lastPoint().z();
			CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);
			//make the transfer
			Signal1D1D signal=segments.getPlot(segments.nbPlots()-1).getSignal();
			double _dx=0-signal.x(signal.getDim()-1);
			double _dy=0-signal.y(signal.getDim()-1);
			CNCUtil.addMovementIncremental(_dx,0,0,speedTransfer,toolPath);
			CNCUtil.addMovementIncremental(0,_dy,0,speedTransfer,toolPath);
			//back to z=0
			_dz=0-toolPath.lastPoint().z();
			CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath);
		}

		//return toolPath3D;
	}







	public Object3DColorSet calcObject3DList() 
	{	
		Object3DColorSet drawing3D=new Object3DColorSet();
		//initialize the drawing 3D
		drawing3D.removeAll();
		//return empty if toolPath3D null
		//if (toolPath3D==null) return drawing3D;
		//draw the upper plane of the material
		double rx=plateSize/2;
		double rz=plateThickness;
		drawing3D.add(new Quadrilatere3DColor(new Vecteur(rx,rx,0),new Vecteur(rx,-rx,0),new Vecteur(-rx,-rx,0),new Vecteur(-rx,rx,0),Color.orange,false));
		drawing3D.add(new Quadrilatere3DColor(new Vecteur(rx,rx,-rz),new Vecteur(rx,-rx,-rz),new Vecteur(-rx,-rx,-rz),new Vecteur(-rx,rx,-rz),Color.orange,false));
		drawing3D.add(new Segment3DColor(new Vecteur(rx,rx,-rz),new Vecteur(rx,rx,0),Color.orange.getRGB()));
		drawing3D.add(new Segment3DColor(new Vecteur(rx,-rx,-rz),new Vecteur(rx,-rx,0),Color.orange.getRGB()));
		drawing3D.add(new Segment3DColor(new Vecteur(-rx,rx,-rz),new Vecteur(-rx,rx,0),Color.orange.getRGB()));
		drawing3D.add(new Segment3DColor(new Vecteur(-rx,-rx,-rz),new Vecteur(-rx,-rx,0),Color.orange.getRGB()));
		//draw the tool path
		//int maxIndexDrawing3D=this.vInt("maxIndexDrawing3D");
		//for (int i=0;i<toolPath3D.size();i++) 
		for (int i=0;i<(int)Math.min(maxIndexDrawing3D,toolPath.size());i++) 	
			drawing3D.add(toolPath.elementAt(i).getSegment3D());
		//draw the tool at the last position:
		double tx=toolDiameter/2;
		double ty=toolDiameter/2;
		double tz=toolDiameter*4;
		Vecteur lastPoint=((Segment3D)drawing3D.elementAt(drawing3D.getNbObjects()-1)).p2();
		double lx=lastPoint.x();
		double ly=lastPoint.y();
		double lz=lastPoint.z();
		int nbPts=8;
		double x=toolDiameter/2;
		double y=0;
		double x1,y1;
		for (int i=1;i<=nbPts;i++)
		{
			double angle=(Math.PI*2/nbPts)*i;
			x1=toolDiameter/2*Math.cos(angle);
			y1=toolDiameter/2*Math.sin(angle);
			drawing3D.add(new Segment3DColor(new Vecteur(lx+x,ly+y,lz),new Vecteur(lx+x1,ly+y1,lz),Color.orange.getRGB()));
			drawing3D.add(new Segment3DColor(new Vecteur(lx,ly,lz),new Vecteur(lx+x,ly+y,lz),Color.orange.getRGB()));
			drawing3D.add(new Segment3DColor(new Vecteur(lx+x,ly+y,lz),new Vecteur(lx+x,ly+y,lz+tz),Color.orange.getRGB()));
			x=x1;
			y=y1;
		}
		return drawing3D;
	}


	public void calculateGcode()
	{
		System.out.println("Calculating gcode");
		gcode=getToolPath().convertInGCode();
	}

	/**
	 * calculate the Gcode from the tool path
	 * @param toolPath3D
	 * @param absolute
	 * @return
	 */
	public static String calculateGcode(ToolPath toolPath3D,boolean absolute)
	{
		Vector<String> gcode=new Vector<String>();
		String s;
		double previousSpeed=-1;
		gcode.add(";generated by com.cionin.CNC on "+new Date());//set current as home (0,0,0)
		//set units as mm:
		gcode.add("; set units as mm:");
		gcode.add("G21");
		if (absolute) 
		{
			gcode.add("; use absolute coordinates:");//absolute positioning
			gcode.add("G90");//absolute positioning
		}
		else 
		{
			gcode.add("; use relative coordinates:");//incremental positioning
			gcode.add("G91");//incremental positioning
		}
		gcode.add(";set current as home (0,0,0):");//set current as home (0,0,0)
		gcode.add("G92 X0 Y0 Z0");//set current as home (0,0,0)

		//gcode.add("G1X0 ; bug?\n");

		for (int i=0;i<toolPath3D.size();i++)
		{
			s=new String("G1 ");
			Segment3D seg=toolPath3D.elementAt(i).getSegment3D();
			double dx=seg.p2().x()-seg.p1().x();
			double dy=seg.p2().y()-seg.p1().y();
			double dz=seg.p2().z()-seg.p1().z();
			if (absolute) 
			{
				if (dx!=0) s+=" X"+df3.format(seg.p2().x());
				if (dy!=0) s+=" Y"+df3.format(seg.p2().y()); 
				if (dz!=0) s+=" Z"+df3.format(seg.p2().z()); 
			}
			else 
			{
				if (dx!=0) s+=" X"+df3.format(dx);
				if (dy!=0) s+=" Y"+df3.format(dy); 
				if (dz!=0) s+=" Z"+df3.format(dz); 
			}
			double speed=toolPath3D.elementAt(i).getSpeed();

			//if (speed!=previousSpeed) 
			{
				s+=" F"+df0.format(speed);
				previousSpeed=speed;
			}

			if ((dx!=0)||(dy!=0)||(dz!=0)) gcode.add(s);
		}
		StringBuffer sb=new StringBuffer();
		for (int i=0;i<gcode.size();i++)
		{
			//sb.append("N"+(i*10)+" "+gcode.elementAt(i)+"\n");
			sb.append(gcode.elementAt(i)+"\n");
		}
		return sb.toString();
	}



	//public double getxSize()
	//{
	//return xSize;
	//}
	//public void setxSize(double xSize)
	//{
	//this.xSize = xSize;
	//}
	//public double getySize()
	//{
	//return ySize;
	//}
	//public void setySize(double ySize)
	//{
	//this.ySize = ySize;
	//}
	//public double getzSize()
	//{
	//return zSize;
	//}
	//public void setzSize(double zSize)
	//{
	//this.zSize = zSize;
	//}
	//



	//
	//public double getPass()
	//{
	//return pass;
	//}
	//
	//
	//
	//
	//public void setPass(double dz)
	//{
	//this.pass = dz;
	//}

	public double getSpeedMachining()
	{
		return speedMachining;
	}
	public void setSpeedMachining(double speedMachining)
	{
		this.speedMachining = speedMachining;
	}


	//
	//public double getSpeedMachiningX()
	//{
	//return speedMachiningX;
	//}
	//public void setSpeedMachiningX(double speedMachining)
	//{
	//this.speedMachiningX = speedMachining;
	//}
	//
	//public double getSpeedMachiningY()
	//{
	//return speedMachiningY;
	//}
	//public void setSpeedMachiningY(double speedMachining)
	//{
	//this.speedMachiningY = speedMachining;
	//}

	public double getSpeedMachiningZ()
	{
		return speedMachiningZ;
	}
	public void setSpeedMachiningZ(double speedMachining)
	{
		this.speedMachiningZ = speedMachining;
	}




	public double getSpeedTransfer()
	{
		return speedTransfer;
	}




	public void setSpeedTransfer(double speedTransfer)
	{
		this.speedTransfer = speedTransfer;
	}




	public double getDy()
	{
		return dy;
	}




	public void setDy(double toolDiameter)
	{
		this.dy = toolDiameter;
	}




	public double getDx()
	{
		return dx;
	}


	public void setDx(double resolutionX)
	{
		this.dx = resolutionX;
	}


	public double getMachiningTime()
	{
		return machiningTime;
	}


	public void setMachiningTime(double machiningTime)
	{
		this.machiningTime = machiningTime;
	}


	public String getGcode()
	{
		return gcode;
	}

	public String getNGC()
	{
		return gcode+"\nG92.1\n"+"\nM2\n";
	}



	public static String getGcodeFileName()
	{
		return gcodeFileName;
	}


	//
	//
	//public Signal2D1D getShape0( )
	//{
	//return this.shape0;
	//
	//}



	public Signal2D1D getShape( )
	{
		return this.shape;

	}


	public int getNbPassesX()
	{
		return nbPassesX;
	}
	public int getNbPassesY()
	{
		return nbPassesY;
	}
	public int getNbPassesZ()
	{
		return nbPassesZ;
	}

	//
	//public double getScaleXY()
	//{
	//return scaleXY;
	//}
	//
	//
	//public void setScaleXY(double scale)
	//{
	//this.scaleXY = scale;
	//}
	//
	//public double getScaleZ()
	//{
	//return scaleZ;
	//}
	//
	//
	//public void setScaleZ(double scale)
	//{
	//this.scaleZ = scale;
	//}

	public double getDepth()
	{
		return depth;
	}


	public double getToolDiameter() {
		return toolDiameter;
	}


	public void setToolDiameter(double toolDiameter) {
		this.toolDiameter = toolDiameter;
	}


	public double getMinSegmentLength() {
		return minSegmentLength;
	}


	public void setMinSegmentLength(double minSegmentLength) {
		this.minSegmentLength = minSegmentLength;
	}



	public double getzTransfert() {
		return zTransfert;
	}

	public void setzTransfert(double zTransfert) {
		this.zTransfert = zTransfert;
	}




	public XMLElement toXML() 
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		el.setAttribute("toolDiameter",toolDiameter);
		el.setAttribute("speedMachining",speedMachining);
		el.setAttribute("speedMachiningZ",speedMachiningZ);
		el.setAttribute("speedTransfer",speedTransfer);
		el.setAttribute("dzPass",dzPass );
		el.setAttribute("zTransfert",zTransfert);
		el.setAttribute("dx",dx);
		el.setAttribute("dy",dy);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		toolDiameter=xml.getDoubleAttribute("toolDiameter",4);
		speedMachining=xml.getDoubleAttribute("speedMachining",100);
		speedMachiningZ=xml.getDoubleAttribute("speedMachiningZ",50);
		speedTransfer=xml.getDoubleAttribute("speedTransfer",500);
		dzPass=xml.getDoubleAttribute("dzPass",1);
		zTransfert=xml.getDoubleAttribute("zTransfert",2);
		dx=xml.getDoubleAttribute("dx",.2);
		dy=xml.getDoubleAttribute("dy",2);
	}


}//end of class
