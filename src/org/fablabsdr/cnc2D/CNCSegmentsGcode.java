package org.fablabsdr.cnc2D;
import java.awt.Color;

import java.util.Vector;

import org.fablabsdr.cnc.ToolMove;

import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;





/** machine a serie (in Faisceau) of curves */
public class CNCSegmentsGcode  extends Object3DColorSet
{
//links
private PlotSet  entry;//user defined polylines	
private PlotSet  curves;//to show the curves like splines and circles	
private Signal1D1DXY  trajectoireCNC;	
private String gcode;
public double  speedMachining=300;//speed of the tool when machining
public double  speedTransfer=500;//speed of the tool when transfering
public double  zTransfert=500;//z pos of the transfert from one machining to another
public double  dz;//z increment at each pass for machining
public boolean  absolute=false;//if true put absolute coordinates in the Gcode
public double  plateThickness=10;//thickness of the plate to machine
public double  plateSize=300;//size of the (square) plate to machine
public double  toolDiameter=5;//diameter of the cutting tool (milling cutter)
public double  speedMin=10;//min speed of the machine mm/min
public double  speedMax=500;//max speed mm/min
public double  minSegmentLength=0.5;//min segment length use to transform curves to polylines
public int maxIndexDrawing3D=100000;

//internal
private Vector<ToolMove> toolPath3D;
private Vector<Object3DColor> drawing3D;
private static double lastSpeed;

private String path;

public CNCSegmentsGcode(String path)
{
	this.path=path;
}
public void init()
{


CNCUtilOLD.fillTables();
}


public void work()
{
CNCUtilOLD.SPEEDMIN=speedMin;
CNCUtilOLD.SPEEDMAX=speedMax;
if (entry.nbPlots()==0) 
		{
		trajectoireCNC.init(0);
		curves.removeAllPlots();
		drawing3D=new Vector<Object3DColor>();
		return;
		}

//eventually proces the curves to polylines:
PlotSet segments=CNCUtilOLD.curves2polyLines(entry,curves,minSegmentLength);

if (segments.nbPlots()==0) return;

//transform into a contour cut
PlotSet contours=CNCUtilOLD.buildContourTrajectories(segments,toolDiameter/2,minSegmentLength);
//transform the polylines into a unique cutter trajectory, making the passes and adding transfers
toolPath3D=CNCUtilOLD.mergeTrajectories(contours,speedTransfer, speedMachining, zTransfert, dz);
//calculate the G code
gcode=CNCUtilOLD.calculateGcode(toolPath3D,absolute);
updateToolTrajectory(toolPath3D,trajectoireCNC);

//calculate the W code
//wcode.val=CNCUtil.gcode2wcode(trajectoireCNC, gcode.val, stepX.val,stepY.val,stepZ.val);

String filename=path+"seeNsee2D.gcode";
TextFiles.saveString(filename, gcode);
System.out.println(filename+" saved");
}

public void updateToolTrajectory(Vector<ToolMove> toolPath3D,Signal1D1DXY  trajectoireCNC)
{
trajectoireCNC.init(0);
for (int i=0;i<toolPath3D.size();i++)
	{
	Segment3D seg=toolPath3D.elementAt(i).getSegment3D();
	trajectoireCNC.addPoint(seg.p1().x(),seg.p1().y());
	}
Segment3D seg=toolPath3D.elementAt(toolPath3D.size()-1).getSegment3D();
trajectoireCNC.addPoint(seg.p2().x(),seg.p2().y());

}

@Override
public Vector<Object3DColor> getObject3DList() 
{	
	//initialize the drawing 3D
	drawing3D=new Vector<Object3DColor>();
	//return empty if toolPath3D null
	if (toolPath3D==null) return drawing3D ;
	//draw the upper plane of the material
	double rx=plateSize/2;
	double rz=plateThickness;
	drawing3D.add(new Quadrilatere3DColor(new Vecteur(rx,rx,0),new Vecteur(rx,-rx,0),new Vecteur(-rx,-rx,0),new Vecteur(-rx,rx,0),Color.orange,false));
	drawing3D.add(new Quadrilatere3DColor(new Vecteur(rx,rx,-rz),new Vecteur(rx,-rx,-rz),new Vecteur(-rx,-rx,-rz),new Vecteur(-rx,rx,-rz),Color.orange,false));
	drawing3D.add(new Segment3DColor(new Vecteur(rx,rx,-rz),new Vecteur(rx,rx,0),Color.orange.getRGB()));
	drawing3D.add(new Segment3DColor(new Vecteur(rx,-rx,-rz),new Vecteur(rx,-rx,0),Color.orange.getRGB()));
	drawing3D.add(new Segment3DColor(new Vecteur(-rx,rx,-rz),new Vecteur(-rx,rx,0),Color.orange.getRGB()));
	drawing3D.add(new Segment3DColor(new Vecteur(-rx,-rx,-rz),new Vecteur(-rx,-rx,0),Color.orange.getRGB()));
	//draw the tool path
	//int maxIndexDrawing3D=this.vInt("maxIndexDrawing3D");
	//for (int i=0;i<toolPath3D.size();i++) 
	for (int i=0;i<(int)Math.min(maxIndexDrawing3D,toolPath3D.size());i++) 	
		drawing3D.add(toolPath3D.elementAt(i).getSegment3D());
	//draw the tool at the last position:
	double tx=toolDiameter/2;
	double ty=toolDiameter/2;
	double tz=toolDiameter*4;
	Vecteur lastPoint=((Segment3D)drawing3D.elementAt(drawing3D.size()-1)).p2();
	double lx=lastPoint.x();
	double ly=lastPoint.y();
	double lz=lastPoint.z();
	int nbPts=8;
	double x=toolDiameter/2;
	double y=0;
	double x1,y1;
	for (int i=1;i<=nbPts;i++)
		{
		double angle=(Math.PI*2/nbPts)*i;
		x1=toolDiameter/2*Math.cos(angle);
		y1=toolDiameter/2*Math.sin(angle);
		drawing3D.add(new Segment3DColor(new Vecteur(lx+x,ly+y,lz),new Vecteur(lx+x1,ly+y1,lz),Color.orange.getRGB()));
		drawing3D.add(new Segment3DColor(new Vecteur(lx,ly,lz),new Vecteur(lx+x,ly+y,lz),Color.orange.getRGB()));
		drawing3D.add(new Segment3DColor(new Vecteur(lx+x,ly+y,lz),new Vecteur(lx+x,ly+y,lz+tz),Color.orange.getRGB()));
		x=x1;
		y=y1;
		}
//	drawing3D.add(new Segment3D(new Vecteur(lx-tx,ly,lz),new Vecteur(lx+tx,ly,lz),Color.orange.getRGB()));
//	drawing3D.add(new Segment3D(new Vecteur(lx,ly-ty,lz),new Vecteur(lx,ly+ty,lz),Color.orange.getRGB()));
//	drawing3D.add(new Segment3D(new Vecteur(lx-tx,ly,lz),new Vecteur(lx-tx,ly,lz+tz),Color.orange.getRGB()));
//	drawing3D.add(new Segment3D(new Vecteur(lx+tx,ly,lz),new Vecteur(lx+tx,ly,lz+tz),Color.orange.getRGB()));
//	drawing3D.add(new Segment3D(new Vecteur(lx,ly-ty,lz),new Vecteur(lx,ly-ty,lz+tz),Color.orange.getRGB()));
//	drawing3D.add(new Segment3D(new Vecteur(lx,ly+ty,lz),new Vecteur(lx,ly+ty,lz+tz),Color.orange.getRGB()));
return drawing3D;
}


	 
}//end of class
