package org.fablabsdr.cnc2D;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import org.fablabsdr.cnc.ToolMove;
import org.fablabsdr.cnc.arduino.ArduinoMessagesFloat;

import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.math.function.Function1D2D;
import com.photonlyx.toolbox.math.function.Function1D2DTangent;
import com.photonlyx.toolbox.math.function.usual1D2D.ArcCircleCurve;
import com.photonlyx.toolbox.math.function.usual1D2D.CircleCurve;
import com.photonlyx.toolbox.math.geometry.Line2D;
import com.photonlyx.toolbox.math.geometry.Segment2D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.spline.Bspline;
import com.photonlyx.toolbox.math.spline.BsplineCurve;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.util.Messager;



public class CNCUtilOLD 
{
	
	
//tables for displacement combinations
private static int max=50;
private static int[]  dxs=new int[max*max/2];
private static int[]  dys=new int[max*max/2];
private static int[]  pgcds=new int[max*max/2];
private static double[]  tans=new double[max*max/2];
private static int nbAngles;

//max length of message due to serial buffer
private static int maxArduinoMessageLength=60;

public static double SPEEDMIN=11;
public static double SPEEDMAX=280;
private static DecimalFormatSymbols dfs=new DecimalFormatSymbols(Locale.ENGLISH);
private static DecimalFormat df0=new DecimalFormat("0",dfs);
private static DecimalFormat df3=new DecimalFormat("0.000",dfs);

static Vector<Integer> primeNumbers(int max)
{
Vector<Integer> v=new Vector<Integer>();
for (int i=3;i<max;i++)
{
boolean flag=true;
for (int j=2;j<i;j++) if ((i/j)-((double)i/(double)j)==0) 
	{
	flag=false;
	break;
	}
if (flag) 
		{
		v.add(new Integer(i));
		//System.out.println(i);
		}
}
return v;
}



static int pgcd2(int a, int b) 
{
 if (b == 0) {
 return a;
 } else if (a >= b) {
 return pgcd2(a - b, b);
 } else {
	 return pgcd2(b, a);
 }
} 
 
 
/**
 * find a combination to move by dx and dy
 * @param stepX
 * @param stepY
 * @param _dx
 * @param _dy
 * @param comb
 */
public  static void findDxDy(double stepX,double stepY,double _dx,double _dy,double[] comb)
{
double[] combReduit=new double[3];

double abs_dx=Math.abs(_dx);
double abs_dy=Math.abs(_dy);
int sign_dx=(int)Math.signum(_dx);
int sign_dy=(int)Math.signum(_dy);

double dx,dy,mult;
//System.out.println("_dx="+_dx);
//System.out.println("_dy="+_dy);

if (abs_dx>=abs_dy) 
	{
	findcombination(stepX,stepY,abs_dx,abs_dy,combReduit);
	dx=combReduit[0]*sign_dx;
	dy=combReduit[1]*sign_dy;
	mult=combReduit[2];
	}
else 
	{
	findcombination(stepX,stepY,abs_dy,abs_dx,combReduit);
	dx=combReduit[1]*sign_dx;
	dy=combReduit[0]*sign_dy;
	mult=combReduit[2];
	}

//System.out.println("dx="+dx);
//System.out.println("dy="+dy);
//System.out.println("mult="+mult);
//System.out.println("dxfinal="+dx*mult*step);
//System.out.println("dyfinal="+dy*mult*step);

comb[0]=dx;
comb[1]=dy;
comb[2]=mult;
}
	
	

/**
 * find a combination to move by dx and dy, dx and dy must >0 and dx>dy
 * @param dx
 * @param dy
 * @param combReduit the combination calculated:array of 3 numbers: dx dy and pgcd
 * @param squareSize
 */
private static void findcombination(double stepX,double stepY,double _dx,double _dy,double[] combReduit)
{
//look for the closest combination  //TODO can be optimized by sorting
double tan=_dy/_dx;
double bestDiff=1e30;
int indexBestAngle=0;
for (int i=0;i<nbAngles;i++)
	{
	double  diff=Math.abs(tan-tans[i]);
	if (diff<bestDiff)
		{
		indexBestAngle=i;
		bestDiff=diff;
		}
	}
//System.out.println("dx="+_dx+" _dy="+_dy+" tan="+tan);
//System.out.println("best"+" dx="+dxs[indexBestAngle]+"dy="+dys[indexBestAngle]+" tan="+tans[indexBestAngle]);
		
combReduit[0]=dxs[indexBestAngle];
combReduit[1]=dys[indexBestAngle];
combReduit[2]=(_dx/stepX/dxs[indexBestAngle]);
}

/**
 * fill the 
 */
public  static void fillTables()
{
 //check all the possibles angles (tan)

int index=0;

		dxs[index]=1;
		dys[index]=0;
		pgcds[index]=1;		
		tans[index]=0;
		//System.out.println(dxs[index]+" "+dys[index]+" "+df3.format(tans[index]));
		index++;

for (int dx=1;dx<=max;dx++)
	for (int dy=1;dy<dx;dy++)
		{
		int pgcd=pgcd2(dx,dy);
		if ((pgcd>1)&(dx!=1)) continue;
		dxs[index]=dx/pgcd;
		dys[index]=dy/pgcd;
		pgcds[index]=pgcd;		
		tans[index]=(double)dy/(double)dx;
		//System.out.println(dxs[index]+" "+dys[index]+" "+df3.format(tans[index]));
		index++;
		}
 nbAngles=index-1;
}

/**
 * add a movement in (X,Y) plane of (_dx,_dy)
 * @param arduinoMessages
 * @param _dx
 * @param _dy
 * @param _stepX movement done by one step of the stepping motor in X
 * @param _stepY movement done by one step of the stepping motor in Y
 * @param trajectoireCNC
 */
public  static void addMovementXY(Vector<StringBuffer> arduinoMessages, double _dx,double _dy,double _stepX,double _stepY,Signal1D1DXY trajectoireCNC)
{
StringBuffer sb=arduinoMessages.lastElement();

if (sb.length()>maxArduinoMessageLength) 
	{
	sb=new StringBuffer();
	arduinoMessages.add(sb);
	}

double[] comb=new double[3];
CNCUtilOLD.findDxDy(_stepX,_stepY,_dx, _dy,comb);
int dx=(int)comb[0];
int dy=(int)comb[1];
int mult=(int)comb[2];		
if (!((dx==0)&(dy==0))&&(mult!=0) )
	{
	sb.append("n"+mult);
	sb.append("x"+dx);
	sb.append("y"+dy);
	sb.append(";");

	//update the simulated trajectory:
	//get the last point
	double nx=trajectoireCNC.x(trajectoireCNC.getDim()-1);
	double ny=trajectoireCNC.y(trajectoireCNC.getDim()-1);
	//add the new points start with x move and ends with y movement:
	for (int j=0;j<mult;j++)
		{
		nx+=dx*_stepX;
		trajectoireCNC.addPoint(nx, ny);
		ny+=dy*_stepY;
		trajectoireCNC.addPoint(nx, ny);
		}
	 }

//go to the final point (to avoid accumulating errors with multi segments
//af("reste:");
double resteX=_dx-dx*mult*_stepX;
double resteY=_dy-dy*mult*_stepY;
CNCUtilOLD.findDxDy(_stepX,_stepY,resteX, resteY,comb);
 dx=(int)(comb[0]*comb[2]);
 dy=(int)(comb[1]*comb[2]);
 mult=1;
 if (!((dx==0)&(dy==0))&&(mult!=0) )
	 {
	sb.append("n"+mult);
	sb.append("x"+dx);
	sb.append("y"+dy);
	sb.append(";");
	//update the simulated trajectory: 
	//get the last point
	double nx=trajectoireCNC.x(trajectoireCNC.getDim()-1);
	double ny=trajectoireCNC.y(trajectoireCNC.getDim()-1);
	nx+=dx*_stepX;
	trajectoireCNC.addPoint(nx, ny);
	// nx=trajectoireCNC.x(i)+mult*dx*step.val;
	ny+=dy*_stepY;
	trajectoireCNC.addPoint(nx, ny);
	}
// resteX=_dx-dx*mult*_step;
// resteY=_dy-dy*mult*_step;
		
}


public  static  void addMovementZ(Vector<StringBuffer> arduinoMessages, double _dz,double _stepZ)
{
int dz=(int)(_dz/_stepZ);
StringBuffer sb=arduinoMessages.lastElement();
if (sb.length()>maxArduinoMessageLength) 
	{
	sb=new StringBuffer();
	arduinoMessages.add(sb);
	}
if (dz!=0 ) sb.append("z"+dz+";");
}

public  static void addSpeed(Vector<StringBuffer> arduinoMessages, int speed)
{
StringBuffer sb=arduinoMessages.lastElement();
if (sb.length()>maxArduinoMessageLength) 
	{
	sb=new StringBuffer();
	arduinoMessages.add(sb);
	}
sb.append("s"+speed+";");	
}

/**
 * transform a curve (1D -> 2D) curve abscisse t from 0 to 1
 * @param curve
 * @param segmentLength the length of the elementary segments
 * @return
 */
public static Signal1D1D curve2Segments(Function1D2D curve,double segmentLength) 
{
Signal1D1DXY segments=new Signal1D1DXY();
Function1D2D tangent=new Function1D2DTangent(curve,false,0.000001);
double t=0.00000;
double[] P=curve.f(t);
segments.addPoint(P[0], P[1]);

for(;;)
	{
	double[] tan=tangent.f(t);
	System.out.println("tang="+tan[0]+" "+tan[1]);
	double normeTan=Math.sqrt(Math.pow(tan[0],2)+Math.pow(tan[1],2));
	double dt= segmentLength/normeTan;
	System.out.println("dt="+dt);
	t+=dt;
	if (t>1) t=1;
	P=curve.f(t);
	segments.addPoint(P[0], P[1]);
	if (t==1) break;
	}

return segments;
}



public static String gcode2wcode(Signal1D1DXY  trajectoireCNC,String gcode,double stepX,double stepY,double stepZ)
{
fillTables();
double[] vals=new double[30];
char[] tags=new char[30];

Vector<StringBuffer> arduinoMessages=new Vector<StringBuffer>();
arduinoMessages.removeAllElements();
arduinoMessages.add(new StringBuffer());
trajectoireCNC.init(0);
trajectoireCNC.addPoint(new double[2]);
fillTables();

double xabs=0,yabs=0,zabs=0;//absolute position of the tool
double dx=0,dy=0,dz=0;
String[] lines=StringSource.readLines(gcode);
String s;
for (int i=0;i<lines.length;i++)
	{
	//System.out.println(lines[i]);
	s=lines[i].replace(" ", "");
	int nbVals=ArduinoMessagesFloat.decodeMessage(s, tags, vals);
	if (tags[0]=='N')
		{
//		System.out.println("Lecture: ");
//		for (int k=0;k<nbVals;k++) System.out.print(tags[i]+vals[i]+" ");
//		System.out.println();
		if ((tags[1]=='G')&((vals[1]==01)||(vals[1]==00)))
			{
			double x=xabs,y=yabs,z=zabs;
			double speedMmPerMin=0;
			for (int k=0;k<nbVals;k++)
				{
				if (tags[k]=='X') x=vals[k];
				if (tags[k]=='Y') y=vals[k];
				if (tags[k]=='Z') z=vals[k];
				if (tags[k]=='F') speedMmPerMin=vals[k];				
				}
			//System.out.println("G "+x+" "+y+" "+z);
			dx=x-xabs;
			dy=y-yabs;
			dz=z-zabs;
			if (speedMmPerMin!=0)
				{
//				double dt=stepX/2/speedMmPerMin*60*1000000;//TODO take all steps into account
				double dt=timeBetweenSteps_us( stepX, speedMmPerMin);
				addSpeed( arduinoMessages,(int)dt);
				}
			addMovementXY(arduinoMessages,dx,dy, stepX,stepY, trajectoireCNC);
			addMovementZ(arduinoMessages,dz, stepZ);
			//the absolute position of the tool is not where
			xabs=trajectoireCNC.x(trajectoireCNC.getDim()-1);
			yabs=trajectoireCNC.y(trajectoireCNC.getDim()-1);
//			xabs=x;
//			yabs=y;
			zabs=z;
			}
		}
	}
String command="";
for (int i=0;i<arduinoMessages.size();i++)
	{
	StringBuffer sb1=arduinoMessages.elementAt(i);
	String toSend=sb1.toString();
	//System.out.println(toSend);
	command+=toSend+"\n";
	}
return command;

}

/**
 * convert speed in mm/min into time between 2 steps in microseconds
 * @param stepX
 * @param speedMmPerMin
 * @return
 */
public static int timeBetweenSteps_us(double stepX,double speedMmPerMin)
{
return (int)(stepX/2/speedMmPerMin*60*1000000);//TODO take all steps into account
}



/**
 * get the last point of the tool path
 * @param toolPath3D
 * @return
 */
public static Vecteur lastPoint(Vector<ToolMove> toolPath3D)
{
return toolPath3D.elementAt(toolPath3D.size()-1).p2();	
}



/**
 * add a movement incremental dx dy dz in the toolPath
 * @param _dx
 * @param _dy
 * @param _dz
 * @param speed
 * @param toolPath3D
 */
public static void addMovementIncremental(double _dx,double _dy,double _dz,double speed,Vector<ToolMove> toolPath3D)
{
Vecteur p1=lastPoint(toolPath3D);
Vecteur p=p1.copy();
p.addn(new Vecteur(_dx,_dy,_dz));
Vecteur p2=p;
float hue=-0.09f+(float)(speed-SPEEDMIN)/(float)(SPEEDMAX-SPEEDMIN)*0.78f;
if (hue>1) hue-=1;
Color c=Color.getHSBColor(hue,1.0f,1.0f);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,c.getRGB()),speed));
}

/**
 * add a movement toward absolute coordinates x y z in the toolPath
 * @param x
 * @param y
 * @param z
 * @param speed speed of the movement
 * @param toolPath3D path to add the move
 */
public static void addMovementAbsolute(double x,double y,double z,double speed,Vector<ToolMove> toolPath3D)
{
Vecteur p1=lastPoint(toolPath3D);
Vecteur p2=new Vecteur(x,y,z);
float hue=-0.09f+(float)(speed-SPEEDMIN)/(float)(SPEEDMAX-SPEEDMIN)*0.78f;
if (hue>1) hue-=1;
Color c=Color.getHSBColor(hue,1.0f,1.0f);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,c.getRGB()),speed));
}



/**
 * //transform polyLines into a contour polyLine
 * @param curves
 * @param toolRadius
 * @return
 */
public static PlotSet buildContourTrajectories(PlotSet curves,double toolRadius,double minSegmentLength)
{
boolean contourAtRight;//if true the contour is done at the right of the segments
PlotSet f=new PlotSet();
for (int k=0;k<curves.nbPlots();k++)
	{
	Plot plot=curves.getPlot(k);
	if (plot.getLabel().contains("drilling")) 
		{
		f.add(plot);
		continue;
		}
	if (plot.getLabel().contains("engraving")) 
		{
		f.add(plot);
		continue;
		}
	if (plot.getLabel().contains("right")) contourAtRight=true;else contourAtRight=false;
	Signal1D1D signal=plot.getSignal();
	Signal1D1DXY parallel=new Signal1D1DXY();
	//construct the segments parallel
	for (int i=0;i<signal.getDim()-1;i++)
		{
		//take a segment
		//first point 
		Vecteur2D p1=new Vecteur2D(signal.x(i),signal.y(i));
		//second point
		Vecteur2D p2=new Vecteur2D(signal.x(i+1),signal.y(i+1));
		//the segment
		Vecteur2D segment=new Vecteur2D(signal.x(i+1)-signal.x(i),signal.y(i+1)-signal.y(i));
		//calc the perpendicular vector to the segment 
		Vecteur2D perp=segment.getPerpVector(contourAtRight).normalisen().scmul(toolRadius);
		//first point translated perpendicularly with tool radius
		Vecteur2D p1bis=p1.addn(perp);
		Vecteur2D p2bis=p2.addn(perp);
		parallel.addPoint(p1bis.x(),p1bis.y());
		parallel.addPoint(p2bis.x(),p2bis.y());
//		if (i==(signal.getDim()-2) )
//			{
//			p1=new Vecteur2D(signal.x(i+1),signal.y(i+1));
//			p2=p1.addn(perp);
//			parallel.addPoint(p2.x(),p2.y());
//			}
		}
	
	//check each node: interior or exterior ?
	//if the consecutive segments intersect,it is interior, cut them , if not complete with circle arc
	boolean[] interior=new boolean[signal.getDim()];
	Vecteur2D[] intersectOrCenter=new Vecteur2D[signal.getDim()];
	for (int i=0;i<parallel.getDim()-3;i+=2)
		{
		int indexOfNodeOnSignal=i/2+1;
		//a segment
		Vecteur2D p1=new Vecteur2D(parallel.x(i),parallel.y(i));
		Vecteur2D p2=new Vecteur2D(parallel.x(i+1),parallel.y(i+1));
		Vecteur2D p1bis=new Vecteur2D(parallel.x(i+2),parallel.y(i+2));
		Vecteur2D p2bis=new Vecteur2D(parallel.x(i+3),parallel.y(i+3));
		Segment2D seg1=new Segment2D(p1,p2);
		Segment2D seg2=new Segment2D(p1bis,p2bis);
		//check segment intersection
		Line2D l1=new Line2D(seg1);
		Line2D l2=new Line2D(seg2);
		Vecteur2D inter=l1.getIntersectionPointWith(l2);
		if (seg1.isOnSegment(inter))//segments intersect
			{
			interior[indexOfNodeOnSignal]=true;
			intersectOrCenter[indexOfNodeOnSignal]=inter;
			}
		else
			{
			interior[indexOfNodeOnSignal]=false;
			//the centre if the point of the original polyline:
			Vecteur2D node=new Vecteur2D(signal.x(indexOfNodeOnSignal),signal.y(indexOfNodeOnSignal));
			intersectOrCenter[indexOfNodeOnSignal]=node;
			}
		}
//check if the first and last segments intersects
if (signal.getDim()>=3)
	{
	int i1=0;
	int i2=parallel.getDim()-3;
	int indexOfNodeOnSignal1=0;
	//a segment
	Vecteur2D p1=new Vecteur2D(parallel.x(i1),parallel.y(i1));
	Vecteur2D p2=new Vecteur2D(parallel.x(i1+1),parallel.y(i1+1));
	Vecteur2D p1bis=new Vecteur2D(parallel.x(i2+1),parallel.y(i2+1));
	Vecteur2D p2bis=new Vecteur2D(parallel.x(i2+2),parallel.y(i2+2));
	Segment2D seg1=new Segment2D(p1,p2);
	Segment2D seg2=new Segment2D(p1bis,p2bis);
	//check segment intersection
	Line2D l1=new Line2D(seg1);
	Line2D l2=new Line2D(seg2);
	Vecteur2D inter=l1.getIntersectionPointWith(l2);
	if (seg1.isOnSegment(inter))//segments intersect
		{
		interior[indexOfNodeOnSignal1]=true;
		intersectOrCenter[indexOfNodeOnSignal1]=inter;
		}
	else
		{
		interior[indexOfNodeOnSignal1]=false;
		//the centre if the point of the original polyline:
		Vecteur2D node=new Vecteur2D(signal.x(indexOfNodeOnSignal1),signal.y(indexOfNodeOnSignal1));
		intersectOrCenter[indexOfNodeOnSignal1]=node;
		}
	}	

	//build another polyline with corrections of trajectory
	Signal1D1DXY parallelCorrected=new Signal1D1DXY();
	
	//put the first point 
	if (interior[0]) parallelCorrected.addPoint(intersectOrCenter[0]);
	else parallelCorrected.addPoint(parallel.x(0),parallel.y(0));
	
	for (int i=0;i<parallel.getDim()-3;i+=2)
		{
		int indexOfNodeOnSignal=i/2+1;
		//a segment
		Vecteur2D p2=new Vecteur2D(parallel.x(i+1),parallel.y(i+1));
		Vecteur2D p1bis=new Vecteur2D(parallel.x(i+2),parallel.y(i+2));
		//parallelCuttedInterior.addPoint(p1);
		if (interior[indexOfNodeOnSignal])//interior
			{
			Vecteur2D inter=intersectOrCenter[indexOfNodeOnSignal];
			parallelCorrected.addPoint(inter);
			}
		else //join with a circle arc
			{
			Vecteur2D centre=intersectOrCenter[indexOfNodeOnSignal];
			//the radius is the same as the tool radius:
			double radius=toolRadius;
			//angles of start and end of the arc circle
			double alpha1=p2.subn(centre).polarAngle();
			double alpha2=p1bis.subn(centre).polarAngle();
			if (Math.abs(alpha2-alpha1)>Math.PI)//circle would go on the wrong side
				{
				if (alpha1<alpha2) alpha1+=2*Math.PI;
				else alpha2+=2*Math.PI;
				}
			ArcCircleCurve circle=new ArcCircleCurve(centre,radius,alpha1,alpha2);
			Signal1D1DXY arcCircle=getSegments(circle,minSegmentLength);
			//parallelAdapted.addPoint(p2);
			//parallelAdapted.addPoint(p1bis);
			for (int j=0;j<arcCircle.getDim();j++) parallelCorrected.addPoint(arcCircle.x(j),arcCircle.y(j));
			}
		//parallelCuttedInterior.addPoint(p2bis);
		}
	//put the last point unmodified
	if (interior[0]) parallelCorrected.addPoint(intersectOrCenter[0]);//same place as first point
	else parallelCorrected.addPoint(parallel.x(parallel.getDim()-1),parallel.y(parallel.getDim()-1));//unmodified
	
	Plot plotAdapted=f.add(parallelCorrected);
	plotAdapted.setLabel(plot.getLabel());
	}
return f;
}


/**
 * transform the curves (polyLines, circles, splines) into polyLines only
 * @param entry
 * @return
 */
public static PlotSet curves2polyLines(PlotSet entry,PlotSet curves,double minSegmentLength)
{
PlotSet f=new PlotSet();
curves.removeAllPlots();
for (int i=0;i<entry.nbPlots();i++)
	{
	Plot plot=entry.getPlot(i);
	if (plot.getLabel().contains("off")) continue;
	if (plot.getLabel().contains("spline"))//spline curve defined by control points
		{
		Signal1D1D ctrlPts=plot.getSignal();
		if (ctrlPts.getDim()<=(3+1)) 
			{
			Messager.messErr("Not enough points for the spline:"+plot.getLabel());
			continue;
			}
		BsplineCurve spline=new BsplineCurve(ctrlPts,3);
		spline.update();
		Signal1D1D polyLine=getSegments(spline,minSegmentLength);
		curves.add(polyLine);
		curves.getPlot(curves.nbPlots()-1).setColor("lightGray");
		curves.getPlot(curves.nbPlots()-1).setLabel(plot.getLabel()+" (curve)");
		f.add(new Plot(polyLine));
		f.getPlot(f.nbPlots()-1).setLabel(plot.getLabel());
		}
	else if (plot.getLabel().contains("fullCircle"))//circle arc defined by 3 points
		{
	//	double x=new Definition(plot.label()).searchValue("x", 0, false);
	//	double y=new Definition(plot.label()).searchValue("y", 0, false);
	//	double r=new Definition(plot.label()).searchValue("r", 0, false);
		double xc=plot.getSignal().x(0);
		double yc=plot.getSignal().y(0);
		double x=plot.getSignal().x(1);
		double y=plot.getSignal().y(1);
		double r=Math.sqrt(Math.pow(x-xc,2)+Math.pow(y-yc,2));
		CircleCurve circle=new CircleCurve(xc,yc,r);
		Signal1D1D polyLine=getSegments(circle,minSegmentLength);
		curves.add(polyLine);
		curves.getPlot(curves.nbPlots()-1).setColor("lightGray");
		curves.getPlot(curves.nbPlots()-1).setLabel(plot.getLabel()+" (curve)");
		f.add(polyLine);
		f.getPlot(f.nbPlots()-1).setLabel(plot.getLabel());
		}
	else if (plot.getLabel().contains("3pointsCircle"))//circle arc defined by 3 points
		{
		Vecteur2D p1=new Vecteur2D(plot.getSignal().x(0),plot.getSignal().y(0));
		Vecteur2D p2=new Vecteur2D(plot.getSignal().x(1),plot.getSignal().y(1));
		Vecteur2D p3=new Vecteur2D(plot.getSignal().x(2),plot.getSignal().y(2));
		CircleCurve circle=new CircleCurve(p1,p2,p3);
		Signal1D1D polyLine=getSegments(circle,minSegmentLength);
		curves.add(polyLine);
		curves.getPlot(curves.nbPlots()-1).setLabel(plot.getLabel()+" (curve)");
		curves.getPlot(curves.nbPlots()-1).setColor("lightGray");
		f.add(polyLine);
		f.getPlot(f.nbPlots()-1).setLabel(plot.getLabel());
		}
	else if (plot.getLabel().contains("3pointsArcCircle"))//circle arc defined by 3 points
		{
		Vecteur2D p1=new Vecteur2D(plot.getSignal().x(0),plot.getSignal().y(0));
		Vecteur2D p2=new Vecteur2D(plot.getSignal().x(1),plot.getSignal().y(1));
		Vecteur2D p3=new Vecteur2D(plot.getSignal().x(2),plot.getSignal().y(2));
		ArcCircleCurve circle=new ArcCircleCurve(p1,p2,p3);
		Signal1D1D polyLine=getSegments(circle,minSegmentLength);
		curves.add(polyLine);
		curves.getPlot(curves.nbPlots()-1).setLabel(plot.getLabel()+" (curve)");
		curves.getPlot(curves.nbPlots()-1).setColor("lightGray");
		f.add(polyLine);
		f.getPlot(f.nbPlots()-1).setLabel(plot.getLabel());
		}
	else f.add(entry.getPlot(i));//take the same polyline as is
	}
return f;
}


/**
 * merge the polyLines into a unique tool trajectory, making the passes and adding the transfers. 
 * Result in toolPath3D.
 * 
 * @param segments
 * @param toolPath3D
 * @param speedTransfer
 * @param speedMachining
 * @param zTransfert
 * @param dzPass
 */
public static Vector<ToolMove> mergeTrajectories(PlotSet  segments,
			double  speedTransfer,double speedMachining,double zTransfert,double dzPass)
{
//if (segments.nbPlots()==0) return null;
Vector<ToolMove> toolPath3D=new Vector<ToolMove>();
Vecteur p1=new Vecteur(0,0,0);
Vecteur p2=new Vecteur(0,0,0);
toolPath3D.add(new ToolMove(new Segment3DColor(p1,p2,Color.red.getRGB()),0) );

//first transfer from 0,0 to the the first point, the tool is placed manually to 0,0 before starting
{
//put the  z for transfer:
double _dz= zTransfert-CNCUtilOLD.lastPoint(toolPath3D).z();
CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);

//make the transfer
double _dx=segments.getPlot(0).getSignal().x(0)-0;
double _dy=segments.getPlot(0).getSignal().y(0)-0;
CNCUtilOLD.addMovementIncremental(_dx,0,0,speedTransfer,toolPath3D);
CNCUtilOLD.addMovementIncremental(0,_dy,0,speedTransfer,toolPath3D);

//back the z
 _dz=-_dz;
 CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
}

for (int curve=0;curve<segments.nbPlots();curve++)
	{
	Plot plot=segments.getPlot(curve);
	if (curve>=1)  //make the transfer from one machining to another
		{
		//put the  z for transfer:
		double _dz= zTransfert-CNCUtilOLD.lastPoint(toolPath3D).z();
		CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
		
		//make the transfer
		Signal1D1D signalPrec=segments.getPlot(curve-1).getSignal();
		Signal1D1D signal=segments.getPlot(curve).getSignal();
		double _dx=signal.x(0)-signalPrec.x(signalPrec.getDim()-1);
		double _dy=signal.y(0)-signalPrec.y(signalPrec.getDim()-1);
		CNCUtilOLD.addMovementIncremental(_dx,0,0,speedTransfer,toolPath3D);
		CNCUtilOLD.addMovementIncremental(0,_dy,0,speedTransfer,toolPath3D);
		//back the z
		 _dz=-_dz;
		 CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
		}
	
	Signal1D1D signal=plot.getSignal();
	
	//make the machining
	
	//the max  z for machining  is in the curve label:
	//if no depth is annoted in the label, put on pass:
	if (!new Definition(plot.getLabel()).hasWord("z")) plot.setLabel("z -"+dzPass+" "+plot.getLabel());
	
	if (plot.getLabel().contains("drilling"))
		{//drilling
		double zBottom=new Definition(plot.getLabel()).searchValue("z", 0, false);//depth of drilling
			
		//drill 1st pointzTransfert
		CNCUtilOLD.addMovementIncremental(0,0,zBottom,speedMachining,toolPath3D);
		CNCUtilOLD.addMovementIncremental(0,0,zTransfert-zBottom,speedTransfer,toolPath3D);
		//drill the other points
		for(int i=0;i<signal.getDim()-1;i++)//segments loop
			{
			//drill:
			double _dx=signal.x(i+1)-signal.x(i);
			double _dy=signal.y(i+1)-signal.y(i);		
			CNCUtilOLD.addMovementIncremental(_dx,_dy,0,speedTransfer,toolPath3D);
			CNCUtilOLD.addMovementIncremental(0,0,0-zTransfert,speedTransfer,toolPath3D);
			CNCUtilOLD.addMovementIncremental(0,0,zBottom-0,speedMachining,toolPath3D);
			CNCUtilOLD.addMovementIncremental(0,0,zTransfert-zBottom,speedTransfer,toolPath3D);
			}//end of segments loop		
		//go back to z=0
		double _dz=0-CNCUtilOLD.lastPoint(toolPath3D).z();
		CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
		}//drilling end
//	else
//		if (plot.label().contains("engraving"))
//		{//engraving
//		double zBottom=new Definition(plot.label()).searchValue("z", 0, false);//depth of drilling
//			
//		//drill 1st pointzTransfert
//		CNCUtil.addMovementIncremental(0,0,zBottom,speedMachining,toolPath3D);
//		CNCUtil.addMovementIncremental(0,0,zTransfert-zBottom,speedTransfer,toolPath3D);
//		//drill the other points
//		for(int i=0;i<signal.getDim()-1;i++)//segments loop
//			{
//			//drill:
//			double _dx=signal.x(i+1)-signal.x(i);
//			double _dy=signal.y(i+1)-signal.y(i);		
//			CNCUtil.addMovementIncremental(_dx,_dy,0,speedTransfer,toolPath3D);
//			CNCUtil.addMovementIncremental(0,0,0-zTransfert,speedTransfer,toolPath3D);
//			CNCUtil.addMovementIncremental(0,0,zBottom-0,speedMachining,toolPath3D);
//			CNCUtil.addMovementIncremental(0,0,zTransfert-zBottom,speedTransfer,toolPath3D);
//			}//end of segments loop		
//		//go back to z=0
//		double _dz=0-CNCUtil.lastPoint(toolPath3D).z();
//		CNCUtil.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
//		}//engraving end
	else
		{//contour machining
		double toCut=0-new Definition(plot.getLabel()).searchValue("z", 0, false);
		int nbPasses=(int) Math.floor(toCut/dzPass)+1;
		//the first passes are complete, the last is smaller to reach the good z
		double lastPass=toCut-(nbPasses-1)*dzPass;
//		System.out.println("toCut="+toCut);
//		System.out.println("nbPasses="+nbPasses);
//		System.out.println("lastPass="+lastPass);
		for (int k=0;k<nbPasses;k++)	
			{
			double _dz=-dzPass;
			if (k==(nbPasses-1)) _dz=-lastPass;
			CNCUtilOLD.addMovementIncremental(0,0,_dz,speedMachining,toolPath3D);
			
			//make the machining in XY plane:
			Vecteur P1=CNCUtilOLD.lastPoint(toolPath3D);
			for(int i=0;i<signal.getDim()-1;i++)//segments loop
				{
				double _dx=signal.x(i+1)-signal.x(i);
				double _dy=signal.y(i+1)-signal.y(i);		
				CNCUtilOLD.addMovementIncremental(_dx,_dy,0,speedMachining,toolPath3D);
				}//end of segments loop
			
			Vecteur P2=CNCUtilOLD.lastPoint(toolPath3D);
			//go back to the begging of the curve:
			if (k<nbPasses-1)
				{
				 _dz= zTransfert-CNCUtilOLD.lastPoint(toolPath3D).z();
				 CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
				//make the transfer
				 double _dx=-(P2.x()-P1.x());
				 double _dy=-(P2.y()-P1.y());
				 CNCUtilOLD.addMovementIncremental(_dx,0,0,speedTransfer,toolPath3D);
				 CNCUtilOLD.addMovementIncremental(0,_dy,0,speedTransfer,toolPath3D);
				//back the z to last one 
				 _dz= -_dz;
				 CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
				}
			}
		//go back to z=0
		double _dz=0-CNCUtilOLD.lastPoint(toolPath3D).z();
		CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
		}//end contour machining				

	
	}//end of curves loop


//back to  0,0,0
	{
	//put the  z for transfer:
	double _dz= zTransfert-CNCUtilOLD.lastPoint(toolPath3D).z();
	CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
	//make the transfer
	Signal1D1D signal=segments.getPlot(segments.nbPlots()-1).getSignal();
	double _dx=0-signal.x(signal.getDim()-1);
	double _dy=0-signal.y(signal.getDim()-1);
	CNCUtilOLD.addMovementIncremental(_dx,0,0,speedTransfer,toolPath3D);
	CNCUtilOLD.addMovementIncremental(0,_dy,0,speedTransfer,toolPath3D);
	//back to z=0
	 _dz=0-CNCUtilOLD.lastPoint(toolPath3D).z();
	 CNCUtilOLD.addMovementIncremental(0,0,_dz,speedTransfer,toolPath3D);
	}
	
return toolPath3D;
}





/**
 * calculate the Gcode from the tool path
 * @param toolPath3D
 * @param absolute
 * @return
 */
public static String calculateGcode(Vector<ToolMove> toolPath3D,boolean absolute)
{
Vector<String> gcode=new Vector<String>();
String s;
gcode.add(";generated by seeNsee "+new Date());

//set units as mm:
gcode.add("G21");

if (absolute) gcode.add("G90");//absolute positioning
else gcode.add("G91");//incremental positioning
gcode.add("G92X0Y0Z0");//set current as home (0,0,0)

for (int i=0;i<toolPath3D.size();i++)
	{
	Segment3D seg=toolPath3D.elementAt(i).getSegment3D();
	double dx=seg.p2().x()-seg.p1().x();
	double dy=seg.p2().y()-seg.p1().y();
	double dz=seg.p2().z()-seg.p1().z();
	if ((dx==0)&(dy==0)&(dz==0)) continue;
	s=new String("G1 ");
	if (absolute) 
		{
		if (dx!=0) s+=" X"+df3.format(seg.p2().x());
		if (dy!=0) s+=" Y"+df3.format(seg.p2().y()); 
		if (dz!=0) s+=" Z"+df3.format(seg.p2().z()); 
		}
	else 
		{
		if (dx!=0) s+=" X"+df3.format(dx);
		if (dy!=0) s+=" Y"+df3.format(dy); 
		if (dz!=0) s+=" Z"+df3.format(dz); 
		}
	double speed=toolPath3D.elementAt(i).getSpeed();
		
	s+=" F"+df0.format(speed);
		
	gcode.add(s);
	}
StringBuffer sb=new StringBuffer();
for (int i=0;i<gcode.size();i++)
	{
	//sb.append("N"+(i*10)+" "+gcode.elementAt(i)+"\n");
	sb.append(gcode.elementAt(i)+"\n");
	}
return sb.toString();
}




/**
 * transform a curve (t->(x,y)) into a polyLine
 * @param curve
 * @param segmentLength
 * @return
 */
public static Signal1D1DXY getSegments(Function1D2D curve,double segmentLength) 
{
Signal1D1DXY segments=new Signal1D1DXY();
Function1D2D tangent=new Function1D2DTangent(curve,false,0.000001);
double t=0.00000;
double[] P=curve.f(t);
segments.addPoint(P[0], P[1]);

for(;;)
	{
	double[] tan=tangent.f(t);
	double normeTan=Math.sqrt(Math.pow(tan[0],2)+Math.pow(tan[1],2));
	double dt= segmentLength/normeTan;
	//af("dt="+dt);
	t+=dt;
	if (t>1) t=1;
	P=curve.f(t);
	segments.addPoint(P[0], P[1]);
	if (t==1) break;
	}

return segments;
}



}
